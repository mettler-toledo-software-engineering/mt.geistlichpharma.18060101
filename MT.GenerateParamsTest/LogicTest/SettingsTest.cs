﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Xml.Serialization;
using MT.GeistlichPharma.Logic.MTSettings;

namespace MT.UnitTest.LogicTest
{
    [Serializable]
    public class SettingsTest
    {
        public static string FileName = Path.Combine(".\\SettingsTest", "SettingsTest.xml");
        public static string BackupFileName = Path.Combine(".\\SettingsTest", "SettingsTest.xml.bak");

        private static readonly XmlSerializer Serializer = new XmlSerializer(typeof(SettingsTest));
        private static SettingsTest _current;

        #region Load
        private static SettingsTest Load()
        {
            if (!File.Exists(FileName))
            {
                return new SettingsTest();
            }
            else
            {
                try
                {
                    using (FileStream fs = new FileStream(FileName, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        return (SettingsTest)Serializer.Deserialize(fs);
                    }
                }
                catch (Exception)
                {
                    return new SettingsTest();
                }
            }
        }

        public static SettingsTest Current
        {
            get
            {
                EnsureSettings();
                return _current;
            }
        }

        public static void EnsureSettings()
        {
            if (_current == null)
                _current = Load();
        }
        #endregion

        public List<Recipe> Recipes { get; set; }
        public int Version { get; set; }
        public DateTime LastChanged { get; set; }
        
        /// <summary>
        /// Initializes a new instance of the <see cref="Settings"/> class.
        /// </summary>
        public SettingsTest()
        {
            Recipes = new List<Recipe>();
            Version = 0;
            LastChanged = new DateTime();
        }

        #region AddRecipe
        public void AddRecipe(Recipe recipe)
        {
            Recipes.Add(recipe);
        }
        #endregion

        #region UpdateRecipe
        public void UpdateRecipe(int index, Recipe newRecipe)
        {
            Recipes[index] = newRecipe;
        }
        #endregion

        #region DeleteRecipe
        public void DeleteRecipe(int index)
        {
            Recipes.RemoveAt(index);
        }
        #endregion

        #region Save
        [SuppressMessage("ReSharper", "AssignNullToNotNullAttribute")]
        public void Save()
        {
            Version++;
            LastChanged = DateTime.Now;

            if (FileName == null) return;
            if (!Directory.Exists(FileName))
                Directory.CreateDirectory(Path.GetDirectoryName(FileName));

            using (var fs = new FileStream(FileName, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                Serializer.Serialize(fs, this);
            }
        }
        #endregion

        #region SaveCurrentVersion
        [SuppressMessage("ReSharper", "AssignNullToNotNullAttribute")]
        public void SaveCurrentVersion()
        {
            if (FileName == null) return;
            if (!Directory.Exists(FileName))
                Directory.CreateDirectory(Path.GetDirectoryName(FileName));

            using (var fs = new FileStream(FileName, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                Serializer.Serialize(fs, this);
            }
        }
        #endregion
    }
}
