﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Xml.Serialization;

namespace MT.UnitTest.LogicTest
{
    public class HashCodeTest
    {
        public static readonly string FileName = Path.Combine(".\\HashCodeTest", "HashCodeTest.xml");
        private static readonly XmlSerializer Serializer = new XmlSerializer(typeof(HashCodeTest));
        private static HashCodeTest _current;

        #region Load
        private static HashCodeTest Load()
        {
            if (!File.Exists(FileName))
            {
                return new HashCodeTest();
            }
            else
            {
                try
                {
                    using (FileStream fs = new FileStream(FileName, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        return (HashCodeTest)Serializer.Deserialize(fs);
                    }
                }
                catch (Exception)
                {
                    return new HashCodeTest();
                }
            }
        }

        public static HashCodeTest Current
        {
            get
            {
                EnsureSettings();
                return _current;
            }
        }

        public static void EnsureSettings()
        {
            if (_current == null)
                _current = Load();
        }
        #endregion

        public string Code { get; set; }

        #region Generate

        public bool Generate(string file)
        {
            try
            {
                Code = GenerateHashcode(file);
                Save();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion

        #region GenerateHashcode

        public string GenerateHashcode(string file)
        {
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(file))
                {
                    return BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", string.Empty);
                }
            }
        }

        #endregion

        #region Compare HashCode

        public bool IsSameHashCode(string file)
        {
            string oldHashcode = Code;
            string newHashcode = GenerateHashcode(file);

            return oldHashcode.Equals(newHashcode);
        }

        #endregion

        #region Save

        public void Save()
        {
            using (FileStream fs = new FileStream(FileName, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                Serializer.Serialize(fs, this);
            }
        }

        #endregion
    }
}
