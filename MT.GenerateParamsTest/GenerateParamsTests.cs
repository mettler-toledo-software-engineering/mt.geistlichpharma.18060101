﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MT.GeistlichPharma.Logic.MTSettings;
using MT.UnitTest.LogicTest;

namespace MT.UnitTest
{
    [TestClass]
    public class GenerateParamsTests
    {
        private Recipe _expectedRecipe;

        [TestMethod]
        public void TestSaveRecipeSuccess()
        {
            ResetRecipesInSettings();

            var actualRecipe = SettingsTest.Current.Recipes[0];

            Assert.AreSame(_expectedRecipe, actualRecipe);
        }

        [TestMethod]
        public void TestEditRecipeSuccess()
        {
            ResetRecipesInSettings();

            _expectedRecipe.Description = "Edited Recipe";
            SettingsTest.Current.UpdateRecipe(0, _expectedRecipe);

            var actualRecipe = SettingsTest.Current.Recipes[0];

            Assert.AreSame(_expectedRecipe, actualRecipe);
        }

        [TestMethod]
        public void TestEditRecipeFail()
        {
            ResetRecipesInSettings();

            var recipe = new Recipe();
            recipe.AcceptanceMax = 0;
            recipe.AcceptanceMin = 0;
            recipe.Description = "new Recipe";

            _expectedRecipe.Description = "Edited Recipe";

            SettingsTest.Current.Recipes.Add(recipe);
            SettingsTest.Current.UpdateRecipe(0, _expectedRecipe);

            var actualRecipe = SettingsTest.Current.Recipes[1];

            Assert.AreNotSame(_expectedRecipe, actualRecipe);
        }

        [TestMethod]
        public void TestDeleteRecipeSuccess()
        {
            ResetRecipesInSettings();

            SettingsTest.Current.DeleteRecipe(0);

            Assert.IsNotNull(_expectedRecipe);
            var actualRecipe = SettingsTest.Current.Recipes.Find(p => p.Equals(_expectedRecipe));

            Assert.IsNull(actualRecipe);
        }

        [TestMethod]
        public void TestDeleteRecipeFail()
        {
            ResetRecipesInSettings();

            try
            {
                SettingsTest.Current.DeleteRecipe(1);

                Assert.IsNotNull(_expectedRecipe);
            }
            catch (Exception)
            {
                // ignored
            }

            var actualRecipe = SettingsTest.Current.Recipes.Find(p => p.Equals(_expectedRecipe));

            Assert.IsNotNull(actualRecipe);
        }

        [TestMethod]
        public void TestGenerateHashcodeSuccess()
        {
            var filename = SettingsTest.FileName;

            ResetSettingsTest();

            var actual = HashCodeTest.Current.GenerateHashcode(filename);
            const string expected = "1C0B746885F03FC4BEE190FE17D66AFD";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestGenerateHashcodeFail()
        {
            var filename = SettingsTest.FileName;

            ResetSettingsTest();

            SettingsTest.Current.Recipes.Add(_expectedRecipe);
            SettingsTest.Current.Save();

            var actual = HashCodeTest.Current.GenerateHashcode(filename);
            const string expected = "1C0B746885F03FC4BEE190FE17D66AFD";

            Assert.AreNotEqual(expected, actual);
        }
        
        #region Helper Method

        private void ResetRecipe()
        {
            _expectedRecipe =  new Recipe
            {
                Number = "12345",
                Description = "Recipe 1",
                Size = "10x25cm",
                Target = 2.5,
                AcceptanceMin = 2.52,
                AcceptanceMax = 2.55,
                Limit1 = 2.0,
                Limit2 = 2.2
            };
        }

        private void ResetRecipesInSettings()
        {
            ResetRecipe();

            SettingsTest.Current.Recipes.Clear();
            SettingsTest.Current.AddRecipe(_expectedRecipe);
            SettingsTest.Current.SaveCurrentVersion();
        }

        private static void ResetSettingsTest()
        {
            SettingsTest.Current.Recipes.Clear();
            SettingsTest.Current.LastChanged = new DateTime(2018, 11, 23, 0, 0, 0);
            SettingsTest.Current.Version = 1;
            SettingsTest.Current.SaveCurrentVersion();
        }

        #endregion
    }
}
