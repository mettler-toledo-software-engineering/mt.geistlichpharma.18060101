﻿using System;
using MT.Singularity.Platform.CommonUX.Setup.ViewModels;
using MT.Singularity.Platform.CommonUX.Setup.Impl;
using System.Threading.Tasks;
using System.Diagnostics;
using MT.Singularity.Collections;

namespace MT.GeistlichPharma.Configuration
{
    class PrinterSubNode : GroupSetupMenuItem
    {
        private readonly BalancerConfiguration _configuration;

        public PrinterSubNode(
            SetupMenuContext context,
            IBalancerComponents customerComponent,
            BalancerConfiguration configuration)
            : base(context,
                new TitleAndSubtitle(
                    Localization.GetTranslationModule(),
                    (int)Localization.Key.PrinterSubNode),
                configuration,
                customerComponent)
        {
            _configuration = configuration;
        }

        public override Task ShowChildrenAsync()
        {
            try
            {
                // Titles
                var apr331KontrastTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.APR331Kontrast);
                var apr331WiderstandTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.APR331Widerstand);

                // values
                var apr331KontrastTarget = new DropDownSetupMenuItem(_context, apr331KontrastTitle, _configuration, "KontrastItems", "Apr331Kontrast");
                var apr331WiderstandTarget = new DropDownSetupMenuItem(_context, apr331WiderstandTitle, _configuration, "WiderstandItems", "Apr331Widerstand");

                var rangeGroup = new GroupedSetupMenuItems(_context, apr331KontrastTarget, apr331WiderstandTarget);
                Children = Indexable.ImmutableValues<SetupMenuItem>(rangeGroup);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }

            return TaskEx.CompletedTask;
        }
    }
}
