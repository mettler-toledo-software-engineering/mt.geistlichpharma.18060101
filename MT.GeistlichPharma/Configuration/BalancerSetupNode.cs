﻿using System;
using MT.Singularity.Collections;
using MT.Singularity.Composition;
using MT.Singularity.Logging;
using MT.Singularity.Platform.CommonUX.Setup.Impl;
using MT.Singularity.Platform.CommonUX.Setup.ViewModels;

namespace MT.GeistlichPharma.Configuration
{
    [Export(typeof(CustomerGroupSetupMenuItem))]
    public class BalancerSetupNode : CustomerGroupSetupMenuItem
    {
        public BalancerSetupNode(SetupMenuContext context)
            : base(
                context,
                new TitleAndSubtitle(
                    Localization.GetTranslationModule(),
                    (int)Localization.Key.BalancerMainNode))
        {
        }

        public async override System.Threading.Tasks.Task ShowChildrenAsync()
        {
            try
            {
                var balancerComponent = _context.CompositionContainer.Resolve<IBalancerComponents>();
                BalancerConfiguration balancerConfiguration = await balancerComponent.GetConfigurationToChangeAsync();
                Children =
                    Indexable.ImmutableValues<SetupMenuItem>(
                        new GeneralSettingsSubNode(_context, balancerComponent, balancerConfiguration),
                        //new ContainerSubNode(_context, balancerComponent, balancerConfiguration),
                        new PrinterSubNode(_context, balancerComponent, balancerConfiguration));
            }
            catch (Exception ex)
            {
                Log4NetManager.ApplicationLogger.Error("Error BalancerSetupnode.ShowChildrenAsync", ex);
            }
        }
    }
}
