﻿using MT.Singularity.Platform.Configuration;

namespace MT.GeistlichPharma.Configuration
{
    public interface IBalancerComponents : IConfigurable<BalancerConfiguration>
    {
    }
}
