﻿using System;
using System.Threading.Tasks;
using MT.GeistlichPharma.Logic.MTSettings;
using MT.Singularity.Collections;
using MT.Singularity.Logging;
using MT.Singularity.Platform.CommonUX.Setup.Impl;
using MT.Singularity.Platform.CommonUX.Setup.ViewModels;

namespace MT.GeistlichPharma.Configuration
{
    class GeneralSettingsSubNode : GroupSetupMenuItem
    {
        private readonly BalancerConfiguration _configuration;

        public GeneralSettingsSubNode(
        SetupMenuContext context,
        IBalancerComponents balancerComponent,
        BalancerConfiguration configuration)
            : base(context,
                new TitleAndSubtitle(
                    Localization.GetTranslationModule(),
                    (int)Localization.Key.GeneralSettingsSubNode),
                configuration,
                balancerComponent)
        {
            _configuration = configuration;
        }

        public override Task ShowChildrenAsync()
        {
            try
            {
                var waitingDurationTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.WaitingDurationSubNode);
                var waitingDuration = new TextSetupMenuItem(_context, waitingDurationTitle, _configuration, "WaitingDuration");

                var pulsingDurationTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.PulsingDurationSubNode);
                var pulsingDuration = new TextSetupMenuItem(_context, pulsingDurationTitle, _configuration, "PulsingDuration");

                var noActionTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.NoActionTimer);
                var noAction = new TextSetupMenuItem(_context, noActionTitle, _configuration, "NoActionTimer");

                var pulsingGroup = new GroupedSetupMenuItems(_context, waitingDuration, pulsingDuration);
                var noActionGroup = new GroupedSetupMenuItems(_context, noAction);

                Children = Indexable.ImmutableValues<SetupMenuItem>(pulsingGroup, noActionGroup);
            }
            catch (Exception ex)
            {
                Log4NetManager.ApplicationLogger.Error("Error BalancerDataServiceSubnode.ShowChildrenAsync", ex);
            }

            return TaskEx.CompletedTask;
        }

    }
}
