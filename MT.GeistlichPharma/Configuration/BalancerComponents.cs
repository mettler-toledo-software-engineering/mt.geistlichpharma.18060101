﻿using MT.Singularity.Composition;
using MT.Singularity.Platform.Configuration;
using MT.Singularity.Platform.UserManagement;

namespace MT.GeistlichPharma.Configuration
{
    [Export(typeof(IBalancerComponents))]
    [Export(typeof(IConfigurable))]
    [InjectionBehavior(IsSingleton = true)]
    public class BalancerComponents : ConfigurationStoreConfigurable<BalancerConfiguration>, IBalancerComponents
    {
        public BalancerComponents(IConfigurationStore configurationStore, ISecurityService securityService, CompositionContainer compositionContainer, bool traceChanges = true)
            : base(configurationStore, securityService, compositionContainer, traceChanges)
        {
        }

        public override string ConfigurationSelector
        {
            get { return "BalancerConfiguration"; }
        }

        public override string FriendlyName
        {
            get { return "Balancer Configuration"; }
        }

    }
}
