﻿using System;
using System.ComponentModel;
using MT.Singularity.Collections;
using MT.Singularity.Components;
using MT.Singularity.Platform.Configuration;

namespace MT.GeistlichPharma.Configuration
{
    [Component]
    public class BalancerConfiguration : ComponentConfiguration
    {
        public BalancerConfiguration()
        {
            KontrastItems = Apr331KonstrastItemStrings;
            WiderstandItems = Apr331WiderstandItemStrings;
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue("192.168.174.149")]
        public virtual String IpAddress
        {
            get { return (String)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(20000)]
        public virtual int Port
        {
            get { return (int)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(5)]
        public virtual int HostTimeout
        {
            get { return (int)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(2)]
        public virtual int WaitingDuration
        {
            get { return (int)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(1)]
        public virtual int PulsingDuration
        {
            get { return (int)GetLocal(); }
            set { SetLocal(value); }
        }
        
        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(60)]
        public virtual int NoActionTimer
        {
            get { return (int)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(8.8)]
        public virtual double PenWeight
        {
            get { return (double)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(0.2)]
        public virtual double PenTolerance
        {
            get { return (double)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(16.8)]
        public virtual double VialWeight
        {
            get { return (double)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(0.2)]
        public virtual double VialTolerance
        {
            get { return (double)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(67.0)]
        public virtual double BottleWeight
        {
            get { return (double)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(0.5)]
        public virtual double BottleTolerance
        {
            get { return (double)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(0.0)]
        public virtual double BioFlowWeight
        {
            get { return (double)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(0.0)]
        public virtual double BioFlowTolerance
        {
            get { return (double)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue("4")]
        public virtual string Apr331Kontrast
        {
            get { return (string)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting]
        [Component]
        public virtual DateTime LastCleanup
        {
            get { return (DateTime)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue("2")]
        public virtual string Apr331Widerstand
        {
            get { return (string)GetLocal(); }
            set { SetLocal(value); }
        }

        private IImmutableIndexable<String> _kontrastItems;
        public IImmutableIndexable<String> KontrastItems
        {
            get { return _kontrastItems; }
            set
            {
                if (!Equals(_kontrastItems, value))
                {
                    _kontrastItems = value;
                    NotifyPropertyChanged();
                    NotifyPropertyChanged();
                }
            }
        }

        private IImmutableIndexable<string> _widerstandItems;
        public IImmutableIndexable<string> WiderstandItems
        {
            get { return _widerstandItems; }
            set
            {
                if (!Equals(_widerstandItems, value))
                {
                    _widerstandItems = value;
                    NotifyPropertyChanged();
                    NotifyPropertyChanged();
                }
            }
        }

        public static readonly IImmutableIndexable<string> Apr331KonstrastItemStrings = Indexable.ImmutableValues(
            "0", "1", "2", "3", "4", "5", "6", "7", "8"
        );

        public static readonly IImmutableIndexable<string> Apr331WiderstandItemStrings = Indexable.ImmutableValues(
            "0", "1", "2", "3", "4"
        );
    }
}
