﻿//using System;
//using System.Threading.Tasks;
//using MT.GeistlichPharma.Logic.MTSettings;
//using MT.Singularity.Collections;
//using MT.Singularity.Logging;
//using MT.Singularity.Platform.CommonUX.Setup.Impl;
//using MT.Singularity.Platform.CommonUX.Setup.ViewModels;

//namespace MT.GeistlichPharma.Configuration
//{
//    class ContainerSubNode : GroupSetupMenuItem
//    {
//        private readonly BalancerConfiguration _configuration;

//        public ContainerSubNode(
//        SetupMenuContext context,
//        IBalancerComponents balancerComponent,
//        BalancerConfiguration configuration)
//            : base(context,
//                new TitleAndSubtitle(
//                    Localization.GetTranslationModule(),
//                    (int)Localization.Key.ContainerSubNode),
//                configuration,
//                balancerComponent)
//        {
//            _configuration = configuration;
//        }

//        public override Task ShowChildrenAsync()
//        {
//            try
//            {
//                // Pen
//                var weightTitlePen = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.PenWeight);
//                var weightTargetPen = new TextSetupMenuItem(_context, weightTitlePen, _configuration, "PenWeight");

//                var toleranceTitlePen = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.PenTolerance);
//                var toleranceWeightPen = new TextSetupMenuItem(_context, toleranceTitlePen, _configuration, "PenTolerance");

//                var penGroup = new GroupedSetupMenuItems(_context, weightTargetPen, toleranceWeightPen);

//                //// Vial
//                //var weightTitleVial = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.VialWeight);
//                //var weightTargetVial = new TextSetupMenuItem(_context, weightTitleVial, _configuration, "VialWeight");
                
//                //var toleranceTitleVial = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.VialTolerance);
//                //var toleranceWeightVial = new TextSetupMenuItem(_context, toleranceTitleVial, _configuration, "VialTolerance");

//                //var vialGroup = new GroupedSetupMenuItems(_context, weightTargetVial, toleranceWeightVial);

//                // Bottle
//                var weightTitleBottle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.BottleWeight);
//                var weightTargetBottle = new TextSetupMenuItem(_context, weightTitleBottle, _configuration, "BottleWeight");
               
//                var toleranceTitleBottle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.BottleTolerance);
//                var toleranceWeightBottle = new TextSetupMenuItem(_context, toleranceTitleBottle, _configuration, "BottleTolerance");

//                var bottleGroup = new GroupedSetupMenuItems(_context, weightTargetBottle, toleranceWeightBottle);

//                // BioFlow
//                var weightTitleBioFlow = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.BioFlowWeight);
//                var weightTargetBioFlow = new TextSetupMenuItem(_context, weightTitleBioFlow, _configuration, "BioFlowWeight");
               
//                var toleranceTitleBioFlow = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.BioFlowTolerance);
//                var toleranceWeightBioFlow = new TextSetupMenuItem(_context, toleranceTitleBioFlow, _configuration, "BioFlowTolerance");

//                var bioFlowGroup = new GroupedSetupMenuItems(_context, weightTargetBioFlow, toleranceWeightBioFlow);

//                Children = Indexable.ImmutableValues<SetupMenuItem>(penGroup, /*vialGroup,*/ bottleGroup, bioFlowGroup);
//            }
//            catch (Exception ex)
//            {
//                Log4NetManager.ApplicationLogger.Error("Error BalancerDataServiceSubnode.ShowChildrenAsync", ex);
//            }

//            return TaskEx.CompletedTask;
//        }

//    }
//}
