﻿using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.GeistlichPharma.Logic.Process;
using MT.GeistlichPharma.Logic.MTSettings;

namespace MT.GeistlichPharma.Pages
{
    /// <summary>
    /// Interaction logic for MainScreen
    /// </summary>
    public partial class MainScreen
    {
        private readonly MainScreenViewModel _viewModel;

        public MainScreen(AnimatedNavigationFrame homeNavigationFrame, Recipe recipe, Charge charge)
        {
            _viewModel = new MainScreenViewModel(this, homeNavigationFrame, recipe, charge);

            InitializeComponents();
        }

        #region Overrides

        /// <summary>
        /// Called when a page is being navigated to for the first time.
        /// </summary>
        protected override void OnFirstNavigation()
        {
            HomeScreenViewModel.Instance.WeightDisplayShow = Visibility.Visible;
            PanelDeltaTrac.Activate();
            base.OnFirstNavigation();
        }

        protected override NavigationResult OnNavigatingBack(INavigationPage nextPage)
        {
            HomeScreenViewModel.Instance.WeightDisplayShow = Visibility.Collapsed;
            PanelDeltaTrac.Deactivate();
            return base.OnNavigatingBack(nextPage);
        }

        #endregion 
    }
}
