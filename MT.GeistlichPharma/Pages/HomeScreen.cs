﻿using System.Diagnostics.CodeAnalysis;
using MT.Singularity.Composition;
using MT.Singularity.Presentation;
using MT.Singularity.Platform.CommonUX.Infrastructure;
using MT.Singularity.Presentation.Controls.Navigation;


namespace MT.GeistlichPharma.Pages
{
    /// <summary>
    /// Interaction logic for HomeScreen
    /// </summary>
    [Export(typeof(IHomeScreenFactoryService))]
    [SuppressMessage("ReSharper", "ArrangeAccessorOwnerBody")]
    public partial class HomeScreen : IHomeScreenFactoryService
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="HomeScreen"/> class.
        /// </summary>
        /// 
        readonly HomeScreenViewModel _viewModel;

        public HomeScreen()
        {
            _viewModel = new HomeScreenViewModel();
            InitializeComponents();
            _viewModel.InitializeScales();

            homeNavigationFrame.NavigateTo(new StartPage(homeNavigationFrame));
            _weightDisplayWindow.Activate();
            _viewModel.WeightDisplayShow = Visibility.Visible;
        }

        /// <summary>
        /// Gets the home screen page.
        /// </summary>
        /// <value>
        /// The home screen page.
        /// </value>
        public INavigationPage HomeScreenPage
        {
            get { return this; }
        }

        /// <summary>
        /// This method is called before the home screen is shown.
        /// </summary>
        public void BeforeStart(IRootVisualProvider rootVisualProvider)
        {
        }

        #region Overrides
        protected override NavigationResult OnNavigatingBack(INavigationPage nextPage)
        {
            _viewModel.WeightDisplayShow = Visibility.Visible;
            return base.OnNavigatingBack(nextPage);
        }
        #endregion

        /// <summary>
        /// Gets a value indicating whether the cursor should be hidden.
        /// </summary>
        /// <value>
        ///   <c>true</c> to hide the cursor; otherwise, <c>false</c>.
        /// </value>
        public bool HideCursor
        {
#if DEBUG
            get { return false; }
#else
            get { return true; }
#endif
        }
    }
}
