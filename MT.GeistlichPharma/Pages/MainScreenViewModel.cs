﻿using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using System.Threading.Tasks;
using MT.GeistlichPharma.Logic;
using MT.GeistlichPharma.Logic.DigitalIO;
using MT.GeistlichPharma.Logic.MTSettings;
using MT.GeistlichPharma.Logic.Process;
using MT.GeistlichPharma.Stores;
using MT.Singularity.Composition;
using MT.Singularity.Data;
using MT.Singularity.Platform.CommonUX.Controls.DeltaTrac;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Model;

namespace MT.GeistlichPharma.Pages
{
    [SuppressMessage("ReSharper", "ArrangeAccessorOwnerBody")]
    public class MainScreenViewModel : PropertyChangedBase
    {
        #region private variables
        private readonly Visual _parent;
        private readonly AnimatedNavigationFrame _homeNavigationFrame;
        private StateEngine _state;
        private StateEngine _oldState;
        private readonly Recipe _recipe;
        private readonly Charge _charge;
        private DigitalIo _digitalIo;
        private Rapport _rapport;
        private InfoPopup _popUpPage;

        private MT.Singularity.Presentation.Utils.Timer _noActionTimer;

        private double _lim2;
        private double _lim1;
        private int _noActionInterval;
        private bool _chargeRunning = false;

        private double _netWeight;
        private double _grossWeight;

        // Binkert 06.11.2019  private bool _firstPuls = true;
        private bool _pulseDone = true;

        private int _waitingDuration;
        private int _pulsingDuration;

        private readonly ProcessStore _processStore = ApplicationBootstrapperBase.CompositionContainer.Resolve<ProcessStore>();

        #endregion

        public MainScreenViewModel(Visual parent, AnimatedNavigationFrame homeNavigationFrame, Recipe recipe, Charge charge)
        {
            _parent = parent;
            _homeNavigationFrame = homeNavigationFrame;
            _recipe = recipe;
            _charge = charge;

            Initialize();
        }

        #region Initialize
        private void Initialize()
        {
            DosingMessage = "";

            _state = StateEngine.Init;
            _lim1 = _recipe.Limit1;
            _lim2 = _recipe.Limit2;
            _digitalIo = new DigitalIo();
            _rapport = new Rapport();

            SetSettingsFromGlobalConfig();

            _noActionTimer = new Singularity.Presentation.Utils.Timer();
            _noActionTimer.Tick += _noActionTimer_Tick;
            _noActionTimer.Interval = _noActionInterval;

            HomeScreenViewModel.Instance.Scale.NewWeightInDisplayUnit += Scale_NewWeightInDisplayUnit;
            ClearTare();
            _chargeRunning = true;
        }



        void _noActionTimer_Tick(object sender, EventArgs e)
        {
            if (_oldState == _state)
            {
                EnableTimer(false);

                DoGoPause();
                InfoPopup info = new InfoPopup(Localization.Get(Localization.Key.NoStateChanged_Title),
                    String.Format(Localization.Get(Localization.Key.NoStateChanged_Content), (_noActionInterval / 1000)) +
                    "\n" + Localization.Get(Localization.Key.ContinueOrQuitCharge),
                    MessageBoxIcon.Warning, MessageBoxButtons.OK);
                info.Show(_parent);
            }
        }

        private void EnableTimer(bool enable)
        {
            Debug.WriteLine(enable ? "Start Timer" : "Stop Timer");
            _noActionTimer.Enabled = enable;
        }
        #endregion

        #region Get settings from global configuration
        private void SetSettingsFromGlobalConfig()
        {
            _waitingDuration = Globals.Instance.Config.WaitingDuration * 1000;
            _pulsingDuration = Globals.Instance.Config.PulsingDuration * 1000;
            _noActionInterval = Globals.Instance.Config.NoActionTimer * 1000;
        }

        #endregion

        #region * * * STATE ENGINE * * *
        void Scale_NewWeightInDisplayUnit(WeightInformation weight)
        {
            _netWeight = weight.NetWeight;
            _grossWeight = weight.GrossWeight;

            switch (_state)
            {
                case StateEngine.Init:
                    InitStateDisplay();
                    if (weight.IsValid)
                        _state = StateEngine.LoadScale;
                    break;
                case StateEngine.LoadScale:
                    StartButtonIsEnabled = CheckMinWeight();
                    break;
                case StateEngine.PrepareDeltaTrac:
                    SetDeltaTracValues();
                    UpdateChargeInfos();
                    _state = StateEngine.Fill;
                    break;
                case StateEngine.Fill:
                    _oldState = _state;
                    // Binkert 06.11.2019          _firstPuls = true;
                    _pulseDone = true;

                    if (_netWeight <= _lim1)
                    {
                        DigitalIO_StartFastFill();
                        Debug.WriteLine("Fastfill");
                    }
                    else if (_netWeight <= _lim2)
                    {
                        DigitalIO_StartSlowFill();
                        Debug.WriteLine("Slowfill");
                    }
                    else
                    {
                        DigitalIO_StopFill();
                        _state = StateEngine.ToleranceCheck;
                    }
                    break;

                //    Original mit Fehler  Binkert 06.11.2019
                //case StateEngine.ToleranceCheck:
                //    _oldState = _state;

                //    if (weight.NetStable && _pulseDone)
                //    {
                //        if (IsInTolerance())
                //            DosingFinished(true);
                //        else if (IsOverdosed())
                //            DosingFinished(false);
                //        else
                //            _state = StateEngine.Pulsing;


                //        Debug.WriteLine("Lim 1: " + _lim1);
                //        Debug.WriteLine("Lim 2: " + _lim2);

                //        if (_firstPuls)
                //            NachstromKorrektur();
                //    }
                //    break;

                //  Binkert 06.11.2019
                // korrigierter Toleranzcheck -> Nachstromkorr. muss hier immer gemacht werden!


                case StateEngine.ToleranceCheck:
                    _oldState = _state;

                    if (weight.NetStable)
                    {
                        if (IsInTolerance())
                            DosingFinished(true);
                        else if (IsOverdosed())
                            DosingFinished(false);
                        else
                            _state = StateEngine.Pulsing;
                        NachstromKorrektur();
                    }
                    break;

                // Binkert 06.11.2019  -> neuer state nur für Toleranzkontrolle nach Puls  !! keine Nachstromkorrektur !!
                case StateEngine.ToleranceCheckAfterPuls:
                    _oldState = _state;
                    if (weight.NetStable && _pulseDone)
                    {
                        if (IsInTolerance())
                            DosingFinished(true);
                        else if (IsOverdosed())
                            DosingFinished(false);
                        else
                            _state = StateEngine.Pulsing;
                    }
                    break;

                case StateEngine.Pulsing:
                    _oldState = _state;
                    SetPulse(_waitingDuration, _pulsingDuration);
                    // Binkert 06.11.2019               _firstPuls = false;
                    // Binkert 06.11.2019               _state = StateEngine.ToleranceCheck;
                    _state = StateEngine.ToleranceCheckAfterPuls; // Binkert 06.11.2019  -> neuer state nur für Toleranzkontrolle nach Puls  !! keine Nachstromkorrektur !!
                    break;
                case StateEngine.Done:
                    EnableTimer(false);
                    UpdateChargeInfos();
                    ShowQuitMenu();
                    _popUpPage.Show(_parent);
                    _state = StateEngine.ResetScale;
                    break;
                case StateEngine.ResetScale:
                    if (weight.GrossWeight <= _recipe.Container.Weight)
                    {
                        if (_isInTolerance)
                            _popUpPage.Close();

                        ClearTare();
                        ResetScaleDisplay();
                        _state = StateEngine.LoadScale;
                    }
                    break;
            }
        }

        private bool CheckMinWeight()
        {
            double minWeight = _recipe.Container.Weight - _recipe.Container.Tolerance;
            double maxWeight = _recipe.Container.Weight + _recipe.Container.Tolerance;

            if (_grossWeight >= minWeight && _grossWeight <= maxWeight)
            {
                _state = StateEngine.Idle;
                Message = Localization.Get(Localization.Key.ScaleReady);
                return true;
            }

            DosingMessage = "";
            Message = String.Format(Localization.Get(Localization.Key.LoadScale), _recipe.Container.Name);
            return false;
        }
        #endregion

        #region DosingFinished
        bool _isInTolerance;
        private void DosingFinished(bool isInTolerance)
        {
            _isInTolerance = isInTolerance;
            Debug.WriteLine("In tolerance: " + _isInTolerance);

            if (isInTolerance)
            {
                _processStore.SaveWeight(_netWeight);
                _popUpPage = new InfoPopup(Localization.Get(Localization.Key.DosingSucceeded),
                    Localization.Get(Localization.Key.FilledValue) + ": " + StaticStuff.WeightToString(_netWeight) /*_netWeight.ToString("0.000 g")*/ + "\n" +
                    Localization.Get(Localization.Key.RemoveFullContainer));
            }
            else
            {
                _processStore.RecipeOverdosed();
                string text = Localization.Get(Localization.Key.PutContentInFunnel);
                Message = text;
                MessageVisibility = Visibility.Visible;
                DeltaTracVisibility = Visibility.Collapsed;
                _popUpPage = new InfoPopup(Localization.Get(Localization.Key.Overdosed), text, MessageBoxIcon.Warning, MessageBoxButtons.OK);
            }

            _state = StateEngine.Done;
        }
        #endregion

        #region DigitalIO
        private void DigitalIO_StopFill()
        {
            DosingMessage = "";
            if (_digitalIo.OutputState != _digitalIo.Stop)
                _digitalIo.StopFill();
        }

        private void DigitalIO_StartFastFill()
        {
            DosingMessage = Localization.Get(Localization.Key.FastFillMessage);
            if (_digitalIo.OutputState != _digitalIo.Fast)
                _digitalIo.StartFastFill();
        }

        private void DigitalIO_StartSlowFill()
        {
            DosingMessage = Localization.Get(Localization.Key.SlowFillMessage);
            if (_digitalIo.OutputState != _digitalIo.Slow)
                _digitalIo.StartSlowFill();
        }
        #endregion

        #region Print Body
        private async Task StartPrinting()
        {
            _rapport.Recipe = _recipe;
            _rapport.Charge = _charge;

            await _rapport.PrintBodyAsync();
        }
        #endregion

        #region Scale
        private static async void ClearTare()
        {
            await HomeScreenViewModel.Instance.Scale.ClearTareAsync();
        }
        #endregion

        #region Displays

        private void UpdateChargeInfos()
        {
            RecipeInTolerance = _charge.RecipesInTol;
            RecipeOverDosed = _charge.RecipesOverdosed;
            RecipeCounter = _charge.TotalFillings;
        }

        private void ResetScaleDisplay()
        {
            ShowMessage(true);
            ShowGraphicField(false);

            RecipeInfoVisibility = Visibility.Visible;
            ChargeInfoVisibility = Visibility.Visible;

            ShowHideButtons(false, false, true, true, false, false);
            StartButtonIsEnabled = false;
        }
        private void ReadyStateDisplay()
        {
            ShowMessage(false);
            ShowGraphicField(true);
            SetRecipeInfos();
        }

        private void InitStateDisplay()
        {
            ShowMessage(true);
            ShowGraphicField(false);

            ShowStartMenu();
        }
        #endregion

        #region Menus
        private void ShowQuitMenu()
        {
            RecipeInfoVisibility = Visibility.Visible;
            ChargeInfoVisibility = Visibility.Visible;

            ShowHideButtons(false, false, true, false, false, false);
        }

        private void ShowStartMenu()
        {
            RecipeInfoVisibility = Visibility.Collapsed;
            ChargeInfoVisibility = Visibility.Collapsed;

            ShowHideButtons(true, false, false, true, false, false);
        }
        #endregion

        #region NachstromKorrektur
        [SuppressMessage("ReSharper", "IdentifierTypo")]
        private void NachstromKorrektur()
        {
            double difference = _recipe.AcceptanceMin - _netWeight;
            double deltaDifference = 2 * (_recipe.AcceptanceMax - _recipe.AcceptanceMin);

            // Binkert 06.11.2019  if (!(difference <= deltaDifference)) return;
            if (Math.Abs(difference) > deltaDifference)  // +/- Korrektur ermöglichen!!
            {
                // keine Nachstromkorrektur, da ausserhalb Plausibilitätsgrenzen
                return;
            }
            _lim1 += difference / 2;
            Debug.WriteLine("Limit 1: " + _lim1);

            _lim2 += difference / 2;
            Debug.WriteLine("Limit 2: " + _lim2);
        }
        #endregion

        #region Pulsing
        int _pauseWidth, _pulseWidth;

        private void SetPulse(int pauseWidth, int pulseWidth)
        {
            _pauseWidth = pauseWidth;
            _pulseWidth = pulseWidth;

            _pulseDone = false;
            Task.Run(async () => await PulseThread());

            //Thread pulseThread = new Thread(PulseThread);
            //pulseThread.Start();
        }

        private async Task PulseThread()
        {
            if (_pulseWidth > 0)
            {
                _digitalIo.StartSlowFill();

                Debug.WriteLine("Pulsing " + _pulseWidth + " seconds...");
                DosingMessage = Localization.Get(Localization.Key.Pulsing) + "...";
                await Task.Delay(_pulseWidth);
                //Thread.Sleep(_pulseWidth);
                _digitalIo.StopFill();
            }

            if (_pauseWidth > 0)
            {
                Debug.WriteLine("Waiting " + _pauseWidth + " seconds...");
                DosingMessage = Localization.Get(Localization.Key.Waiting) + "...";
                await Task.Delay(_pauseWidth);
                //Thread.Sleep(_pauseWidth);
            }

            DosingMessage = "";

            _pulseDone = true;
        }
        #endregion

        #region Helper Methods

        #region ---- SetDeltaTracValues
        [SuppressMessage("ReSharper", "SpecifyACultureInStringConversionExplicitly")]
        private void SetDeltaTracValues()
        {
            var target = _recipe.AcceptanceMin;
            const double lowTol = 0;
            var upTol = _recipe.AcceptanceMax - target;

            Target = target.ToString();
            LowerTolerance = Math.Round(lowTol, 4).ToString();
            UpperTolerance = upTol.ToString();
        }
        #endregion

        #region ---- ShowTextField
        private void ShowMessage(bool show)
        {
            MessageVisibility = show ? Visibility.Visible : Visibility.Collapsed;
        }
        #endregion

        #region ---- ShowRecipeInfos
        private void SetRecipeInfos()
        {
            RecipeNumber = _recipe.Number;
            RecipeDescription = _recipe.Description;
            RecipeSize = _recipe.Size + ", " + StaticStuff.WeightToString(_recipe.Target); //_recipe.Target.ToString("0.000 g");
            ChargeNumber = Localization.Get(Localization.Key.ChargrNr) + ": " + _charge.Number;
        }
        #endregion

        #region ---- ShowGraphicField
        private void ShowGraphicField(bool show)
        {
            DeltaTracVisibility = show ? Visibility.Visible : Visibility.Collapsed;
        }
        #endregion

        #region ---- IsInTolerance
        private bool IsInTolerance()
        {
            return (_netWeight >= _recipe.AcceptanceMin) && (_netWeight <= _recipe.AcceptanceMax);
        }
        #endregion

        #region ---- IsOverdosed
        private bool IsOverdosed()
        {
            return _netWeight > _recipe.AcceptanceMax;
        }
        #endregion

        #endregion

        #region KeyHandler

        #region ---- Back
        /// <summary>
        /// GoBack
        /// </summary>
        public ICommand GoBack
        {
            get { return new DelegateCommand(DoGoBack); }
        }

        private async void DoGoBack()
        {
            if (_chargeRunning == false)
            {
                var mb = MessageBox.Show(_parent, "Achtung Prozessdaten werden gelöscht\nEs werden keine weiteren Ausdrucke möglich sein!", "Achtung!", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                var result = await mb.ShowAsync(_parent);
                if (result == DialogResult.OK)
                {
                    _processStore.ResetProcess();
                }
            }

            _homeNavigationFrame.Back();
        }
        #endregion

        #region ---- Print
        /// <summary>
        /// GoPrint
        /// </summary>
        public ICommand GoPrint
        {
            get { return new DelegateCommand(DoGoPrint); }
        }

        private void DoGoPrint()
        {
            PrintButtonIsEnabled = false;

            //Thread print = new Thread(async () =>
            //{
            //    await _rapport.ReprintAsync();
            //    PrintButtonIsEnabled = true;
            //});
            //print.Start();

            Task.Run(async () =>
            {
                await _rapport.ReprintAsync();
                PrintButtonIsEnabled = true;
            });
        }
        #endregion

        #region ---- Start
        /// <summary>
        /// Start
        /// </summary>
        public ICommand GoStart
        {
            get { return new DelegateCommand(DoGoStart); }
        }

        private void DoGoStart()
        {
            if (CheckMinWeight())
            {
                StartButtonIsEnabled = false;

                Task.Run(async () =>
                {
                    WeightState state = await HomeScreenViewModel.Instance.Scale.TareAsync();

                    if (state == WeightState.OK)
                    {
                        ReadyStateDisplay();

                        RecipeInfoVisibility = Visibility.Visible;
                        ChargeInfoVisibility = Visibility.Visible;

                        ShowHideButtons(false, false, false, false, true, false);

                        _state = StateEngine.PrepareDeltaTrac;
                    }
                    else
                    {
                        InfoPopup info = new InfoPopup(Localization.Get(Localization.Key.TareFailed_Title),
                            Localization.Get(Localization.Key.TareFailed_Content), MessageBoxIcon.Error,
                            MessageBoxButtons.OK);
                        info.Show(_parent);
                        StartButtonIsEnabled = true;
                    }
                });

                //Thread t = new Thread(async () =>
                //{
                //    WeightState state = await HomeScreenViewModel.Instance.Scale.TareAsync();

                //    if (state == WeightState.OK)
                //    {
                //        ReadyStateDisplay();

                //        RecipeInfoVisibility = Visibility.Visible;
                //        ChargeInfoVisibility = Visibility.Visible;

                //        ShowHideButtons(false, false, false, false, true, false);

                //        _state = StateEngine.PrepareDeltaTrac;
                //    }
                //    else
                //    {
                //        InfoPopup info = new InfoPopup(Localization.Get(Localization.Key.TareFailed_Title),
                //            Localization.Get(Localization.Key.TareFailed_Content), MessageBoxIcon.Error, MessageBoxButtons.OK);
                //        info.Show(_parent);
                //        _startButtonIsEnabled = true;
                //    }
                //});

                //t.Start();

                EnableTimer(true);
            }
            else
                _state = StateEngine.LoadScale;
        }
        #endregion

        #region ---- Done
        /// <summary>
        /// Done
        /// </summary>
        public ICommand GoDone
        {
            get { return new DelegateCommand(DoGoQuit); }
        }

        private void DoGoQuit()
        {
            _popUpPage = new InfoPopup(Localization.Get(Localization.Key.ChargeDone_TitleQuestion),
                String.Format(Localization.Get(Localization.Key.ChargeDone_ContentQuestion), _charge.Number),
                MessageBoxIcon.Question, MessageBoxButtons.YesNo);
            _popUpPage.Closed += _popUpPage_ChargeDone;
            _popUpPage.Show(_parent);
        }


        void _popUpPage_ChargeDone(object sender, EventArgs e)
        {
            if (((InfoPopup)sender).ViewModel.DialogResult == DialogResult.Yes)
            {
                EnableTimer(false);

                ShowHideButtons(true, true, false, false, false, false);

                StopIoPort();

                //Thread print = new Thread(async () => { await StartPrinting(); });
                //print.Start();

                Task.Run(async () => { await StartPrinting(); });

                _chargeRunning = false;
                HomeScreenViewModel.Instance.Scale.NewWeightInDisplayUnit -= Scale_NewWeightInDisplayUnit;
            }
        }

        private void ShowHideButtons(bool backBtn, bool printBtn, bool quitBtn, bool startBtn, bool pauseBtn, bool continueBtn)
        {
            BackButtonVisibility = backBtn ? Visibility.Visible : Visibility.Collapsed;
            PrintButtonVisibility = printBtn ? Visibility.Visible : Visibility.Collapsed;
            QuitButtonVisibility = quitBtn ? Visibility.Visible : Visibility.Collapsed;
            StartButtonVisibility = startBtn ? Visibility.Visible : Visibility.Collapsed;
            PauseButtonVisibility = pauseBtn ? Visibility.Visible : Visibility.Collapsed;
            ContinueButtonVisibility = continueBtn ? Visibility.Visible : Visibility.Collapsed;
        }
        #endregion

        #region ---- Pause
        /// <summary>
        /// Pause
        /// </summary>
        public ICommand GoPause
        {
            get { return new DelegateCommand(DoGoPause); }
        }

        private void DoGoPause()
        {
            EnableTimer(false);
            StopIoPort();
            Message = Localization.Get(Localization.Key.ProzessPaused);
            DosingMessage = "";

            ShowMessage(true);
            ShowGraphicField(false);

            ShowHideButtons(false, false, true, false, false, true);
        }

        private void StopIoPort()
        {
            _digitalIo.StopFill();
            _state = StateEngine.Idle;
        }
        #endregion

        #region ---- Continue
        /// <summary>
        /// Continue
        /// </summary>
        public ICommand GoContinue
        {
            get { return new DelegateCommand(DoGoContinue); }
        }

        private void DoGoContinue()
        {
            EnableTimer(true);
            StartIoPort();
            ShowMessage(false);
            ShowGraphicField(true);

            ShowHideButtons(false, false, false, false, true, false);
        }

        private void StartIoPort()
        {
            _digitalIo.Continue();
            _state = _oldState;
        }
        #endregion

        #endregion

        #region DeltaTrac

        #region Target
        /// <summary>
        /// Gets or sets the target.
        /// </summary>
        /// <value>
        /// The target.
        /// </value>
        public string Target
        {
            get { return _target; }
            set
            {
                if (_target != value)
                {
                    _target = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _target = "3";
        #endregion

        #region LowerTolerance
        /// <summary>
        /// Gets or sets the lower tolerance.
        /// </summary>
        /// <value>
        /// The lower tolerance.
        /// </value>
        public string LowerTolerance
        {
            get { return _lowerTolerance; }
            set
            {
                // ReSharper disable once RedundantCheckBeforeAssignment
                if (_lowerTolerance != value)
                {
                    _lowerTolerance = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _lowerTolerance = "-1";
        #endregion

        #region UpperTolerance
        /// <summary>
        /// Gets or sets the upper tolerance.
        /// </summary>
        /// <value>
        /// The upper tolerance.
        /// </value>
        public string UpperTolerance
        {
            get { return _upperTolerance; }
            set
            {
                if (_upperTolerance != value)
                {
                    _upperTolerance = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _upperTolerance = "2";
        #endregion

        #region Appereance
        /// <summary>
        /// Gets or sets the appearance.
        /// </summary>
        /// <value>
        /// The appearance.
        /// </value>
        public DeltaTracAppearance Appearance
        {
            get { return _appearance; }
            set
            {
                if (_appearance != value)
                {
                    _appearance = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private DeltaTracAppearance _appearance = DeltaTracAppearance.BarGraph;
        #endregion

        #endregion DeltaTrac

        #region Visibility

        #region ---- StartButtonIsEnabled
        /// <summary>
        /// StartButtonIsEnabled
        /// </summary>
        public bool StartButtonIsEnabled
        {
            get { return _startButtonIsEnabled; }
            set
            {
                if (_startButtonIsEnabled != value)
                {
                    _startButtonIsEnabled = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private bool _startButtonIsEnabled = true;
        #endregion

        #region ---- PrintButtonIsEnabled
        /// <summary>
        /// StartButtonIsEnabled
        /// </summary>
        public bool PrintButtonIsEnabled
        {
            get { return _printButtonIsEnabled; }
            set
            {
                if (_printButtonIsEnabled != value)
                {
                    _printButtonIsEnabled = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private bool _printButtonIsEnabled = true;
        #endregion

        #region ---- Message
        /// <summary>
        /// Show Text in YML
        /// </summary>
        public string Message
        {
            get { return _message; }
            set
            {
                if (_message != value)
                {
                    _message = value;
                    NotifyPropertyChanged();
                }
            }
        }
        
        //private string _message = Localization.Get(Localization.Key.LoadScale);
        private string _message = Localization.Get(Localization.Key.LoadScale);
        #endregion

        #region ---- DosingMessage
        /// <summary>
        /// Show Text in YML
        /// </summary>
        public string DosingMessage
        {
            get { return _dosingMessage; }
            set
            {
                if (_dosingMessage != value)
                {
                    _dosingMessage = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _dosingMessage = Localization.Get(Localization.Key.FastFillMessage);
        #endregion

        #region ---- RecipeNumber
        /// <summary>
        /// Show Text in YML
        /// </summary>
        public string RecipeNumber
        {
            get { return _recipeNumber; }
            set
            {
                if (_recipeNumber != value)
                {
                    _recipeNumber = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _recipeNumber = "";
        #endregion

        #region ---- RecipeDescription
        /// <summary>
        /// Show Text in YML
        /// </summary>
        public string RecipeDescription
        {
            get { return _recipeDescription; }
            set
            {
                if (_recipeDescription != value)
                {
                    _recipeDescription = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _recipeDescription = "";
        #endregion

        #region ---- RecipeSize
        /// <summary>
        /// Show Text in YML
        /// </summary>
        public string RecipeSize
        {
            get { return _recipeSize; }
            set
            {
                if (_recipeSize != value)
                {
                    _recipeSize = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _recipeSize = "";
        #endregion

        #region ---- ChargeNumber
        /// <summary>
        /// Show Text in YML
        /// </summary>
        public string ChargeNumber
        {
            get { return _chargeNumber; }
            set
            {
                if (_chargeNumber != value)
                {
                    _chargeNumber = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _chargeNumber = "";
        #endregion

        #region ---- RecipeCounter
        /// <summary>
        /// Show Text in YML
        /// </summary>
        public int RecipeCounter
        {
            get { return _recipeCounter; }
            set
            {
                if (_recipeCounter != value)
                {
                    _recipeCounter = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private int _recipeCounter;
        #endregion

        #region ---- RecipeInTolerance
        /// <summary>
        /// Show Text in YML
        /// </summary>
        public int RecipeInTolerance
        {
            get { return _recipeInTolerance; }
            set
            {
                if (_recipeInTolerance != value)
                {
                    _recipeInTolerance = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private int _recipeInTolerance;
        #endregion

        #region ---- RecipeOverDosed
        /// <summary>
        /// Show Text in YML
        /// </summary>
        public int RecipeOverDosed
        {
            get { return _recipeOverDosed; }
            set
            {
                if (_recipeOverDosed != value)
                {
                    _recipeOverDosed = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private int _recipeOverDosed;
        #endregion

        #region ---- RecipeInfoVisibility
        /// <summary>
        /// Show Text in YML
        /// </summary>
        public Visibility RecipeInfoVisibility
        {
            get { return _recipeInfoVisibility; }
            set
            {
                if (_recipeInfoVisibility != value)
                {
                    _recipeInfoVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private Visibility _recipeInfoVisibility = Visibility.Visible;
        #endregion

        #region ---- ChargeInfoVisibility
        /// <summary>
        /// Show Text in YML
        /// </summary>
        public Visibility ChargeInfoVisibility
        {
            get { return _chargeInfoVisibility; }
            set
            {
                if (_chargeInfoVisibility != value)
                {
                    _chargeInfoVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private Visibility _chargeInfoVisibility = Visibility.Visible;
        #endregion

        #region ---- DeltaTracVisibility
        /// <summary>
        /// Show Text in YML
        /// </summary>
        public Visibility DeltaTracVisibility
        {
            get { return _deltaTracVisibility; }
            set
            {
                if (_deltaTracVisibility != value)
                {
                    _deltaTracVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private Visibility _deltaTracVisibility = Visibility.Visible;
        #endregion

        #region ---- TextBoxVisibility
        /// <summary>
        /// Show Text in YML
        /// </summary>
        public Visibility MessageVisibility
        {
            get { return _messageVisibility; }
            set
            {
                if (_messageVisibility != value)
                {
                    _messageVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private Visibility _messageVisibility = Visibility.Visible;
        #endregion

        #region ---- BackButtonVisibility
        /// <summary>
        /// BackButtonVisibility
        /// </summary>
        public Visibility BackButtonVisibility
        {
            get { return _backButtonVisibility; }
            set
            {
                if (_backButtonVisibility != value)
                {
                    _backButtonVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private Visibility _backButtonVisibility = Visibility.Visible;
        #endregion

        #region ---- QuitButtonVisibility
        /// <summary>
        /// QuitButtonVisibility
        /// </summary>
        public Visibility QuitButtonVisibility
        {
            get { return _quitButtonVisibility; }
            set
            {
                if (_quitButtonVisibility != value)
                {
                    _quitButtonVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private Visibility _quitButtonVisibility = Visibility.Visible;
        #endregion

        #region ---- StartButtonVisibility
        /// <summary>
        /// StartButtonVisibility
        /// </summary>
        public Visibility StartButtonVisibility
        {
            get { return _startButtonVisibility; }
            set
            {
                if (_startButtonVisibility != value)
                {
                    _startButtonVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private Visibility _startButtonVisibility = Visibility.Visible;
        #endregion

        #region ---- PauseButtonVisibility
        /// <summary>
        /// PauseButtonVisibility
        /// </summary>
        public Visibility PauseButtonVisibility
        {
            get { return _pauseButtonVisibility; }
            set
            {
                if (_pauseButtonVisibility != value)
                {
                    _pauseButtonVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private Visibility _pauseButtonVisibility = Visibility.Visible;
        #endregion

        #region ---- ContinueButtonVisibility
        /// <summary>
        /// ContinueButtonVisibility
        /// </summary>
        public Visibility ContinueButtonVisibility
        {
            get { return _continueButtonVisibility; }
            set
            {
                if (_continueButtonVisibility != value)
                {
                    _continueButtonVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private Visibility _continueButtonVisibility = Visibility.Visible;
        #endregion

        #region ---- PrintButtonVisibility
        /// <summary>
        /// PrintButtonVisibility
        /// </summary>
        public Visibility PrintButtonVisibility
        {
            get { return _printButtonVisibility; }
            set
            {
                if (_printButtonVisibility != value)
                {
                    _printButtonVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private Visibility _printButtonVisibility = Visibility.Visible;
        #endregion

        #endregion
    }
}
