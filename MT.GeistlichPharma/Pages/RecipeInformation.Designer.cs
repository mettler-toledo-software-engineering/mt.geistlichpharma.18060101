﻿using MT.Singularity.Presentation.Controls;
namespace MT.GeistlichPharma.Pages
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.1.0.0")]
    public partial class RecipeInformation : MT.Singularity.Presentation.Controls.ChildWindow
    {
        private MT.Singularity.Presentation.Controls.TextBox txtChargeNumber;
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.GroupPanel internal1;
            MT.Singularity.Presentation.Controls.StackPanel internal2;
            MT.Singularity.Presentation.Controls.GroupPanel internal3;
            MT.Singularity.Presentation.Controls.StackPanel internal4;
            MT.Singularity.Presentation.Controls.TextBlock internal5;
            MT.Singularity.Presentation.Controls.TextBox internal6;
            MT.Singularity.Presentation.Controls.TextBlock internal7;
            MT.Singularity.Presentation.Controls.StackPanel internal8;
            MT.Singularity.Presentation.Controls.Button internal9;
            MT.Singularity.Presentation.Controls.GroupPanel internal10;
            MT.Singularity.Presentation.Controls.Image internal11;
            MT.Singularity.Presentation.Controls.TextBlock internal12;
            MT.Singularity.Presentation.Controls.Button internal13;
            MT.Singularity.Presentation.Controls.GroupPanel internal14;
            MT.Singularity.Presentation.Controls.Image internal15;
            MT.Singularity.Presentation.Controls.TextBlock internal16;
            internal5 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal5.Text = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.RecipeNr);
            internal5.AddTranslationAction(() => {
                internal5.Text = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.RecipeNr);
            });
            internal5.FontSize = ((System.Nullable<System.Int32>)30);
            internal6 = new MT.Singularity.Presentation.Controls.TextBox();
            internal6.Margin = new MT.Singularity.Presentation.Thickness(0, 0, 0, 20);
            internal6.FontSize = ((System.Nullable<System.Int32>)40);
            internal6.Width = 400;
            internal6.MaxLength = 30;
            internal6.KeyboardLayout = MT.Singularity.Presentation.Input.KeyboardLayout.Numeric;
            internal6.KeyboardTitle = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.RecipeNr);
            internal6.AddTranslationAction(() => {
                internal6.KeyboardTitle = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.RecipeNr);
            });
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal6.Text,() => _viewModel.RecipeNumber,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal7 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal7.Text = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.ChargrNumber);
            internal7.AddTranslationAction(() => {
                internal7.Text = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.ChargrNumber);
            });
            internal7.FontSize = ((System.Nullable<System.Int32>)30);
            txtChargeNumber = new MT.Singularity.Presentation.Controls.TextBox();
            txtChargeNumber.FontSize = ((System.Nullable<System.Int32>)40);
            txtChargeNumber.Width = 400;
            txtChargeNumber.MaxLength = 8;
            txtChargeNumber.KeyboardLayout = MT.Singularity.Presentation.Input.KeyboardLayout.Numeric;
            txtChargeNumber.KeyboardTitle = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.ChargrNumber);
            txtChargeNumber.AddTranslationAction(() => {
                txtChargeNumber.KeyboardTitle = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.ChargrNumber);
            });
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => txtChargeNumber.Text,() => _viewModel.ChargeNumber,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal4 = new MT.Singularity.Presentation.Controls.StackPanel(internal5, internal6, internal7, txtChargeNumber);
            internal4.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal4.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal3 = new MT.Singularity.Presentation.Controls.GroupPanel(internal4);
            internal9 = new MT.Singularity.Presentation.Controls.Button();
            internal9.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal9.Width = 180;
            internal9.Height = 90;
            internal9.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal9.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal11 = new MT.Singularity.Presentation.Controls.Image();
            internal11.Source = "embedded://MT.GeistlichPharma/MT.GeistlichPharma.Images.Cancel.al8";
            internal11.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal11.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal11.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal12 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal12.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal12.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal12.Text = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.Cancel);
            internal12.AddTranslationAction(() => {
                internal12.Text = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.Cancel);
            });
            internal12.FontSize = ((System.Nullable<System.Int32>)20);
            internal12.Margin = new MT.Singularity.Presentation.Thickness(1);
            internal10 = new MT.Singularity.Presentation.Controls.GroupPanel(internal11, internal12);
            internal9.Content = internal10;
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal9.Command,() => _viewModel.GoCancel,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal13 = new MT.Singularity.Presentation.Controls.Button();
            internal13.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal13.Width = 180;
            internal13.Height = 90;
            internal13.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal13.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal15 = new MT.Singularity.Presentation.Controls.Image();
            internal15.Source = "embedded://MT.GeistlichPharma/MT.GeistlichPharma.Images.ok.al8";
            internal15.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal15.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal15.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal16 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal16.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal16.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal16.Text = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.Ok);
            internal16.AddTranslationAction(() => {
                internal16.Text = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.Ok);
            });
            internal16.FontSize = ((System.Nullable<System.Int32>)20);
            internal16.Margin = new MT.Singularity.Presentation.Thickness(1);
            internal14 = new MT.Singularity.Presentation.Controls.GroupPanel(internal15, internal16);
            internal13.Content = internal14;
            this.bindings[3] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal13.Command,() =>  _viewModel.GoOk,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal8 = new MT.Singularity.Presentation.Controls.StackPanel(internal9, internal13);
            internal8.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal8.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal8.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal8.Margin = new MT.Singularity.Presentation.Thickness(0, 40, 0, 0);
            internal2 = new MT.Singularity.Presentation.Controls.StackPanel(internal3, internal8);
            internal2.Margin = new MT.Singularity.Presentation.Thickness(20);
            internal1 = new MT.Singularity.Presentation.Controls.GroupPanel(internal2);
            this.Content = internal1;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[4];
    }
}
