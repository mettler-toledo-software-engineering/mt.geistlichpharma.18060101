﻿using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls.Navigation;

namespace MT.GeistlichPharma.Pages
{
    /// <summary>
    /// Interaction logic for RecipeSelection
    /// </summary>
    public partial class RecipeSelection
    {
        private readonly RecipeSelectionViewModel _viewModel;

        public RecipeSelection(AnimatedNavigationFrame homeNavigationFrame)
        {
            _viewModel = new RecipeSelectionViewModel(this, homeNavigationFrame);
            InitializeComponents();
        }

        #region Overrides

        /// <summary>
        /// Called when a page is being navigated to for the first time.
        /// </summary>
        protected override void OnFirstNavigation()
        {
            HomeScreenViewModel.Instance.WeightDisplayShow = Visibility.Collapsed;
            base.OnFirstNavigation();
        }

        #endregion 
    }
}
