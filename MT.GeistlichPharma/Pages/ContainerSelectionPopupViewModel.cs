﻿using System;

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using MT.GeistlichPharma.Commands;
using MT.GeistlichPharma.Infrastructure;
using MT.GeistlichPharma.Logic;
using MT.GeistlichPharma.Logic.MTSettings;
using MT.Singularity.Data;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Model;

namespace MT.GeistlichPharma.Pages
{
    public class ContainerSelectionPopupViewModel : PropertyChangedBase, IScrollableViewModel
    {
        private readonly Visual _parent;

        public ContainerSelectionPopupViewModel(Visual parent)
        {
            _parent = parent;
            ScrollUpCommand = new ScrollUpCommand(this);
            ScrollDownCommand = new ScrollDownCommand(this);
            UpdateContainerList();
        }
        public void UpdateContainerList()
        {
            Container?.Clear();
            Container = new ObservableCollection<Container>(Settings.Current.Container.OrderBy(c => c.Name));
            SelectedItem = Container.FirstOrDefault();
        }

        public ICommand ScrollUpCommand { get; }
        public ICommand ScrollDownCommand { get; }

        private ObservableCollection<Container> _container;

        public ObservableCollection<Container> Container
        {
            get { return _container; }
            set
            {
                _container = value;
                NotifyPropertyChanged(nameof(Container));
                NotifyPropertyChanged(nameof(ListCount));
            }
        }

        private Container _selectedItem;

        public object SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = (Container)value;
                SelectedContainer = _selectedItem == null ? new Container() : _selectedItem.Clone();
                NotifyPropertyChanged(nameof(SelectedItem));
            }
        }

        private Container _selectedContainer;

        public Container SelectedContainer
        {
            get { return _selectedContainer; }
            set
            {
                _selectedContainer = value;
                NotifyPropertyChanged(nameof(SelectedContainer));
            }
        }

        private int _listIndex;
        public int ListIndex
        {
            get { return _listIndex; }
            set
            {
                _listIndex = value;
                NotifyPropertyChanged(nameof(ListIndex));
            }
        }
        public int ListCount => Container == null ? 0 : Container.Count;

        public ContainerSelectionDialogResult SelectionDialogResult = new ContainerSelectionDialogResult();

        public ICommand OkCommand
        {
            get { return new DelegateCommand(Ok); }
        }

        private void Ok()
        {
            SelectionDialogResult.DialogResult = DialogResult.OK;
            SelectionDialogResult.SelectedContainer = SelectedContainer;
            ((ContainerSelectionPopup)_parent).Close();
            
        }
        public ICommand CancelCommand
        {
            get { return new DelegateCommand(Cancel); }
        }

        private void Cancel()
        {
            SelectionDialogResult.DialogResult = DialogResult.Cancel;
            ((ContainerSelectionPopup)_parent).Close();
        }
    }
}
