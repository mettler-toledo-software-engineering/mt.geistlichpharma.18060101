﻿using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.DataGrid;
using MT.GeistlichPharma.Logic;
namespace MT.GeistlichPharma.Pages
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.1.0.0")]
    public partial class ContainerSelectionPopup : MT.Singularity.Presentation.Controls.ChildWindow
    {
        private MT.Singularity.Presentation.Controls.DataGrid.DataGrid _containerGrid;
        private MT.Singularity.Presentation.Controls.Button _listUpButton;
        private MT.Singularity.Presentation.Controls.Button _listDownButton;
        private MT.Singularity.Presentation.Controls.Button _okButton;
        private MT.Singularity.Presentation.Controls.Button _cancelButton;
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.GroupPanel internal1;
            MT.Singularity.Presentation.Controls.StackPanel internal2;
            MT.Singularity.Presentation.Controls.DataGrid.DataGridTextColumn internal3;
            MT.Singularity.Presentation.Controls.DataGrid.DataGridTextColumn internal4;
            MT.Singularity.Presentation.Controls.DataGrid.DataGridTextColumn internal5;
            MT.Singularity.Presentation.Controls.StackPanel internal6;
            MT.Singularity.Presentation.Controls.GroupPanel internal7;
            MT.Singularity.Presentation.Controls.Image internal8;
            MT.Singularity.Presentation.Controls.GroupPanel internal9;
            MT.Singularity.Presentation.Controls.Image internal10;
            MT.Singularity.Presentation.Controls.StackPanel internal11;
            MT.Singularity.Presentation.Controls.TextBlock internal12;
            MT.Singularity.Presentation.Controls.TextBlock internal13;
            internal3 = new MT.Singularity.Presentation.Controls.DataGrid.DataGridTextColumn();
            internal3.Header = "Bezeichnung";
            internal3.ColumnName = "Name";
            internal3.Width = 300;
            internal4 = new MT.Singularity.Presentation.Controls.DataGrid.DataGridTextColumn();
            internal4.Header = "Gewicht";
            internal4.ColumnName = "Weight";
            internal4.Width = 200;
            internal5 = new MT.Singularity.Presentation.Controls.DataGrid.DataGridTextColumn();
            internal5.Header = "Toleranz";
            internal5.ColumnName = "Tolerance";
            internal5.Width = 200;
            _containerGrid = new MT.Singularity.Presentation.Controls.DataGrid.DataGrid();
            _containerGrid.Columns.Add(internal3);
            _containerGrid.Columns.Add(internal4);
            _containerGrid.Columns.Add(internal5);
            _containerGrid.Margin = new MT.Singularity.Presentation.Thickness(5, 5, 5, 5);
            _containerGrid.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            _containerGrid.RowHeight = 40;
            _containerGrid.ColumnHeaderHeight = 40;
            _containerGrid.Height = 500;
            _containerGrid.FontSize = ((System.Nullable<System.Int32>)24);
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => _containerGrid.ItemsSource,() =>  ViewModel.Container,MT.Singularity.Expressions.BindingMode.OneWay,false);
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => _containerGrid.SelectedIndex,() =>  ViewModel.ListIndex,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => _containerGrid.SelectedItem,() =>  ViewModel.SelectedItem,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            _listUpButton = new MT.Singularity.Presentation.Controls.Button();
            _listUpButton.Margin = new MT.Singularity.Presentation.Thickness(5, 5, 10, 5);
            _listUpButton.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            _listUpButton.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal8 = new MT.Singularity.Presentation.Controls.Image();
            internal8.Source = Globals.ArrowUpImage;
            internal8.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal8.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal7 = new MT.Singularity.Presentation.Controls.GroupPanel(internal8);
            _listUpButton.Content = internal7;
            this.bindings[3] = MT.Singularity.Expressions.ExpressionBinding.Create(() => _listUpButton.Command,() =>  ViewModel.ScrollUpCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            _listDownButton = new MT.Singularity.Presentation.Controls.Button();
            _listDownButton.Margin = new MT.Singularity.Presentation.Thickness(10, 5, 5, 5);
            _listDownButton.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            _listDownButton.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal10 = new MT.Singularity.Presentation.Controls.Image();
            internal10.Source = Globals.ArrowDownImage;
            internal10.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal10.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal9 = new MT.Singularity.Presentation.Controls.GroupPanel(internal10);
            _listDownButton.Content = internal9;
            this.bindings[4] = MT.Singularity.Expressions.ExpressionBinding.Create(() => _listDownButton.Command,() =>  ViewModel.ScrollDownCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal6 = new MT.Singularity.Presentation.Controls.StackPanel(_listUpButton, _listDownButton);
            internal6.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal6.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal12 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal12.Text = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.Ok);
            internal12.AddTranslationAction(() => {
                internal12.Text = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.Ok);
            });
            internal12.FontSize = ((System.Nullable<System.Int32>)22);
            internal12.Margin = new MT.Singularity.Presentation.Thickness(4);
            _okButton = new MT.Singularity.Presentation.Controls.Button();
            _okButton.Content = internal12;
            _okButton.Margin = new MT.Singularity.Presentation.Thickness(4);
            _okButton.Width = 180;
            _okButton.Height = 60;
            _okButton.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            _okButton.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            this.bindings[5] = MT.Singularity.Expressions.ExpressionBinding.Create(() => _okButton.Command,() =>  ViewModel.OkCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal13 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal13.Text = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.Cancel);
            internal13.AddTranslationAction(() => {
                internal13.Text = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.Cancel);
            });
            internal13.FontSize = ((System.Nullable<System.Int32>)22);
            internal13.Margin = new MT.Singularity.Presentation.Thickness(4);
            _cancelButton = new MT.Singularity.Presentation.Controls.Button();
            _cancelButton.Content = internal13;
            _cancelButton.Margin = new MT.Singularity.Presentation.Thickness(4);
            _cancelButton.Width = 180;
            _cancelButton.Height = 60;
            _cancelButton.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            _cancelButton.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            this.bindings[6] = MT.Singularity.Expressions.ExpressionBinding.Create(() => _cancelButton.Command,() =>  ViewModel.CancelCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal11 = new MT.Singularity.Presentation.Controls.StackPanel(_okButton, _cancelButton);
            internal11.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal11.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal2 = new MT.Singularity.Presentation.Controls.StackPanel(_containerGrid, internal6, internal11);
            internal2.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal1 = new MT.Singularity.Presentation.Controls.GroupPanel(internal2);
            internal1.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal1.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            this.Content = internal1;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[7];
    }
}
