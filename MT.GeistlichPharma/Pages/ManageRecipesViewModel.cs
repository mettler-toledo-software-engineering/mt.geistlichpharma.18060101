﻿using System;

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using MT.GeistlichPharma.Commands;
using MT.GeistlichPharma.Infrastructure;
using MT.GeistlichPharma.Logic.MTSettings;
using MT.Singularity.Data;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Model;

namespace MT.GeistlichPharma.Pages
{
    public class ManageRecipesViewModel : PropertyChangedBase, IScrollableViewModel
    {
        private readonly AnimatedNavigationFrame _homeNavigationFrame;
        private readonly ContainerSelectionPopup _containerSelectionPopup;
        private readonly Visual _parent;
        public ICommand ScrollUpCommand { get; }
        public ICommand ScrollDownCommand { get; }
        public ICommand CreateNewRecipeCommand { get; }
        public ICommand UpdateRecipeCommand { get; }
        public ICommand DeleteRecipeCommand { get; }


        public ManageRecipesViewModel(Visual parent, AnimatedNavigationFrame homeNavigationFrame)
        {
            _parent = parent;
            _homeNavigationFrame = homeNavigationFrame;
            
            UpdateRecipeList();
            ScrollUpCommand = new ScrollUpCommand(this);
            ScrollDownCommand = new ScrollDownCommand(this);
            CreateNewRecipeCommand = new CreateNewRecipeCommand(this);
            UpdateRecipeCommand = new UpdateRecipeCommand(this, parent);
            DeleteRecipeCommand = new DeleteRecipeCommand(this, parent);
            SelectedItem = Recipes?[0];
            _showContainerCommand = new DelegateCommand(ShowContainer, CanShowContainer);
            _containerSelectionPopup = new ContainerSelectionPopup();
            _containerSelectionPopup.Closed += ContainerSelectionPopupOnClosed;
        }

        private void ContainerSelectionPopupOnClosed(object sender, EventArgs e)
        {

            var result = ((ContainerSelectionPopup)sender).ViewModel.SelectionDialogResult;

            if (result.DialogResult == DialogResult.OK)
            {
                SelectedRecipe.Container = result.SelectedContainer;
                //UpdateRecipeCommand.Execute(null);
                //Settings.Current.UpdateRecipe(SelectedRecipe);
                NotifyPropertyChanged(nameof(ContainerName));
                
            }
        }

        public void UpdateRecipeList()
        {
            Recipes?.Clear();
            Recipes = new ObservableCollection<Recipe>(Settings.Current.Recipes.OrderBy(r => r.Number));
        }

        private ObservableCollection<Recipe> _recipes;

        public ObservableCollection<Recipe> Recipes
        {
            get { return _recipes; }
            set
            {
                _recipes = value;
                NotifyPropertyChanged(nameof(Recipes));
                NotifyPropertyChanged(nameof(ListCount));
            }
        }

        private int _listIndex;

        public int ListIndex
        {
            get { return _listIndex; }
            set
            {
                _listIndex = value;
                NotifyPropertyChanged(nameof(ListIndex));
            }
        }

        public int ListCount => Recipes == null ? 0 : Recipes.Count;

        private Recipe _selectedItem;

        public object SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = (Recipe)value;
                SelectedRecipe = _selectedItem == null ? new Recipe() : _selectedItem.Clone();
                NotifyPropertyChanged(nameof(SelectedItem));
            }
        }

        private Recipe _selectedRecipe;

        public Recipe SelectedRecipe
        {
            get { return _selectedRecipe; }
            set
            {
                _selectedRecipe = value;
                NotifyPropertyChanged(nameof(SelectedRecipe));
                NotifyPropertyChanged(nameof(Number));
                NotifyPropertyChanged(nameof(Description));
                NotifyPropertyChanged(nameof(Size));
                NotifyPropertyChanged(nameof(Target));
                NotifyPropertyChanged(nameof(AcceptanceMin));
                NotifyPropertyChanged(nameof(AcceptanceMax));
                NotifyPropertyChanged(nameof(Limit1));
                NotifyPropertyChanged(nameof(Limit2));
                NotifyPropertyChanged(nameof(LastModifiedFormatted));
                NotifyPropertyChanged(nameof(ContainerName));
                _showContainerCommand?.NotifyCanExecuteChanged();
            }
        }

        public string ContainerName
        {
            get { return SelectedRecipe?.Container?.Name ?? "kein Behälter"; }
        }

        public string Number
        {
            get { return SelectedRecipe?.Number ?? string.Empty; }
            set
            {
                SelectedRecipe.Number = value;
                NotifyPropertyChanged(nameof(Number));
            }
        }

        public string Description
        {
            get { return SelectedRecipe?.Description ?? string.Empty; }
            set
            {
                SelectedRecipe.Description = value;
                NotifyPropertyChanged(nameof(Description));
            }
        }
        public string Size
        {
            get { return SelectedRecipe?.Size ?? string.Empty; }
            set
            {
                SelectedRecipe.Size = value;
                NotifyPropertyChanged(nameof(Size));
            }
        }
        public double Target
        {
            get { return SelectedRecipe?.Target ?? 0; }
            set
            {
                SelectedRecipe.Target = value;
                NotifyPropertyChanged(nameof(Target));
            }
        }
        public double AcceptanceMin
        {
            get { return SelectedRecipe?.AcceptanceMin ?? 0; }
            set
            {
                SelectedRecipe.AcceptanceMin = value;
                NotifyPropertyChanged(nameof(AcceptanceMin));
            }
        }
        public double AcceptanceMax
        {
            get { return SelectedRecipe?.AcceptanceMax ?? 0; }
            set
            {
                SelectedRecipe.AcceptanceMax = value;
                NotifyPropertyChanged(nameof(AcceptanceMax));
            }
        }
        public double Limit1
        {
            get { return SelectedRecipe?.Limit1 ?? 0; }
            set
            {
                SelectedRecipe.Limit1 = value;
                NotifyPropertyChanged(nameof(Limit1));
            }
        }
        public double Limit2
        {
            get { return SelectedRecipe?.Limit2 ?? 0; }
            set
            {
                SelectedRecipe.Limit2 = value;
                NotifyPropertyChanged(nameof(Limit2));
            }
        }

        public string LastModifiedFormatted
        {
            get {  return SelectedRecipe?.LastModifiedFormatted ?? string.Empty; }
        }



        #region KeyHandler

        #region ---- Cancel
        /// <summary>
        /// GoCancel
        /// </summary>
        public ICommand GoCancel
        {
            get { return new DelegateCommand(DoGoCancel); }
        }

        private void DoGoCancel()
        {
            _homeNavigationFrame.Back();
        }
        #endregion

        private readonly DelegateCommand _showContainerCommand;
        public ICommand ShowContainerCommand
        {
            get { return _showContainerCommand; } 
        }

        private bool CanShowContainer()
        {
            return SelectedRecipe != null;
        }

        private void ShowContainer()
        {
            _containerSelectionPopup.Show(_parent);
        }

        #endregion

    }
}
