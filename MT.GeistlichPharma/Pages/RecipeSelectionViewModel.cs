﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using MT.GeistlichPharma.Commands;
using MT.GeistlichPharma.Controlls;
using MT.GeistlichPharma.Logic.MTSettings;
using MT.GeistlichPharma.Logic.Process;
using MT.GeistlichPharma.Stores;
using MT.Singularity.Composition;
using MT.Singularity.Data;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Model;

namespace MT.GeistlichPharma.Pages
{
    public class RecipeSelectionViewModel : PropertyChangedBase
    {
        public Visual Parent { get; }
        public AnimatedNavigationFrame HomeNavigationFrame { get; }


        public Rapport Rapport { get; set; }

        public ICommand ScrollUpCommand { get; }
        public ICommand ScrollDownCommand { get; }
        public ICommand SelectRecipeCommand { get; }

        public int MaxNumberOfElements => 16;
        public int NumberOfElements => (int)(AllRecipes?.Count < PanelIndex + MaxNumberOfElements ? AllRecipes?.Count - PanelIndex : MaxNumberOfElements);
        public int PanelIndex { get; set; } = 0;
        private readonly ProcessStore _processStore = ApplicationBootstrapperBase.CompositionContainer.Resolve<ProcessStore>();

        public RecipeSelectionViewModel(Visual parent, AnimatedNavigationFrame homeNavigationFrame)
        {
            Parent = parent;
            HomeNavigationFrame = homeNavigationFrame;
            ScrollUpCommand = new ScrollUpRecipePanelCommand(this);
            ScrollDownCommand = new ScrollDownRecipePanelCommand(this);
            SelectRecipeCommand = new SelectRecipeCommand(this, _processStore);
            Rapport = new Rapport();

            GetRecipeInformation();
        }

        private ObservableCollection<Recipe> _recipes;

        public ObservableCollection<Recipe> Recipes
        {
            get { return _recipes; }
            set
            {
                _recipes = value;
                NotifyPropertyChanged(nameof(Recipes));
            }
        }

        public ObservableCollection<Recipe> AllRecipes { get; set; } = new ObservableCollection<Recipe>();





        private Recipe _selectedRecipe;

        public Recipe SelectedRecipe
        {
            get { return _selectedRecipe; }
            set
            {
                _selectedRecipe = value;
                NotifyPropertyChanged(nameof(SelectedRecipe));

            }
        }

        private string _recipeNumber;

        public string RecipeNumber
        {
            get { return _recipeNumber; }
            set
            {
                _recipeNumber = value;
                NotifyPropertyChanged(nameof(RecipeNumber));
            }
        }

        private string _charge;

        public string Charge
        {
            get { return _charge; }
            set
            {
                _charge = value;
                NotifyPropertyChanged(nameof(Charge));
            }
        }

        private void GetRecipeInformation()
        {


            if (Settings.Current.Recipes.Count > 0)
            {
                AllRecipes = new ObservableCollection<Recipe>(Settings.Current.Recipes.OrderBy(r => r.Number));

                Recipes = new ObservableCollection<Recipe>(AllRecipes.ToList().GetRange(PanelIndex, NumberOfElements));

            }
            else
            {
                var info = new InfoPopup(Localization.Get(Localization.Key.SearchRecipe),
                    String.Format(Localization.Get(Localization.Key.NoRecipesFound), Settings.FileName),
                    MessageBoxIcon.Warning, MessageBoxButtons.OK);
                info.Show(Parent);
            }
        }



 

        #region RecipeLastChanged
        /// <summary>
        /// Gets or sets the RecipeLastChanged.
        /// </summary>
        /// <value>
        /// The RecipeItems.
        /// </value>
        public string RecipeLastChanged
        {
            get
            {
                return _recipeLastChanged;
            }
            set
            {
                if (value != _recipeLastChanged)
                {
                    _recipeLastChanged = value;
                    NotifyPropertyChanged();
                }
            }
        }
        private string _recipeLastChanged;
        #endregion

        #region RecipeVersion
        /// <summary>
        /// Gets or sets the RecipeVersion.
        /// </summary>
        /// <value>
        /// The RecipeItems.
        /// </value>
        public string RecipeVersion
        {
            get
            {
                return _recipeVersion;
            }
            set
            {
                if (value != _recipeVersion)
                {
                    _recipeVersion = value;
                    NotifyPropertyChanged();
                }
            }
        }
        private string _recipeVersion;
        #endregion

        #region KeyHandler

        #region ---- Cancel
        /// <summary>
        /// GoCancel
        /// </summary>
        public ICommand GoCancel
        {
            get { return new DelegateCommand(DoGoCancel); }
        }

        private void DoGoCancel()
        {
            HomeNavigationFrame.Back();
        }
        #endregion

        #endregion
    }
}
