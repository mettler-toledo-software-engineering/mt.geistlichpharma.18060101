﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using MT.Singularity;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;

namespace MT.GeistlichPharma.Pages
{
    /// <summary>
    /// Interaction logic for ContainerSelectionPopup
    /// </summary>
    public partial class ContainerSelectionPopup
    {

        public readonly ContainerSelectionPopupViewModel ViewModel;
        public ContainerSelectionPopup()
        {
            ViewModel = new ContainerSelectionPopupViewModel(this);
            InitializeComponents();
        }
    }
}
