﻿using System;
using System.Diagnostics;
using System.Resources;
using MT.GeistlichPharma.Stores;
using MT.Singularity.Composition;
using MT.Singularity.Data;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Presentation.Model;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Presentation;
using MT.Singularity.Platform.Devices;
using MT.Singularity.Logging;


namespace MT.GeistlichPharma.Pages
{
    /// <summary>
    /// Home Screen view model implementation
    /// </summary>
    public class HomeScreenViewModel : PropertyChangedBase
    {
        /// <summary>
        /// Constructor Initializes a new instance of the <see cref="HomeScreenViewModel"/> class.
        /// </summary>
        /// 

        private IInterfaceService _interfaces;
        public static HomeScreenViewModel Instance;
        private IScaleService _scaleService;
        
        public IScale Scale;
        public IDigitalIOInterface DigitalIo;
        private int _currentScaleNr = 0;


        public HomeScreenViewModel()
        {
            Instance = this;
            InitializeViewModel();
        }

        public void SwitchScale()
        {
            if (_currentScaleNr == 0)
            {
                _currentScaleNr = 1;
            }
            else
            {
                _currentScaleNr = 0;
            }
            _scaleService.SwitchSelectedScaleAsync(_currentScaleNr);
        }

        private async void InitializeViewModel()
        {
            var compositionContainer = ApplicationBootstrapperBase.CompositionContainer;
            var platformEngine = compositionContainer.Resolve<IPlatformEngine>();


            _interfaces = await platformEngine.GetInterfaceServiceAsync();

            _scaleService = await platformEngine.GetScaleServiceAsync();
            if (_scaleService == null)
                return;
            


            Scale = await _scaleService.GetScaleAsync(1);
            SearchDigitalIoInterface();
        }

        public async void InitializeScales()
        {
            var compositionContainer = ApplicationBootstrapperBase.CompositionContainer;

            var platformEngine = compositionContainer.Resolve<IPlatformEngine>();
            await platformEngine.GetScaleServiceAsync();
        }

        private async void SearchDigitalIoInterface()
        {
            for (int i = 1; i <= 7; i++)
            {
                try
                {
                    DigitalIo = await _interfaces.GetDigitalIOInterfaceAsync(i);
                    if (DigitalIo != null)
                        break;
                }
                catch (Exception ex)
                {
                    Log4NetManager.ApplicationLogger.Error("Error on initializing Digital IO:\n" + ex.Message);
                    Debug.WriteLine(ex.Message);
                }
            }

            if (DigitalIo == null)
                Log4NetManager.ApplicationLogger.Warning("No Digital IO found");
        }

        #region Visibility
        private Visibility _statusBarVisibility = Visibility.Collapsed;

        public Visibility StatusBarVisibility
        {
            get { return _statusBarVisibility; }
            set
            {
                if (_statusBarVisibility != value)
                {
                    _statusBarVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private Visibility _weightDisplayShow;

        public Visibility WeightDisplayShow
        {
            get { return _weightDisplayShow; }
            set
            {
                if (_weightDisplayShow != value)
                {
                    _weightDisplayShow = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion 
    }
}