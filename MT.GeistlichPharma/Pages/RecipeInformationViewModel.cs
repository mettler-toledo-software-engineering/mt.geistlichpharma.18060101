﻿using System;
using System.Collections.Generic;
using System.Text;
using MT.Singularity.Data;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Model;

namespace MT.GeistlichPharma.Pages
{
    public class RecipeInformationViewModel : PropertyChangedBase
    {
        private Visual _parent;
        private RecipeSelectionViewModel _recipeSelectionViewModel;

        public RecipeInformationViewModel(Visual parent, RecipeSelectionViewModel recipeSelectionViewModel)
        {
            _parent = parent;
            _recipeSelectionViewModel = recipeSelectionViewModel;
            _recipeSelectionViewModel.RecipeNumber = string.Empty;
            _recipeSelectionViewModel.Charge = string.Empty;
        }


        public string RecipeNumber
        {
            get
            {
                return _recipeSelectionViewModel.RecipeNumber;
            }
            set
            {
                if (_recipeSelectionViewModel.RecipeNumber != value)
                {
                    _recipeSelectionViewModel.RecipeNumber = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public string ChargeNumber
        {
            get
            {
                return _recipeSelectionViewModel.Charge;
            }
            set
            {
                if (_recipeSelectionViewModel.Charge != value)
                {
                    _recipeSelectionViewModel.Charge = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public int ChargeNumberLength => ChargeNumber.ToString().Length;
        public int ChargeNumberMaxLength => 30;

        #region KeyHandler

        #region ---- Cancel
        /// <summary>
        /// GoCancel
        /// </summary>
        public ICommand GoCancel
        {
            get { return new DelegateCommand(DoGoCancel); }
        }

        private void DoGoCancel()
        {
            ((RecipeInformation)_parent).Close();
        }
        #endregion

        #region ---- Ok
        /// <summary>
        /// GoCancel
        /// </summary>
        public ICommand GoOk
        {
            get { return new DelegateCommand(DoGoOk); }
        }

        private void DoGoOk()
        {
            ((RecipeInformation)_parent).DialogResult = DialogResult.OK;
            DoGoCancel();
        }
        #endregion

        #endregion
    }
}
