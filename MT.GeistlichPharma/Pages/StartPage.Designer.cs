﻿using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Platform.CommonUX.Controls;
using MT.Singularity.Platform.CommonUX.Controls.MeasurementDisplay.View;
using MT.GeistlichPharma.Logic;
namespace MT.GeistlichPharma.Pages
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.1.0.0")]
    public partial class StartPage : MT.Singularity.Presentation.Controls.Navigation.NavigationPage
    {
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.GroupPanel internal1;
            MT.Singularity.Presentation.Controls.DockPanel internal2;
            MT.Singularity.Presentation.Controls.DockPanel internal3;
            MT.Singularity.Presentation.Controls.StackPanel internal4;
            MT.Singularity.Presentation.Controls.Image internal5;
            MT.Singularity.Presentation.Controls.TextBlock internal6;
            MT.Singularity.Presentation.Controls.DockPanel internal7;
            MT.Singularity.Presentation.Controls.GroupPanel internal8;
            MT.Singularity.Presentation.Controls.StackPanel internal9;
            MT.Singularity.Presentation.Controls.Button internal10;
            MT.Singularity.Presentation.Controls.GroupPanel internal11;
            MT.Singularity.Presentation.Controls.Image internal12;
            MT.Singularity.Presentation.Controls.TextBlock internal13;
            MT.Singularity.Presentation.Controls.Button internal14;
            MT.Singularity.Presentation.Controls.GroupPanel internal15;
            MT.Singularity.Presentation.Controls.Image internal16;
            MT.Singularity.Presentation.Controls.TextBlock internal17;
            MT.Singularity.Presentation.Controls.Button internal18;
            MT.Singularity.Presentation.Controls.GroupPanel internal19;
            MT.Singularity.Presentation.Controls.Image internal20;
            MT.Singularity.Presentation.Controls.TextBlock internal21;
            MT.Singularity.Presentation.Controls.StackPanel internal22;
            MT.Singularity.Presentation.Controls.Button internal23;
            MT.Singularity.Presentation.Controls.GroupPanel internal24;
            MT.Singularity.Presentation.Controls.Image internal25;
            MT.Singularity.Presentation.Controls.TextBlock internal26;
            MT.Singularity.Presentation.Controls.TextBlock internal27;
            MT.Singularity.Presentation.Controls.Button internal28;
            MT.Singularity.Presentation.Controls.GroupPanel internal29;
            MT.Singularity.Presentation.Controls.Image internal30;
            MT.Singularity.Presentation.Controls.TextBlock internal31;
            MT.Singularity.Presentation.Controls.TextBlock internal32;
            MT.Singularity.Presentation.Controls.Button internal33;
            MT.Singularity.Presentation.Controls.GroupPanel internal34;
            MT.Singularity.Presentation.Controls.Image internal35;
            MT.Singularity.Presentation.Controls.TextBlock internal36;
            MT.Singularity.Presentation.Controls.TextBlock internal37;
            MT.Singularity.Presentation.Controls.Button internal38;
            MT.Singularity.Presentation.Controls.GroupPanel internal39;
            MT.Singularity.Presentation.Controls.Image internal40;
            MT.Singularity.Presentation.Controls.TextBlock internal41;
            MT.Singularity.Presentation.Controls.TextBlock internal42;
            MT.Singularity.Presentation.Controls.Button internal43;
            MT.Singularity.Presentation.Controls.GroupPanel internal44;
            MT.Singularity.Presentation.Controls.Image internal45;
            MT.Singularity.Presentation.Controls.TextBlock internal46;
            MT.Singularity.Presentation.Controls.TextBlock internal47;
            MT.Singularity.Presentation.Controls.Button internal48;
            MT.Singularity.Presentation.Controls.GroupPanel internal49;
            MT.Singularity.Presentation.Controls.Image internal50;
            MT.Singularity.Presentation.Controls.TextBlock internal51;
            MT.Singularity.Presentation.Controls.TextBlock internal52;
            MT.Singularity.Presentation.Controls.StackPanel internal53;
            MT.Singularity.Presentation.Controls.Button internal54;
            MT.Singularity.Presentation.Controls.GroupPanel internal55;
            MT.Singularity.Presentation.Controls.Image internal56;
            MT.Singularity.Presentation.Controls.TextBlock internal57;
            MT.Singularity.Presentation.Controls.TextBlock internal58;
            internal5 = new MT.Singularity.Presentation.Controls.Image();
            internal5.Margin = new MT.Singularity.Presentation.Thickness(0, 0, 0, 0);
            internal5.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal5.Source = "embedded://MT.GeistlichPharma/MT.GeistlichPharma.Images.logo.png";
            internal6 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal6.FontSize = ((System.Nullable<System.Int32>)28);
            internal6.Margin = new MT.Singularity.Presentation.Thickness(10, 50, 10, 10);
            internal6.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal6.Text = "Version 1.0.0";
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal6.Text,() => _viewModel.Version,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal4 = new MT.Singularity.Presentation.Controls.StackPanel(internal5, internal6);
            internal4.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4294967295u));
            internal4.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal4.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal4.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal3 = new MT.Singularity.Presentation.Controls.DockPanel(internal4);
            internal3.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal10 = new MT.Singularity.Presentation.Controls.Button();
            internal10.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal10.Width = Globals.ButtonWidth;
            internal10.Height = 90;
            internal10.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal10.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal12 = new MT.Singularity.Presentation.Controls.Image();
            internal12.Source = "embedded://MT.GeistlichPharma/MT.GeistlichPharma.Images.Zero.al8";
            internal12.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal12.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal12.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal13 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal13.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal13.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal13.Text = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.Zero);
            internal13.AddTranslationAction(() => {
                internal13.Text = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.Zero);
            });
            internal13.FontSize = ((System.Nullable<System.Int32>)20);
            internal13.Margin = new MT.Singularity.Presentation.Thickness(1);
            internal11 = new MT.Singularity.Presentation.Controls.GroupPanel(internal12, internal13);
            internal10.Content = internal11;
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal10.Command,() => _viewModel.GoZero,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal14 = new MT.Singularity.Presentation.Controls.Button();
            internal14.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal14.Width = Globals.ButtonWidth;
            internal14.Height = 90;
            internal14.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal14.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal16 = new MT.Singularity.Presentation.Controls.Image();
            internal16.Source = "embedded://MT.GeistlichPharma/MT.GeistlichPharma.Images.Tare.al8";
            internal16.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal16.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal16.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal17 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal17.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal17.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal17.Text = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.Tare);
            internal17.AddTranslationAction(() => {
                internal17.Text = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.Tare);
            });
            internal17.FontSize = ((System.Nullable<System.Int32>)20);
            internal17.Margin = new MT.Singularity.Presentation.Thickness(1);
            internal15 = new MT.Singularity.Presentation.Controls.GroupPanel(internal16, internal17);
            internal14.Content = internal15;
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal14.Command,() => _viewModel.GoTare,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal18 = new MT.Singularity.Presentation.Controls.Button();
            internal18.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal18.Width = Globals.ButtonWidth;
            internal18.Height = 90;
            internal18.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal18.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal20 = new MT.Singularity.Presentation.Controls.Image();
            internal20.Source = "embedded://MT.GeistlichPharma/MT.GeistlichPharma.Images.ClearTare.al8";
            internal20.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal20.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal20.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal21 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal21.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal21.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal21.Text = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.ClearTare);
            internal21.AddTranslationAction(() => {
                internal21.Text = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.ClearTare);
            });
            internal21.FontSize = ((System.Nullable<System.Int32>)20);
            internal21.Margin = new MT.Singularity.Presentation.Thickness(1);
            internal19 = new MT.Singularity.Presentation.Controls.GroupPanel(internal20, internal21);
            internal18.Content = internal19;
            this.bindings[3] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal18.Command,() => _viewModel.GoClearTare,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal9 = new MT.Singularity.Presentation.Controls.StackPanel(internal10, internal14, internal18);
            internal9.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal9.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal23 = new MT.Singularity.Presentation.Controls.Button();
            internal23.Margin = new MT.Singularity.Presentation.Thickness(380, 4, 4, 4);
            internal23.Width = Globals.ButtonWidth;
            internal23.Height = 90;
            internal23.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal23.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal25 = new MT.Singularity.Presentation.Controls.Image();
            internal25.Source = "embedded://MT.GeistlichPharma/MT.GeistlichPharma.Images.Start.al8";
            internal25.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal25.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal26 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal26.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal26.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal26.Text = "Grobdosierung";
            internal26.Text = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.CoarseDosing);
            internal26.AddTranslationAction(() => {
                internal26.Text = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.CoarseDosing);
            });
            internal26.FontSize = ((System.Nullable<System.Int32>)20);
            internal26.Margin = new MT.Singularity.Presentation.Thickness(0, 20, 0, 0);
            internal27 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal27.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal27.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal27.Text = "Starten";
            internal27.Text = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.Start);
            internal27.AddTranslationAction(() => {
                internal27.Text = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.Start);
            });
            internal27.FontSize = ((System.Nullable<System.Int32>)20);
            internal27.Margin = new MT.Singularity.Presentation.Thickness(1);
            internal24 = new MT.Singularity.Presentation.Controls.GroupPanel(internal25, internal26, internal27);
            internal23.Content = internal24;
            this.bindings[4] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal23.Visibility,() => _viewModel.ShowCoarseDosing,MT.Singularity.Expressions.BindingMode.OneWay,false);
            this.bindings[5] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal23.Command,() => _viewModel.GoCoarseDosing,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal28 = new MT.Singularity.Presentation.Controls.Button();
            internal28.Margin = new MT.Singularity.Presentation.Thickness(380, 4, 4, 4);
            internal28.Width = Globals.ButtonWidth;
            internal28.Height = 90;
            internal28.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal28.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal30 = new MT.Singularity.Presentation.Controls.Image();
            internal30.Source = "embedded://MT.GeistlichPharma/MT.GeistlichPharma.Images.Pause.al8";
            internal30.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal30.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal31 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal31.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal31.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal31.Text = "Grobdosierung";
            internal31.Text = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.CoarseDosing);
            internal31.AddTranslationAction(() => {
                internal31.Text = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.CoarseDosing);
            });
            internal31.FontSize = ((System.Nullable<System.Int32>)20);
            internal31.Margin = new MT.Singularity.Presentation.Thickness(0, 20, 0, 0);
            internal32 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal32.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal32.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal32.Text = "Stoppen";
            internal32.Text = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.Stop);
            internal32.AddTranslationAction(() => {
                internal32.Text = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.Stop);
            });
            internal32.FontSize = ((System.Nullable<System.Int32>)20);
            internal32.Margin = new MT.Singularity.Presentation.Thickness(1);
            internal29 = new MT.Singularity.Presentation.Controls.GroupPanel(internal30, internal31, internal32);
            internal28.Content = internal29;
            internal28.Visibility = MT.Singularity.Presentation.Visibility.Collapsed;
            this.bindings[6] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal28.Visibility,() => _viewModel.ShowStopCoarseDosing,MT.Singularity.Expressions.BindingMode.OneWay,false);
            this.bindings[7] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal28.Command,() => _viewModel.GoStopCoarseDosing,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal33 = new MT.Singularity.Presentation.Controls.Button();
            internal33.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal33.Width = Globals.ButtonWidth;
            internal33.Height = 90;
            internal33.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal33.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal35 = new MT.Singularity.Presentation.Controls.Image();
            internal35.Source = "embedded://MT.GeistlichPharma/MT.GeistlichPharma.Images.Start.al8";
            internal35.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal35.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal36 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal36.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal36.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal36.Text = "Feindosierung";
            internal36.Text = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.PreciseDosing);
            internal36.AddTranslationAction(() => {
                internal36.Text = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.PreciseDosing);
            });
            internal36.FontSize = ((System.Nullable<System.Int32>)20);
            internal36.Margin = new MT.Singularity.Presentation.Thickness(0, 20, 0, 0);
            internal37 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal37.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal37.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal37.Text = "Starten";
            internal37.Text = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.Start);
            internal37.AddTranslationAction(() => {
                internal37.Text = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.Start);
            });
            internal37.FontSize = ((System.Nullable<System.Int32>)20);
            internal37.Margin = new MT.Singularity.Presentation.Thickness(1);
            internal34 = new MT.Singularity.Presentation.Controls.GroupPanel(internal35, internal36, internal37);
            internal33.Content = internal34;
            this.bindings[8] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal33.Visibility,() => _viewModel.ShowPreciseDosing,MT.Singularity.Expressions.BindingMode.OneWay,false);
            this.bindings[9] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal33.Command,() => _viewModel.GoPreciseDosing,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal38 = new MT.Singularity.Presentation.Controls.Button();
            internal38.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal38.Width = Globals.ButtonWidth;
            internal38.Height = 90;
            internal38.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal38.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal40 = new MT.Singularity.Presentation.Controls.Image();
            internal40.Source = "embedded://MT.GeistlichPharma/MT.GeistlichPharma.Images.BeakerNew.al8";
            internal40.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal40.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal41 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal41.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal41.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal41.Text = "Rezepte";
            internal41.FontSize = ((System.Nullable<System.Int32>)20);
            internal41.Margin = new MT.Singularity.Presentation.Thickness(0, 20, 0, 0);
            internal42 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal42.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal42.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal42.Text = "Verwalten";
            internal42.FontSize = ((System.Nullable<System.Int32>)20);
            internal42.Margin = new MT.Singularity.Presentation.Thickness(1);
            internal39 = new MT.Singularity.Presentation.Controls.GroupPanel(internal40, internal41, internal42);
            internal38.Content = internal39;
            this.bindings[10] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal38.Command,() => _viewModel.GoRecipeScreenCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal43 = new MT.Singularity.Presentation.Controls.Button();
            internal43.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal43.Width = Globals.ButtonWidth;
            internal43.Height = 90;
            internal43.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal43.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal45 = new MT.Singularity.Presentation.Controls.Image();
            internal45.Source = "embedded://MT.GeistlichPharma/MT.GeistlichPharma.Images.BeakerNew.al8";
            internal45.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal45.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal46 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal46.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal46.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal46.Text = "Behälter";
            internal46.FontSize = ((System.Nullable<System.Int32>)20);
            internal46.Margin = new MT.Singularity.Presentation.Thickness(0, 20, 0, 0);
            internal47 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal47.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal47.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal47.Text = "Verwalten";
            internal47.FontSize = ((System.Nullable<System.Int32>)20);
            internal47.Margin = new MT.Singularity.Presentation.Thickness(1);
            internal44 = new MT.Singularity.Presentation.Controls.GroupPanel(internal45, internal46, internal47);
            internal43.Content = internal44;
            this.bindings[11] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal43.Command,() => _viewModel.GoContainerScreenCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal48 = new MT.Singularity.Presentation.Controls.Button();
            internal48.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal48.Width = Globals.ButtonWidth;
            internal48.Height = 90;
            internal48.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal48.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal50 = new MT.Singularity.Presentation.Controls.Image();
            internal50.Source = "embedded://MT.GeistlichPharma/MT.GeistlichPharma.Images.Pause.al8";
            internal50.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal50.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal51 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal51.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal51.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal51.Text = "Feindosierung";
            internal51.Text = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.PreciseDosing);
            internal51.AddTranslationAction(() => {
                internal51.Text = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.PreciseDosing);
            });
            internal51.FontSize = ((System.Nullable<System.Int32>)20);
            internal51.Margin = new MT.Singularity.Presentation.Thickness(0, 20, 0, 0);
            internal52 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal52.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal52.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal52.Text = "Stoppen";
            internal52.Text = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.Stop);
            internal52.AddTranslationAction(() => {
                internal52.Text = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.Stop);
            });
            internal52.FontSize = ((System.Nullable<System.Int32>)20);
            internal52.Margin = new MT.Singularity.Presentation.Thickness(1);
            internal49 = new MT.Singularity.Presentation.Controls.GroupPanel(internal50, internal51, internal52);
            internal48.Content = internal49;
            internal48.Visibility = MT.Singularity.Presentation.Visibility.Collapsed;
            this.bindings[12] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal48.Visibility,() => _viewModel.ShowStopPreciseDosing,MT.Singularity.Expressions.BindingMode.OneWay,false);
            this.bindings[13] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal48.Command,() => _viewModel.GoStopPreciseDosing,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal22 = new MT.Singularity.Presentation.Controls.StackPanel(internal23, internal28, internal33, internal38, internal43, internal48);
            internal22.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal22.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal54 = new MT.Singularity.Presentation.Controls.Button();
            internal54.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal54.Width = Globals.ButtonWidth;
            internal54.Height = 90;
            internal54.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal54.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal56 = new MT.Singularity.Presentation.Controls.Image();
            internal56.Source = "embedded://MT.GeistlichPharma/MT.GeistlichPharma.Images.ArrowRight.al8";
            internal56.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal56.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal56.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal57 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal57.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal57.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal57.Text = "Programm";
            internal57.FontSize = ((System.Nullable<System.Int32>)20);
            internal57.Margin = new MT.Singularity.Presentation.Thickness(1);
            internal57.Margin = new MT.Singularity.Presentation.Thickness(0, 20, 0, 0);
            internal58 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal58.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal58.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal58.Text = "Starten";
            internal58.FontSize = ((System.Nullable<System.Int32>)20);
            internal58.Margin = new MT.Singularity.Presentation.Thickness(1);
            internal55 = new MT.Singularity.Presentation.Controls.GroupPanel(internal56, internal57, internal58);
            internal54.Content = internal55;
            this.bindings[14] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal54.Command,() => _viewModel.GoContinue,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal53 = new MT.Singularity.Presentation.Controls.StackPanel(internal54);
            internal53.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal53.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            internal8 = new MT.Singularity.Presentation.Controls.GroupPanel(internal9, internal22, internal53);
            internal8.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal8.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4278204523u));
            internal8.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal7 = new MT.Singularity.Presentation.Controls.DockPanel(internal8);
            internal2 = new MT.Singularity.Presentation.Controls.DockPanel(internal3, internal7);
            internal1 = new MT.Singularity.Presentation.Controls.GroupPanel(internal2);
            this.Content = internal1;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[15];
    }
}
