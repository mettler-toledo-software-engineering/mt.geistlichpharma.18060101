﻿using MT.Singularity.Data;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Model;

namespace MT.GeistlichPharma.Pages
{
    public class InfoPopupViewModel : PropertyChangedBase
    {
        public DialogResult DialogResult { get; set; }

        private readonly Visual _parent;
        private readonly MessageBoxButtons _buttons;
        private readonly MessageBoxIcon _icon;
        private readonly string _infoTitle;
        private readonly string _message;
        private readonly bool _hasIcon = true;
        private readonly bool _hasButton = true;

        public InfoPopupViewModel(Visual parent, string infoTitle, string message)
        {
            _parent = parent;
            _infoTitle = infoTitle;
            _message = message;

            _hasIcon = false;
            _hasButton = false;

            Initialize();
        }

        public InfoPopupViewModel(Visual parent, string infoTitle, string message, MessageBoxIcon icon)
        {
            _parent = parent;
            _icon = icon;
            _infoTitle = infoTitle;
            _message = message;
            _hasButton = false;

            Initialize();
        }

        public InfoPopupViewModel(Visual parent, string infoTitle, string message, MessageBoxIcon icon, MessageBoxButtons buttons)
        {
            _parent = parent;
            _buttons = buttons;
            _icon = icon;
            _infoTitle = infoTitle;
            _message = message;

            Initialize();
        }

        private void Initialize()
        {
            CollapseAllButtons();
            CollapseAllSigns();

            ShowButton();
            ShowIcon();
            ShowContent();
        }

        private void ShowContent()
        {
            Title = _infoTitle;
            Information = _message;
        }

        private void ShowIcon()
        {
            if (_hasIcon)
            {
                switch (_icon)
                {
                    case MessageBoxIcon.Error:
                        ErrorSign = Visibility.Visible;
                        break;
                    case MessageBoxIcon.Information:
                        InfoSign = Visibility.Visible;
                        break;
                    case MessageBoxIcon.Question:
                        QuestionSign = Visibility.Visible;
                        break;
                    case MessageBoxIcon.Warning:
                        WarnSign = Visibility.Visible;
                        break;
                }
            }
            else
                OkSign = Visibility.Visible;
        }

        private void CollapseAllSigns()
        {
            OkSign = Visibility.Collapsed;
            WarnSign = Visibility.Collapsed;
            InfoSign = Visibility.Collapsed;
            ErrorSign = Visibility.Collapsed;
            QuestionSign = Visibility.Collapsed;
        }

        private void ShowButton()
        {
            if (!_hasButton) return;

            switch (_buttons)
            {
                case MessageBoxButtons.YesNo:
                    ButtonYesVisibility = Visibility.Visible;
                    ButtonNoVisibility = Visibility.Visible;
                    break;
                case MessageBoxButtons.OK:
                    ButtonOkVisibility = Visibility.Visible;
                    break;
                case MessageBoxButtons.OKCancel:
                    break;
            }
        }

        private void CollapseAllButtons()
        {
            ButtonOkVisibility = Visibility.Collapsed;
            ButtonYesVisibility = Visibility.Collapsed;
            ButtonNoVisibility = Visibility.Collapsed;
        }

        #region KeyHandler

        #region ---- Yes
        /// <summary>
        /// GoCancel
        /// </summary>
        public ICommand GoYes
        {
            get { return new DelegateCommand(DoGoYes); }
        }

        private void DoGoYes()
        {
            DialogResult = DialogResult.Yes;
            ((InfoPopup)_parent).Close();
        }
        #endregion

        #region ---- No
        /// <summary>
        /// GoCancel
        /// </summary>
        public ICommand GoNo
        {
            get { return new DelegateCommand(DoGoGoNo); }
        }

        private void DoGoGoNo()
        {
            DialogResult = DialogResult.No;
            ((InfoPopup)_parent).Close();
        }
        #endregion

        #region ---- Ok
        /// <summary>
        /// GoCancel
        /// </summary>
        public ICommand GoOk
        {
            get { return new DelegateCommand(DoGoOk); }
        }

        private void DoGoOk()
        {
            DialogResult = DialogResult.OK;
            ((InfoPopup)_parent).Close();
        }
        #endregion

        #endregion

        #region Icon

        #region ---- OkSign
        private Visibility _oKSign = Visibility.Visible;
        public Visibility OkSign
        {
            get { return _oKSign; }
            set
            {
                if (_oKSign != value)
                {
                    _oKSign = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region ---- QuestionSign
        private Visibility _questionSign = Visibility.Visible;
        public Visibility QuestionSign
        {
            get { return _questionSign; }
            set
            {
                if (_questionSign != value)
                {
                    _questionSign = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region ---- WarnSign
        private Visibility _warnSign = Visibility.Visible;
        public Visibility WarnSign
        {
            get { return _warnSign; }
            set
            {
                if (_warnSign != value)
                {
                    _warnSign = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region ---- ErrorSign
        private Visibility _errorSign = Visibility.Visible;
        public Visibility ErrorSign
        {
            get { return _errorSign; }
            set
            {
                if (_errorSign != value)
                {
                    _errorSign = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region ---- InfoSign
        private Visibility _infoSign = Visibility.Visible;
        public Visibility InfoSign
        {
            get { return _infoSign; }
            set
            {
                if (_infoSign != value)
                {
                    _infoSign = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #endregion

        #region Buttons

        #region ---- ButtonOkVisibility
        private Visibility _buttonOkVisibility = Visibility.Visible;
        public Visibility ButtonOkVisibility
        {
            get { return _buttonOkVisibility; }
            set
            {
                if (_buttonOkVisibility != value)
                {
                    _buttonOkVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region ---- ButtonNoVisibility
        private Visibility _buttonNoVisibility = Visibility.Visible;
        public Visibility ButtonNoVisibility
        {
            get { return _buttonNoVisibility; }
            set
            {
                if (_buttonNoVisibility != value)
                {
                    _buttonNoVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region ---- ButtonYesVisibility
        private Visibility _buttonYesVisibility = Visibility.Visible;
        public Visibility ButtonYesVisibility
        {
            get { return _buttonYesVisibility; }
            set
            {
                if (_buttonYesVisibility != value)
                {
                    _buttonYesVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #endregion

        #region Content

        #region ---- Title
        public string  Title
        {
            get { return _title; }
            set
            {
                if (value != _title)
                {
                    _title = value;
                    NotifyPropertyChanged();
                }
            }
        }
        private string _title;
        #endregion

        #region ---- Information
        private string _information = "";
        public string Information
        {
            get { return _information; }
            set
            {
                if (_information != value)
                {
                    _information = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #endregion
    }
}
