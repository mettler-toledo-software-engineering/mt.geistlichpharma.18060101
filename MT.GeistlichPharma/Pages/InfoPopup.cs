﻿using MT.Singularity.Presentation.Controls;

namespace MT.GeistlichPharma.Pages
{
    /// <summary>
    /// Interaction logic for RecipeInformation
    /// </summary>
    public partial class InfoPopup
    {
        public readonly InfoPopupViewModel ViewModel;

        public InfoPopup(string title, string message)
        {
            ViewModel = new InfoPopupViewModel(this, title, message);

            InitializeComponents();
        }

        public InfoPopup(string title, string message, MessageBoxIcon icon)
        {
            ViewModel = new InfoPopupViewModel(this, title, message, icon);

            InitializeComponents();
        }

        public InfoPopup(string title, string message, MessageBoxIcon icon, MessageBoxButtons buttons)
        {
            ViewModel = new InfoPopupViewModel(this, title, message, icon, buttons);
                        
            InitializeComponents();
        }
    }
}
