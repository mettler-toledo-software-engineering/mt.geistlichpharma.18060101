﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using MT.Singularity;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;

namespace MT.GeistlichPharma.Pages
{
    /// <summary>
    /// Interaction logic for ManageContainerView
    /// </summary>
    public partial class ManageContainerView
    {
        private readonly ManageContainerViewModel _viewModel;
        public ManageContainerView(AnimatedNavigationFrame homeNavigationFrame)
        {
            _viewModel = new ManageContainerViewModel(this, homeNavigationFrame);
            InitializeComponents();
        }


        #region Overrides

        /// <summary>
        /// Called when a page is being navigated to for the first time.
        /// </summary>
        protected override void OnFirstNavigation()
        {
            HomeScreenViewModel.Instance.WeightDisplayShow = Visibility.Collapsed;
            base.OnFirstNavigation();
        }

        protected override NavigationResult OnNavigatingBack(INavigationPage nextPage)
        {
            HomeScreenViewModel.Instance.WeightDisplayShow = Visibility.Visible;
            return base.OnNavigatingBack(nextPage);
        }

        protected override void OnNavigationReturning(INavigationPage previousPage)
        {
            HomeScreenViewModel.Instance.WeightDisplayShow = Visibility.Collapsed;
            base.OnNavigationReturning(previousPage);
        }

        #endregion 
    }
}
