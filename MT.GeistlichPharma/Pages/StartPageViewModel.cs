﻿using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using MT.GeistlichPharma.Logic;
using MT.Singularity.Composition;
using MT.Singularity.Data;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Model;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.GeistlichPharma.Logic.MTSettings;
using System.Reflection;
using MT.GeistlichPharma.Configuration;
using MT.GeistlichPharma.Logic.HashCode;
using System.IO;
using log4net;
using MT.GeistlichPharma.Commands;
using MT.GeistlichPharma.Logic.DigitalIO;
using MT.GeistlichPharma.Stores;
using MT.Singularity.Logging;
using MT.Singularity.Platform.Configuration;
using MT.Singularity.Platform.UserManagement;

namespace MT.GeistlichPharma.Pages
{
    public class StartPageViewModel : PropertyChangedBase
    {
        private readonly Visual _parent;
        private readonly AnimatedNavigationFrame _homeNavigationFrame;
        private readonly InfoPopup _infoPopup;
        public  DigitalIo DigitalIo { get; }
        private ISecurityService _securityService;
        private static readonly ILog Logger = Log4NetManager.ApplicationLogger;
        private static readonly string SourceClass = nameof(StartPageViewModel);
        private readonly ProcessStore _processStore = ApplicationBootstrapperBase.CompositionContainer.Resolve<ProcessStore>();
        public bool IsCoarseDosing { get; set; }
        public bool IsPreciseDosing { get; set; }
        private User _currentUser;

        public User CurrentUser
        {
            get { return _currentUser; }
            set
            {
                _currentUser = value;
                NotifyPropertyChanged(nameof(CurrentUser));
                _goRecipeScreenCommand?.NotifyCanExecuteChanged();
                _goContainerScreenCommand?.NotifyCanExecuteChanged();
            }
        }
        
        public StartPageViewModel(Visual parent, AnimatedNavigationFrame homeNavigationFrame)
        {
            _parent = parent;
            _homeNavigationFrame = homeNavigationFrame;
            DigitalIo = new DigitalIo();

            InitializeViewModel();
            Debug.WriteLine(_homeNavigationFrame.Count.ToString());

            Version = "P18060101 Geistlich Pharma AG V" + Assembly.GetExecutingAssembly().GetName().Version.Major +
                "." + Assembly.GetExecutingAssembly().GetName().Version.Minor +
                "." + Assembly.GetExecutingAssembly().GetName().Version.Build;

            _infoPopup = new InfoPopup(Localization.Get(Localization.Key.ConnectionFailed_Title),
                Localization.Get(Localization.Key.ConnectionFailed_Message), MessageBoxIcon.Error, MessageBoxButtons.OK);
            //GoRecipeScreenCommand = new NavigateSelectRecipeCommand(this, parent, homeNavigationFrame, _processStore);
            GoContinue = new NavigateSelectRecipeCommand(this, parent, homeNavigationFrame, _processStore);
            _goRecipeScreenCommand =  new DelegateCommand(DoGoRecipe, CanGoRecipeScreen);
            _goContainerScreenCommand =  new DelegateCommand(DoGoContainer, CanGoContainerScreen);
            
        }

        [SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
        private async void InitializeViewModel()
        {
            var compositionContainer = ApplicationBootstrapperBase.CompositionContainer;
            // init config
            var balancerComponent = compositionContainer.Resolve<IBalancerComponents>();
            Globals.Instance.Config = await balancerComponent.GetConfigurationAsync();
            IpAddress = Globals.Instance.Config.IpAddress;
            Port = Globals.Instance.Config.Port;

            ResetDosingButtons();

            _securityService = compositionContainer.Resolve<ISecurityService>();
            _securityService.CurrentUserChanged += SecurityServiceOnCurrentUserChanged;
            CurrentUser = _securityService.CurrentUser;

            HomeScreenViewModel.Instance.StatusBarVisibility = Visibility.Visible;

        }



        private void SecurityServiceOnCurrentUserChanged(User user)
        {
            CurrentUser = user;
        }

            private void ResetDosingButtons()
            {
                IsCoarseDosing = false;
                IsPreciseDosing = false;
                DigitalIo.StartStopFill(IsCoarseDosing, IsPreciseDosing);

                ShowCoarseDosingButton(true);
                ShowPreciseDosingButton(true);
            }

        public void ShowCoarseDosingButton(bool show)
        {
            ShowCoarseDosing = show ? Visibility.Visible : Visibility.Collapsed;
            ShowStopCoarseDosing = show ? Visibility.Collapsed : Visibility.Visible;
        }

        public void ShowPreciseDosingButton(bool show)
        {
            ShowPreciseDosing = show ? Visibility.Visible : Visibility.Collapsed;
            ShowStopPreciseDosing = show ? Visibility.Collapsed : Visibility.Visible;
        }

        #region KeyHandling

        #region GoRecipeScreen

        private readonly DelegateCommand _goRecipeScreenCommand;
        public ICommand GoRecipeScreenCommand
        {
            get { return _goRecipeScreenCommand; }

        }
        private readonly DelegateCommand _goContainerScreenCommand;
        public ICommand GoContainerScreenCommand
        {
            get { return _goContainerScreenCommand; }

        }

        private void DoGoRecipe()
        {
            _homeNavigationFrame.NavigateTo(new ManageRecipesView(_homeNavigationFrame));
        }

        private bool CanGoRecipeScreen()
        {
            bool canExecute = Permissions.IsSupervisorOrHigher(CurrentUser.Permission);

            return canExecute;
        }
        private void DoGoContainer()
        {
            _homeNavigationFrame.NavigateTo(new ManageContainerView(_homeNavigationFrame));
        }

        private bool CanGoContainerScreen()
        {
            bool canExecute = Permissions.IsSupervisorOrHigher(CurrentUser.Permission);

            return canExecute;
        }

        #endregion

        #region ---- GoContinue
        /// <summary>
        /// Continue
        /// </summary>
        public ICommand GoContinue { get; }
        //{
        //    get { return new DelegateCommand(DoGoContinue); }
        //}

        private void DoGoContinue()
        {
            ResetDosingButtons();

            InfoPopup info;

            if (!File.Exists(Settings.FileName))
            {
                var title = !File.Exists(Settings.FileName)
                    ? Localization.Get(Localization.Key.RecipesNotFoundTitle)
                    : Localization.Get(Localization.Key.HashcodeNotFoundTitle);

                var message = Localization.Get(Localization.Key.RecipesNotFoundMessage)
                                 + "\n" + Settings.FileName;

                info = new InfoPopup(title, message, MessageBoxIcon.Error, MessageBoxButtons.OK);
                info.Show(_parent);
            }
            else
            {
                _homeNavigationFrame.NavigateTo(new RecipeSelection(_homeNavigationFrame), TransitionAnimation.LeftToRight);
            }
        }

        // 04.05.2022 zero, tare clear tare mit try catch versehen, um abstürze beim Tarieren abzufangen.
        #endregion



        #region ---- Zero
        /// <summary>
        /// Zero
        /// </summary>
        public ICommand GoZero
        {
            get { return new DelegateCommand(DoGoZero); }
          //  get { return new DelegateCommand(HomeScreenViewModel.Instance.SwitchScale); }
        }

        private async void DoGoZero()
        {
            if (HomeScreenViewModel.Instance.Scale != null)
                try
                {
                    await HomeScreenViewModel.Instance.Scale.ZeroAsync();
                }
                catch (Exception e)
                {
                    Logger.ErrorEx(e.Message, SourceClass, e);
                }
                
            else
                _infoPopup.Show(_parent);
        }


        #endregion

        #region ---- Tare
        /// <summary>
        /// Tare
        /// </summary>
        public ICommand GoTare
        {
            get { return new DelegateCommand(DoGoTare); }
        }

        private async void DoGoTare()
        {
            if (HomeScreenViewModel.Instance.Scale != null)
                try
                {
                    await HomeScreenViewModel.Instance.Scale.TareAsync();
                }
                catch (Exception e)
                {
                    Logger.ErrorEx(e.Message, SourceClass, e);
                }
                
            else
                _infoPopup.Show(_parent);
        }
        #endregion

        #region ---- ClearTare
        /// <summary>
        /// ClearTare
        /// </summary>
        public ICommand GoClearTare
        {
            get { return new DelegateCommand(DoGoClearTare); }
        }

        private async void DoGoClearTare()
        {
            if (HomeScreenViewModel.Instance.Scale != null)
                try
                {
                    await HomeScreenViewModel.Instance.Scale.ClearTareAsync();
                }
                catch (Exception e)
                {
                    Logger.ErrorEx(e.Message, SourceClass, e);
                }
                
            else
                _infoPopup.Show(_parent);
        }
        #endregion

        #region ---- CoarseDosing
        /// <summary>
        /// CoarseDosing
        /// </summary>
        public ICommand GoCoarseDosing
        {
            get { return new DelegateCommand(DoGoCoarseDosing); }
        }

        private void DoGoCoarseDosing()
        {
            IsCoarseDosing = true;
            DigitalIo.StartStopFill(IsCoarseDosing, IsPreciseDosing);

            ShowCoarseDosingButton(false);
        }
        #endregion

        #region ---- StopCoarseDosing
        /// <summary>
        /// CoarseDosing
        /// </summary>
        public ICommand GoStopCoarseDosing
        {
            get { return new DelegateCommand(DoGoStopCoarseDosing); }
        }

        private void DoGoStopCoarseDosing()
        {
            IsCoarseDosing = false;
            DigitalIo.StartStopFill(IsCoarseDosing, IsPreciseDosing);

            ShowCoarseDosingButton(true);
        }
        #endregion

        #region ---- PreciseDosing
        /// <summary>
        /// PreciseDosing
        /// </summary>
        public ICommand GoPreciseDosing
        {
            get { return new DelegateCommand(DoGoPreciseDosing); }
        }

        private void DoGoPreciseDosing()
        {
            IsPreciseDosing = true;
            DigitalIo.StartStopFill(IsCoarseDosing, IsPreciseDosing);

            ShowPreciseDosingButton(false);
        }
        #endregion

        #region ---- GoStopPreciseDosing
        /// <summary>
        /// PreciseDosing
        /// </summary>
        public ICommand GoStopPreciseDosing
        {
            get { return new DelegateCommand(DoGoStopPreciseDosing); }
        }

        private void DoGoStopPreciseDosing()
        {
            IsPreciseDosing = false;
            DigitalIo.StartStopFill(IsCoarseDosing, IsPreciseDosing);

            ShowPreciseDosingButton(true);
        }
        #endregion
        
        #endregion

        #region Visibility

        #region ---- ShowCoarseDosing

        private Visibility _showCoarseDosing;

        public Visibility ShowCoarseDosing
        {
            get
            {
                return _showCoarseDosing;
            }
            set
            {
                if (_showCoarseDosing != value)
                {
                    _showCoarseDosing = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region ---- ShowStopCoarseDosing

        private Visibility _showStopCoarseDosing;

        public Visibility ShowStopCoarseDosing
        {
            get
            {
                return _showStopCoarseDosing;
            }
            set
            {
                if (_showStopCoarseDosing != value)
                {
                    _showStopCoarseDosing = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region ---- ShowPreciseDosing

        private Visibility _showPreciseDosing;

        public Visibility ShowPreciseDosing
        {
            get
            {
                return _showPreciseDosing;
            }
            set
            {
                if (_showPreciseDosing != value)
                {
                    _showPreciseDosing = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region ---- ShowStopPreciseDosing

        private Visibility _showStopPreciseDosing;

        public Visibility ShowStopPreciseDosing
        {
            get
            {
                return _showStopPreciseDosing;
            }
            set
            {
                if (_showStopPreciseDosing != value)
                {
                    _showStopPreciseDosing = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #endregion

        #region Properties

        #region ---- IPAddress
        private String _iPAddress = "127.0.0.1";
        public String IpAddress
        {
            get
            {
                return _iPAddress;
            }
            set
            {
                if (_iPAddress != value)
                {
                    _iPAddress = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region ---- Port
        private int _port = 20000;
        public int Port
        {
            get
            {
                return _port;
            }
            set
            {
                if (_port != value)
                {
                    _port = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #region ---- Version
        private string _version = "";
        public string Version
        {
            get
            {
                return _version;
            }
            set
            {
                if (_version != value)
                {
                    _version = value;
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        #endregion
    }
}
