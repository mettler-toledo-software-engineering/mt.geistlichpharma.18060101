﻿namespace MT.GeistlichPharma.Pages
{
    /// <summary>
    /// Interaction logic for RecipeInformation
    /// </summary>
    public partial class RecipeInformation
    {
        private readonly RecipeInformationViewModel _viewModel;


        public RecipeInformation(RecipeSelectionViewModel recipeSelectionViewModel)
        {
            _viewModel = new RecipeInformationViewModel(this, recipeSelectionViewModel);
                        
            InitializeComponents();

        }

    }
}
