﻿using System;

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using MT.GeistlichPharma.Commands;
using MT.GeistlichPharma.Infrastructure;
using MT.GeistlichPharma.Logic.MTSettings;
using MT.Singularity.Data;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Model;

namespace MT.GeistlichPharma.Pages
{
    public class ManageContainerViewModel : PropertyChangedBase, IScrollableViewModel
    {
        private readonly AnimatedNavigationFrame _homeNavigationFrame;


        public ICommand ScrollUpCommand { get; }
        public ICommand ScrollDownCommand { get; }
        public ICommand CreateNewContainerCommand { get; }
        public ICommand UpdateContainerCommand { get; }
        public ICommand DeleteContainerCommand { get; }

        public ManageContainerViewModel(Visual parent, AnimatedNavigationFrame homeNavigationFrame)
        {
            _homeNavigationFrame = homeNavigationFrame;
            UpdateContainerList();
            ScrollUpCommand = new ScrollUpCommand(this);
            ScrollDownCommand = new ScrollDownCommand(this);
            CreateNewContainerCommand = new CreateNewContainerCommand(this);
            UpdateContainerCommand = new UpdateContainerCommand(this, parent);
            DeleteContainerCommand = new DeleteContainerCommand(this, parent);
        }

        public void UpdateContainerList()
        {
            Container?.Clear();
            Container = new ObservableCollection<Container>(Settings.Current.Container.OrderBy(c => c.Name));
            SelectedItem = Container.FirstOrDefault();
            ListIndex = 0;
        }

        private ObservableCollection<Container> _container;

        public ObservableCollection<Container> Container
        {
            get { return _container; }
            set
            {
                _container = value;
                NotifyPropertyChanged(nameof(Container));
                NotifyPropertyChanged(nameof(ListCount));
            }
        }

        private Container _selectedItem;

        public object SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = (Container)value;
                SelectedContainer = _selectedItem == null ? new Container() : _selectedItem.Clone();
                NotifyPropertyChanged(nameof(SelectedItem));
            }
        }

        private Container _selectedContainer;

        public Container SelectedContainer
        {
            get { return _selectedContainer; }
            set
            {
                _selectedContainer = value;
                NotifyPropertyChanged(nameof(SelectedContainer));
                NotifyPropertyChanged(nameof(Name));
                NotifyPropertyChanged(nameof(Weight));
                NotifyPropertyChanged(nameof(Tolerance));
            }
        }

        public string Name
        {
            get { return SelectedContainer?.Name ?? string.Empty; }
            set
            {
                SelectedContainer.Name = value;
                NotifyPropertyChanged(nameof(Name));
            }
        }
        public double Weight
        {
            get { return SelectedContainer?.Weight ?? 0; }
            set
            {
                SelectedContainer.Weight = value;
                NotifyPropertyChanged(nameof(Weight));
            }
        }
        public double Tolerance
        {
            get { return SelectedContainer?.Tolerance ?? 0; }
            set
            {
                SelectedContainer.Tolerance = value;
                NotifyPropertyChanged(nameof(Tolerance));
            }
        }

        private int _listIndex;
        public int ListIndex
        {
            get { return _listIndex; }
            set
            {
                _listIndex = value;
                NotifyPropertyChanged(nameof(ListIndex));
            }
        }
        public int ListCount => Container == null ? 0 : Container.Count;

        public ICommand GoCancel
        {
            get { return new DelegateCommand(DoGoCancel); }
        }

        private void DoGoCancel()
        {
            _homeNavigationFrame.Back();
        }
    }
}
