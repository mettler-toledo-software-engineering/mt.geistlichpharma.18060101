﻿using MT.Singularity.Presentation.Controls;
namespace MT.GeistlichPharma.Pages
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.1.0.0")]
    public partial class InfoPopup : MT.Singularity.Presentation.Controls.ChildWindow
    {
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.GroupPanel internal1;
            MT.Singularity.Presentation.Controls.StackPanel internal2;
            MT.Singularity.Presentation.Controls.StackPanel internal3;
            MT.Singularity.Presentation.Controls.Image internal4;
            MT.Singularity.Presentation.Controls.Image internal5;
            MT.Singularity.Presentation.Controls.Image internal6;
            MT.Singularity.Presentation.Controls.Image internal7;
            MT.Singularity.Presentation.Controls.Image internal8;
            MT.Singularity.Presentation.Controls.StackPanel internal9;
            MT.Singularity.Presentation.Controls.TextBlock internal10;
            MT.Singularity.Presentation.Controls.TextBlock internal11;
            MT.Singularity.Presentation.Controls.StackPanel internal12;
            MT.Singularity.Presentation.Controls.Button internal13;
            MT.Singularity.Presentation.Controls.TextBlock internal14;
            MT.Singularity.Presentation.Controls.Button internal15;
            MT.Singularity.Presentation.Controls.TextBlock internal16;
            MT.Singularity.Presentation.Controls.Button internal17;
            MT.Singularity.Presentation.Controls.TextBlock internal18;
            internal4 = new MT.Singularity.Presentation.Controls.Image();
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal4.Visibility,() =>  ViewModel.OkSign,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal4.Source = "embedded://MT.GeistlichPharma/MT.GeistlichPharma.Images.InTolerance.png";
            internal4.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal5 = new MT.Singularity.Presentation.Controls.Image();
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal5.Visibility,() =>  ViewModel.QuestionSign,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal5.Source = "embedded://MT.GeistlichPharma/MT.GeistlichPharma.Images.question.png";
            internal5.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal6 = new MT.Singularity.Presentation.Controls.Image();
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal6.Visibility,() =>  ViewModel.WarnSign,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal6.Source = "embedded://MT.GeistlichPharma/MT.GeistlichPharma.Images.Warning.png";
            internal6.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal7 = new MT.Singularity.Presentation.Controls.Image();
            this.bindings[3] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal7.Visibility,() =>  ViewModel.ErrorSign,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal7.Source = "embedded://MT.GeistlichPharma/MT.GeistlichPharma.Images.Error.png";
            internal7.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal8 = new MT.Singularity.Presentation.Controls.Image();
            this.bindings[4] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal8.Visibility,() =>  ViewModel.InfoSign,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal8.Source = "embedded://MT.GeistlichPharma/MT.GeistlichPharma.Images.Info.png";
            internal8.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal3 = new MT.Singularity.Presentation.Controls.StackPanel(internal4, internal5, internal6, internal7, internal8);
            internal10 = new MT.Singularity.Presentation.Controls.TextBlock();
            this.bindings[5] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal10.Text,() =>  ViewModel.Title,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal10.FontSize = ((System.Nullable<System.Int32>)28);
            internal11 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal11.Margin = new MT.Singularity.Presentation.Thickness(0, 20, 0, 0);
            this.bindings[6] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal11.Text,() =>  ViewModel.Information,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal11.FontSize = ((System.Nullable<System.Int32>)22);
            internal14 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal14.Text = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.Yes);
            internal14.AddTranslationAction(() => {
                internal14.Text = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.Yes);
            });
            internal14.FontSize = ((System.Nullable<System.Int32>)22);
            internal14.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal13 = new MT.Singularity.Presentation.Controls.Button();
            internal13.Content = internal14;
            internal13.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal13.Width = 180;
            internal13.Height = 60;
            internal13.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal13.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            this.bindings[7] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal13.Visibility,() =>  ViewModel.ButtonYesVisibility,MT.Singularity.Expressions.BindingMode.OneWay,false);
            this.bindings[8] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal13.Command,() =>  ViewModel.GoYes,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal16 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal16.Text = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.No);
            internal16.AddTranslationAction(() => {
                internal16.Text = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.No);
            });
            internal16.FontSize = ((System.Nullable<System.Int32>)22);
            internal16.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal15 = new MT.Singularity.Presentation.Controls.Button();
            internal15.Content = internal16;
            internal15.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal15.Width = 180;
            internal15.Height = 60;
            internal15.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal15.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            this.bindings[9] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal15.Visibility,() =>  ViewModel.ButtonNoVisibility,MT.Singularity.Expressions.BindingMode.OneWay,false);
            this.bindings[10] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal15.Command,() =>  ViewModel.GoNo,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal18 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal18.Text = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.Ok);
            internal18.AddTranslationAction(() => {
                internal18.Text = MT.GeistlichPharma.Localization.Get(MT.GeistlichPharma.Localization.Key.Ok);
            });
            internal18.FontSize = ((System.Nullable<System.Int32>)22);
            internal18.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal17 = new MT.Singularity.Presentation.Controls.Button();
            internal17.Content = internal18;
            internal17.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal17.Width = 180;
            internal17.Height = 60;
            internal17.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal17.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            this.bindings[11] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal17.Visibility,() =>  ViewModel.ButtonOkVisibility,MT.Singularity.Expressions.BindingMode.OneWay,false);
            this.bindings[12] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal17.Command,() =>  ViewModel.GoOk,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal12 = new MT.Singularity.Presentation.Controls.StackPanel(internal13, internal15, internal17);
            internal12.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal12.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal12.Margin = new MT.Singularity.Presentation.Thickness(0, 20, 0, 0);
            internal9 = new MT.Singularity.Presentation.Controls.StackPanel(internal10, internal11, internal12);
            internal9.Margin = new MT.Singularity.Presentation.Thickness(20, 0, 40, 0);
            internal9.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal9.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal2 = new MT.Singularity.Presentation.Controls.StackPanel(internal3, internal9);
            internal2.Margin = new MT.Singularity.Presentation.Thickness(20);
            internal2.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal1 = new MT.Singularity.Presentation.Controls.GroupPanel(internal2);
            this.Content = internal1;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[13];
    }
}
