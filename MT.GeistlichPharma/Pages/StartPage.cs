﻿using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls.Navigation;

namespace MT.GeistlichPharma.Pages
{
    /// <summary>
    /// Interaction logic for StartPage
    /// </summary>
    public partial class StartPage
    {
        private readonly StartPageViewModel _viewModel;

        public StartPage(AnimatedNavigationFrame homeNavigationFrame)
        {
            _viewModel = new StartPageViewModel(this, homeNavigationFrame);
            InitializeComponents();
        }

        #region Overrides

        protected override void OnFirstNavigation()
        {
            HomeScreenViewModel.Instance.WeightDisplayShow = Visibility.Visible;
            base.OnFirstNavigation();
        }

        protected override void OnNavigationReturning(INavigationPage previousPage)
        {
            HomeScreenViewModel.Instance.WeightDisplayShow = Visibility.Visible;
            base.OnNavigationReturning(previousPage);
        }

        protected override NavigationResult OnNavigatingBack(INavigationPage nextPage)
        {
            HomeScreenViewModel.Instance.WeightDisplayShow = Visibility.Visible;
            return base.OnNavigatingBack(nextPage);
        }

        #endregion
    }
}