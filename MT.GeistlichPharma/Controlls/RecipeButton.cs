﻿using MT.GeistlichPharma.Logic.MTSettings;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;

namespace MT.GeistlichPharma.Controlls
{
    public class RecipeButton : Button
    {
        private TextBlock _description;
        private TextBlock _additionalDescription;
        private TextBlock _number;
        private TextBlock _target;

        public Recipe RecipeItem { get; set; }

        public RecipeButton(Recipe recipe)
        {
            _description = new TextBlock();
            _number = new TextBlock();
            _target = new TextBlock();

            RecipeItem = recipe;

            ConfigureButton();
            ConfigureDescription();
            ConfigureAdditionalDescription();
            ConfigureTarget();
            ConfigureNumber();

            Visual[] children = { _description, _additionalDescription, _target, _number };
            GroupPanel groupPanel = new GroupPanel(children)
            {
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center
            };

            Content = groupPanel;
        }

        private void ConfigureButton()
        {
            Width = 240;
            Height = 80;
            HorizontalContentAlignment = HorizontalAlignment.Stretch;
            VerticalContentAlignment = VerticalAlignment.Stretch;

            Margin = new Thickness(4, 4, 4, 4);
        }

        private void ConfigureDescription()
        {
            var description = RecipeItem.Description;

            _description = ConfigureNewTextBlock(VerticalAlignment.Top,
                HorizontalAlignment.Center, 10, 6, 10, 0, 18, description);
        }

        private void ConfigureAdditionalDescription()
        {
            var additionalDescription = RecipeItem.Size;

            _additionalDescription = ConfigureNewTextBlock(VerticalAlignment.Top,
                HorizontalAlignment.Center, 10, 24, 10, 0, 18, additionalDescription);
        }

        private void ConfigureTarget()
        {
            _target = ConfigureNewTextBlock(VerticalAlignment.Bottom,
                HorizontalAlignment.Center, 0, 0, 0, 20, 16, RecipeItem.Target.ToString("0.000") + " g");
        }

        private void ConfigureNumber()
        {
            _number = ConfigureNewTextBlock(VerticalAlignment.Bottom,
                HorizontalAlignment.Center, 0, 0, 0, 2, 16, RecipeItem.Number);
        }

        #region ConfigureNewTextBlock
        private TextBlock ConfigureNewTextBlock(VerticalAlignment vertical, HorizontalAlignment horizontal,
            int marginLeft, int marginTop, int marginRight, int marginBottom, int fontSize, string text)
        {
            TextBlock textBlock = new TextBlock
            {
                VerticalAlignment = vertical,
                HorizontalAlignment = horizontal,
                Margin = new Thickness(marginLeft, marginTop, marginRight, marginBottom),
                FontSize = fontSize,
                Text = text
            };
            return textBlock;
        }
        #endregion

    }
}
