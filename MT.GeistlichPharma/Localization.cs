﻿// <auto-generated/>

using System;
using System.Collections.Generic;
using MT.Singularity.Collections;
using MT.Singularity.Localization;

namespace MT.GeistlichPharma 
{
	[System.CodeDom.Compiler.GeneratedCodeAttribute("MT.Singularity.Environment.Yml.Localization.LanguageXmlTemplate", "2.1.0.0")]
	[TranslationModule("en-US", "de-DE", "fr-FR", Strings = 110)]
	public class Localization
	{	
		internal static readonly TranslationModule Module = new ModuleClass();

		public enum Key
		{
			/// <summary>
			/// Aktueller Wert
			/// </summary>
			ActualValue,
			/// <summary>
			/// Anzahl Abfüllungen in Toleranz:
			/// </summary>
			Amount_InTolerance,
			/// <summary>
			/// Anzahl Überfüllungen:
			/// </summary>
			Amount_Overdosed,
			/// <summary>
			/// APR331 Kontrast
			/// </summary>
			APR331Kontrast,
			/// <summary>
			/// APR331 Widerstandsklasse
			/// </summary>
			APR331Widerstand,
			/// <summary>
			/// Zurück
			/// </summary>
			Back,
			/// <summary>
			/// Waage
			/// </summary>
			BalanceLabel,
			/// <summary>
			/// Geistlich Pharma AG
			/// </summary>
			BalancerMainNode,
			/// <summary>
			/// Toleranz (Bio-Flow) [g]
			/// </summary>
			BioFlowTolerance,
			/// <summary>
			/// Gewicht (Bio-Flow) [g]
			/// </summary>
			BioFlowWeight,
			/// <summary>
			/// Toleranz (Flasche) [g]
			/// </summary>
			BottleTolerance,
			/// <summary>
			/// Gewicht (Flasche) [g]
			/// </summary>
			BottleWeight,
			/// <summary>
			/// Abbrechen
			/// </summary>
			Cancel,
			/// <summary>
			/// Prozess abbrechen
			/// </summary>
			CancelProcess,
			/// <summary>
			/// Möchten Sie die Charge '{0}' wirklich beenden?
			/// </summary>
			ChargeDone_ContentQuestion,
			/// <summary>
			/// Charge beenden?
			/// </summary>
			ChargeDone_TitleQuestion,
			/// <summary>
			/// Chargenr.
			/// </summary>
			ChargrNr,
			/// <summary>
			/// Chargennummer
			/// </summary>
			ChargrNumber,
			/// <summary>
			/// Tara löschen
			/// </summary>
			ClearTare,
			/// <summary>
			/// Grobdosierung
			/// </summary>
			CoarseDosing,
			/// <summary>
			/// Waage konnte nicht initialisiert werden!
			/// </summary>
			ConnectionFailed_Message,
			/// <summary>
			/// Verbindung fehlgeschlagen!
			/// </summary>
			ConnectionFailed_Title,
			/// <summary>
			/// No connection to the host!
			/// </summary>
			ConnectionTimeOut,
			/// <summary>
			/// Behälter
			/// </summary>
			ContainerSubNode,
			/// <summary>
			/// Weiterfahren
			/// </summary>
			Continue,
			/// <summary>
			/// Bitte Charge fortsetzen oder beenden.
			/// </summary>
			ContinueOrQuitCharge,
			/// <summary>
			/// Gewicht Abdeckung
			/// </summary>
			CoverplateWeight,
			/// <summary>
			/// Aktuelle Abfüllnummer:
			/// </summary>
			CurrentDosingNr,
			/// <summary>
			/// Angemeldeter Benutzer:
			/// </summary>
			CurrentUser,
			/// <summary>
			/// Digital IO
			/// </summary>
			DigitalIOSubNode,
			/// <summary>
			/// Erledigt
			/// </summary>
			Done,
			/// <summary>
			/// Dosierung erfolgreich abgeschlossen!
			/// </summary>
			DosingSucceeded,
			/// <summary>
			/// Grobdosierung läuft...
			/// </summary>
			FastFillMessage,
			/// <summary>
			/// Abgefüllter Wert
			/// </summary>
			FilledValue,
			/// <summary>
			/// Allgemeine Einstell.
			/// </summary>
			GeneralSettingsSubNode,
			/// <summary>
			/// Brutto
			/// </summary>
			Gross,
			/// <summary>
			/// Kein Hashcode gefunden!
			/// </summary>
			HashcodeNotFoundTitle,
			/// <summary>
			/// Passwort eingeben
			/// </summary>
			InputPassword,
			/// <summary>
			/// Benutzername eingeben
			/// </summary>
			InputUsername,
			/// <summary>
			/// Ungültiges Gewicht!
			/// </summary>
			InvalidWeight,
			/// <summary>
			/// IP-Addresse
			/// </summary>
			IPAddress,
			/// <summary>
			/// Zuletzt geändert
			/// </summary>
			LastChanged,
			/// <summary>
			/// Bitte {0} in die Vorrichtung setzen.
			/// </summary>
			LoadScale,
			/// <summary>
			/// Anmelden
			/// </summary>
			Login,
			/// <summary>
			/// Logout
			/// </summary>
			Logout,
			/// <summary>
			/// Do you really want to logout?
			/// </summary>
			LogoutQuestion,
			/// <summary>
			/// must not be empty!
			/// </summary>
			MustNotBeEmpty,
			/// <summary>
			/// Nein
			/// </summary>
			No,
			/// <summary>
			/// Zustandsänderungsdauer [s]
			/// </summary>
			NoActionTimer,
			/// <summary>
			/// Keine Rezepte unter '{0}' gefunden!
			/// </summary>
			NoRecipesFound,
			/// <summary>
			/// Es gab keine Zustandsänderung in den letzten {0} Sekunden.
			/// </summary>
			NoStateChanged_Content,
			/// <summary>
			/// Keine Zustandsänderung!
			/// </summary>
			NoStateChanged_Title,
			/// <summary>
			/// Ok
			/// </summary>
			Ok,
			/// <summary>
			/// Überdosiert!
			/// </summary>
			Overdosed,
			/// <summary>
			/// Passwort:
			/// </summary>
			Password,
			/// <summary>
			/// Passwort abgelaufen
			/// </summary>
			PasswordExpired,
			/// <summary>
			/// Password does not meet requirements
			/// </summary>
			PasswordPolicy,
			/// <summary>
			/// Unterbruch
			/// </summary>
			Pause,
			/// <summary>
			/// Toleranz (Pen) [g]
			/// </summary>
			PenTolerance,
			/// <summary>
			/// Gewicht (Pen) [g]
			/// </summary>
			PenWeight,
			/// <summary>
			/// Port
			/// </summary>
			Port,
			/// <summary>
			/// Feindosierung
			/// </summary>
			PreciseDosing,
			/// <summary>
			/// Vorschau
			/// </summary>
			Preview,
			/// <summary>
			/// Drucken
			/// </summary>
			Print,
			/// <summary>
			/// Drucker APR331
			/// </summary>
			PrinterSubNode,
			/// <summary>
			/// Prozess wurde angehalten!
			/// </summary>
			ProzessPaused,
			/// <summary>
			/// Pulsieren
			/// </summary>
			Pulsing,
			/// <summary>
			/// Plusierungsdauer [s]
			/// </summary>
			PulsingDurationSubNode,
			/// <summary>
			/// Bitte abgefüllten Behälter in den Trichten leeren.
			/// </summary>
			PutContentInFunnel,
			/// <summary>
			/// Change Password successfull
			/// </summary>
			PwChangeSucceed,
			/// <summary>
			/// Beenden
			/// </summary>
			Quit,
			/// <summary>
			/// Artikel registriert!
			/// </summary>
			RecipeDone,
			/// <summary>
			/// Rezeptverwaltung
			/// </summary>
			RecipeManagement,
			/// <summary>
			/// Rezeptnummer
			/// </summary>
			RecipeNr,
			/// <summary>
			/// Rezeptauswahl
			/// </summary>
			RecipeSelection,
			/// <summary>
			/// Folgende Datei ist nicht vorhanden:
			/// </summary>
			RecipesNotFoundMessage,
			/// <summary>
			/// Keine Rezepte gefunden!
			/// </summary>
			RecipesNotFoundTitle,
			/// <summary>
			/// Rezeptversion
			/// </summary>
			RecipeVersion,
			/// <summary>
			/// Bitte abgefüllten Behälter von der Waage nehmen.
			/// </summary>
			RemoveFullContainer,
			/// <summary>
			/// Reprint
			/// </summary>
			Reprint,
			/// <summary>
			/// Waage
			/// </summary>
			Scale,
			/// <summary>
			/// Überlast Waage
			/// </summary>
			ScaleOverload,
			/// <summary>
			/// Waage bereit!
			/// </summary>
			ScaleReady,
			/// <summary>
			/// Unterlast Waage
			/// </summary>
			ScaleUnderload,
			/// <summary>
			/// Rezepte suchen!
			/// </summary>
			SearchRecipe,
			/// <summary>
			/// Die Chargennummer muss genau 8 Ziffern beinhalten.
			/// </summary>
			ShortChargeNr_Message,
			/// <summary>
			/// Chargennummer zu kurz!
			/// </summary>
			ShortChargeNr_Title,
			/// <summary>
			/// Feindosierung läuft...
			/// </summary>
			SlowFillMessage,
			/// <summary>
			/// Starten
			/// </summary>
			Start,
			/// <summary>
			/// Programm starten
			/// </summary>
			StartProgram,
			/// <summary>
			/// Stoppen
			/// </summary>
			Stop,
			/// <summary>
			/// english
			/// </summary>
			SupportedLanguages,
			/// <summary>
			/// Tara
			/// </summary>
			Tare,
			/// <summary>
			/// Waage konnte nicht tarieren!
			/// </summary>
			TareFailed_Content,
			/// <summary>
			/// Tara fehlgeschlagen!
			/// </summary>
			TareFailed_Title,
			/// <summary>
			/// Zielwert
			/// </summary>
			TargetValue,
			/// <summary>
			/// Please unload scale!
			/// </summary>
			UnloadScale,
			/// <summary>
			/// Benutzername:
			/// </summary>
			Username,
			/// <summary>
			/// Toleranz (Vial) [g]
			/// </summary>
			VialTolerance,
			/// <summary>
			/// Gewicht (Vial) [g]
			/// </summary>
			VialWeight,
			/// <summary>
			/// Warten
			/// </summary>
			Waiting,
			/// <summary>
			/// Wartedauer [s]
			/// </summary>
			WaitingDurationSubNode,
			/// <summary>
			/// Bitte Waage tarieren!
			/// </summary>
			WaitTare,
			/// <summary>
			/// Hashcode stimmt nicht überein! Die Rezeptliste wurde modifiziert.
			/// </summary>
			WrongHashCode_Message,
			/// <summary>
			/// Hashcode falsch!
			/// </summary>
			WrongHashCode_Title,
			/// <summary>
			/// Rezeptnummer stimmt nicht mit dem ausgewählten Rezept überein.
			/// </summary>
			WrongRecipeNr_Message,
			/// <summary>
			/// Falsche Rezeptnummer!
			/// </summary>
			WrongRecipeNr_Title,
			/// <summary>
			/// Benutzer oder Passwort falsch
			/// </summary>
			WrongUserPassword,
			/// <summary>
			/// Ja
			/// </summary>
			Yes,
			/// <summary>
			/// Nullieren
			/// </summary>
			Zero,
		}

		public static string Get(Key key)
		{
			return LocalizationManager.GetTranslation(Module, (int)key);
		}

		public static string Format(Key key, string arg0)
		{
			return string.Format(LocalizationManager.GetTranslation(Module, (int)key), arg0);
		}

		public static string Format(Key key, string arg0, string arg1)
		{
			return string.Format(LocalizationManager.GetTranslation(Module, (int)key), arg0, arg1);
		}

		public static string Format(Key key, params string[] args)
		{
			return string.Format(LocalizationManager.GetTranslation(Module, (int)key), args);
		}

		public static TranslationModule GetTranslationModule()
		{
			return Module;
		}

		private class ModuleClass : TranslationModule
		{
			private readonly IImmutableIndexable<string> languages = Indexable.ImmutableValues("en-US", "de-DE", "fr-FR");

			public override string PrimaryLanguage
			{
				get { return "en-US"; }
			}

			public override IEnumerable<string> SupportedLanguages
			{
				get { return languages; }
			}

			public override string Name 
			{
				get { return "MT.GeistlichPharma.Localization"; }
			}

			public override ITranslationProvider GetTranslation(string language)
			{
				if (language == "en-US")
					return new TranslationEN_US();
				if (language == "de-DE")
					return new TranslationDE_DE();
				if (language == "fr-FR")
					return new TranslationFR_FR();
				return null;
			}

			public override Dictionary<string, int> GetKeyNames()
			{
                Dictionary<string, int> keyNames = new Dictionary<string, int>();
				for (int i = 0; i < 110; i++)
				{
					keyNames.Add(((Key)i).ToString(), i);
				}
                return keyNames;
			}
		}

		private class TranslationEN_US : ITranslationProvider
		{
			public string Get(int key) 
			{
				switch((Key)key)
				{
					case Key.SupportedLanguages:
						return "english";
					case Key.BalancerMainNode:
						return "Geistlich Pharma AG";
					case Key.GeneralSettingsSubNode:
						return "Allgemeine Einstell.";
					case Key.ContainerSubNode:
						return "Behälter";
					case Key.DigitalIOSubNode:
						return "Digital IO";
					case Key.IPAddress:
						return "IP-Addresse";
					case Key.Port:
						return "Port";
					case Key.Back:
						return "Zurück";
					case Key.Pause:
						return "Unterbruch";
					case Key.BalanceLabel:
						return "Waage";
					case Key.Continue:
						return "Weiterfahren";
					case Key.InputPassword:
						return "Passwort eingeben";
					case Key.Username:
						return "Benutzername:";
					case Key.Login:
						return "Anmelden";
					case Key.Password:
						return "Passwort:";
					case Key.InputUsername:
						return "Benutzername eingeben";
					case Key.CurrentUser:
						return "Angemeldeter Benutzer:";
					case Key.PasswordExpired:
						return "Passwort abgelaufen";
					case Key.WrongUserPassword:
						return "Benutzer oder Passwort falsch";
					case Key.PasswordPolicy:
						return "Password does not meet requirements";
					case Key.PwChangeSucceed:
						return "Change Password successfull";
					case Key.MustNotBeEmpty:
						return "must not be empty!";
					case Key.Logout:
						return "Logout";
					case Key.LogoutQuestion:
						return "Do you really want to logout?";
					case Key.Gross:
						return "Brutto";
					case Key.Tare:
						return "Tara";
					case Key.Scale:
						return "Waage";
					case Key.ScaleOverload:
						return "Überlast Waage";
					case Key.ScaleUnderload:
						return "Unterlast Waage";
					case Key.InvalidWeight:
						return "Ungültiges Gewicht!";
					case Key.Quit:
						return "Beenden";
					case Key.TargetValue:
						return "Zielwert";
					case Key.ActualValue:
						return "Aktueller Wert";
					case Key.CoverplateWeight:
						return "Gewicht Abdeckung";
					case Key.Print:
						return "Drucken";
					case Key.Reprint:
						return "Reprint";
					case Key.Cancel:
						return "Abbrechen";
					case Key.CancelProcess:
						return "Prozess abbrechen";
					case Key.Preview:
						return "Vorschau";
					case Key.StartProgram:
						return "Programm starten";
					case Key.Start:
						return "Starten";
					case Key.RecipeNr:
						return "Rezeptnummer";
					case Key.Yes:
						return "Ja";
					case Key.No:
						return "Nein";
					case Key.Ok:
						return "Ok";
					case Key.Amount_Overdosed:
						return "Anzahl Überfüllungen:";
					case Key.Amount_InTolerance:
						return "Anzahl Abfüllungen in Toleranz:";
					case Key.CurrentDosingNr:
						return "Aktuelle Abfüllnummer:";
					case Key.UnloadScale:
						return "Please unload scale!";
					case Key.WaitTare:
						return "Bitte Waage tarieren!";
					case Key.ScaleReady:
						return "Waage bereit!";
					case Key.LoadScale:
						return "Bitte {0} in die Vorrichtung setzen.";
					case Key.Zero:
						return "Nullieren";
					case Key.ClearTare:
						return "Tara löschen";
					case Key.RecipeDone:
						return "Artikel registriert!";
					case Key.ConnectionTimeOut:
						return "No connection to the host!";
					case Key.ConnectionFailed_Title:
						return "Verbindung fehlgeschlagen!";
					case Key.ConnectionFailed_Message:
						return "Waage konnte nicht initialisiert werden!";
					case Key.PenWeight:
						return "Gewicht (Pen) [g]";
					case Key.VialWeight:
						return "Gewicht (Vial) [g]";
					case Key.BioFlowWeight:
						return "Gewicht (Bio-Flow) [g]";
					case Key.BottleWeight:
						return "Gewicht (Flasche) [g]";
					case Key.PenTolerance:
						return "Toleranz (Pen) [g]";
					case Key.VialTolerance:
						return "Toleranz (Vial) [g]";
					case Key.BioFlowTolerance:
						return "Toleranz (Bio-Flow) [g]";
					case Key.BottleTolerance:
						return "Toleranz (Flasche) [g]";
					case Key.FastFillMessage:
						return "Grobdosierung läuft...";
					case Key.SlowFillMessage:
						return "Feindosierung läuft...";
					case Key.APR331Kontrast:
						return "APR331 Kontrast";
					case Key.APR331Widerstand:
						return "APR331 Widerstandsklasse";
					case Key.PrinterSubNode:
						return "Drucker APR331";
					case Key.WaitingDurationSubNode:
						return "Wartedauer [s]";
					case Key.PulsingDurationSubNode:
						return "Plusierungsdauer [s]";
					case Key.NoActionTimer:
						return "Zustandsänderungsdauer [s]";
					case Key.NoStateChanged_Title:
						return "Keine Zustandsänderung!";
					case Key.NoStateChanged_Content:
						return "Es gab keine Zustandsänderung in den letzten {0} Sekunden.";
					case Key.ContinueOrQuitCharge:
						return "Bitte Charge fortsetzen oder beenden.";
					case Key.Overdosed:
						return "Überdosiert!";
					case Key.Done:
						return "Erledigt";
					case Key.ProzessPaused:
						return "Prozess wurde angehalten!";
					case Key.ChargeDone_TitleQuestion:
						return "Charge beenden?";
					case Key.ChargeDone_ContentQuestion:
						return "Möchten Sie die Charge '{0}' wirklich beenden?";
					case Key.TareFailed_Title:
						return "Tara fehlgeschlagen!";
					case Key.TareFailed_Content:
						return "Waage konnte nicht tarieren!";
					case Key.ChargrNr:
						return "Chargenr.";
					case Key.ChargrNumber:
						return "Chargennummer";
					case Key.Waiting:
						return "Warten";
					case Key.Pulsing:
						return "Pulsieren";
					case Key.RecipeSelection:
						return "Rezeptauswahl";
					case Key.RecipeManagement:
						return "Rezeptverwaltung";
					case Key.RecipeVersion:
						return "Rezeptversion";
					case Key.LastChanged:
						return "Zuletzt geändert";
					case Key.PutContentInFunnel:
						return "Bitte abgefüllten Behälter in den Trichten leeren.";
					case Key.DosingSucceeded:
						return "Dosierung erfolgreich abgeschlossen!";
					case Key.FilledValue:
						return "Abgefüllter Wert";
					case Key.SearchRecipe:
						return "Rezepte suchen!";
					case Key.NoRecipesFound:
						return "Keine Rezepte unter '{0}' gefunden!";
					case Key.RemoveFullContainer:
						return "Bitte abgefüllten Behälter von der Waage nehmen.";
					case Key.WrongHashCode_Title:
						return "Hashcode falsch!";
					case Key.WrongHashCode_Message:
						return "Hashcode stimmt nicht überein! Die Rezeptliste wurde modifiziert.";
					case Key.WrongRecipeNr_Title:
						return "Falsche Rezeptnummer!";
					case Key.WrongRecipeNr_Message:
						return "Rezeptnummer stimmt nicht mit dem ausgewählten Rezept überein.";
					case Key.ShortChargeNr_Title:
						return "Chargennummer zu kurz!";
					case Key.ShortChargeNr_Message:
						return "Die Chargennummer muss genau 8 Ziffern beinhalten.";
					case Key.RecipesNotFoundTitle:
						return "Keine Rezepte gefunden!";
					case Key.HashcodeNotFoundTitle:
						return "Kein Hashcode gefunden!";
					case Key.RecipesNotFoundMessage:
						return "Folgende Datei ist nicht vorhanden:";
					case Key.CoarseDosing:
						return "Grobdosierung";
					case Key.PreciseDosing:
						return "Feindosierung";
					case Key.Stop:
						return "Stoppen";
					default:
						return string.Empty;
				}
			}

			public string Language
			{
				get { return "en-US"; }
			}
		}
		private class TranslationDE_DE : ITranslationProvider
		{
			public string Get(int key) 
			{
				switch((Key)key)
				{
					case Key.SupportedLanguages:
						return "deutsch";
					case Key.GeneralSettingsSubNode:
						return "Allgemeine Einstell.";
					case Key.ContainerSubNode:
						return "Behälter";
					case Key.DigitalIOSubNode:
						return "Digital IO";
					case Key.IPAddress:
						return "IP-Addresse";
					case Key.Back:
						return "Zurück";
					case Key.Pause:
						return "Unterbruch";
					case Key.BalanceLabel:
						return "Waage";
					case Key.Continue:
						return "Weiterfahren";
					case Key.InputPassword:
						return "Passwort eingeben";
					case Key.Username:
						return "Benutzername:";
					case Key.Login:
						return "Anmelden";
					case Key.Password:
						return "Passwort:";
					case Key.InputUsername:
						return "Benutzername eingeben";
					case Key.CurrentUser:
						return "Angemeldeter Benutzer:";
					case Key.PasswordExpired:
						return "Passwort abgelaufen";
					case Key.WrongUserPassword:
						return "Benutzer oder Passwort falsch";
					case Key.PasswordPolicy:
						return "Passwort entspricht nicht der Policy";
					case Key.PwChangeSucceed:
						return "Passwort erfolgreich geändert";
					case Key.MustNotBeEmpty:
						return "darf nicht leer sein!";
					case Key.Logout:
						return "Abmelden";
					case Key.LogoutQuestion:
						return "Wollen Sie sich wirklich abmelden?";
					case Key.Gross:
						return "Brutto";
					case Key.Tare:
						return "Tara";
					case Key.Scale:
						return "Waage";
					case Key.ScaleOverload:
						return "Überlast Waage";
					case Key.ScaleUnderload:
						return "Unterlast Waage";
					case Key.InvalidWeight:
						return "Ungültiges Gewicht!";
					case Key.Quit:
						return "Beenden";
					case Key.TargetValue:
						return "Zielwert";
					case Key.ActualValue:
						return "Aktueller Wert";
					case Key.CoverplateWeight:
						return "Gewicht Abdeckung";
					case Key.Print:
						return "Drucken";
					case Key.Reprint:
						return "Reprint";
					case Key.Cancel:
						return "Abbrechen";
					case Key.CancelProcess:
						return "Prozess abbrechen";
					case Key.Preview:
						return "Vorschau";
					case Key.StartProgram:
						return "Programm starten";
					case Key.Start:
						return "Starten";
					case Key.RecipeNr:
						return "Rezeptnummer";
					case Key.Yes:
						return "Ja";
					case Key.No:
						return "Nein";
					case Key.Ok:
						return "Ok";
					case Key.Amount_Overdosed:
						return "Anzahl Überfüllungen:";
					case Key.Amount_InTolerance:
						return "Anzahl Abfüllungen in Toleranz:";
					case Key.CurrentDosingNr:
						return "Aktuelle Abfüllnummer:";
					case Key.UnloadScale:
						return "Bitte Waage entlasten!";
					case Key.WaitTare:
						return "Bitte Waage tarieren!";
					case Key.ScaleReady:
						return "Waage bereit!";
					case Key.LoadScale:
						return "Bitte {0} in die Vorrichtung setzen.";
					case Key.Zero:
						return "Nullieren";
					case Key.ClearTare:
						return "Tara löschen";
					case Key.RecipeDone:
						return "Artikel registriert!";
					case Key.ConnectionTimeOut:
						return "Keine Verbindung zum Host!";
					case Key.ConnectionFailed_Title:
						return "Verbindung fehlgeschlagen!";
					case Key.ConnectionFailed_Message:
						return "Waage konnte nicht initialisiert werden!";
					case Key.PenWeight:
						return "Gewicht (Pen) [g]";
					case Key.VialWeight:
						return "Gewicht (Vial) [g]";
					case Key.BioFlowWeight:
						return "Gewicht (Bio-Flow) [g]";
					case Key.BottleWeight:
						return "Gewicht (Flasche) [g]";
					case Key.PenTolerance:
						return "Toleranz (Pen) [g]";
					case Key.VialTolerance:
						return "Toleranz (Vial) [g]";
					case Key.BioFlowTolerance:
						return "Toleranz (Bio-Flow) [g]";
					case Key.BottleTolerance:
						return "Toleranz (Flasche) [g]";
					case Key.FastFillMessage:
						return "Grobdosierung läuft...";
					case Key.SlowFillMessage:
						return "Feindosierung läuft...";
					case Key.APR331Kontrast:
						return "APR331 Kontrast";
					case Key.APR331Widerstand:
						return "APR331 Widerstandsklasse";
					case Key.PrinterSubNode:
						return "Drucker APR331";
					case Key.WaitingDurationSubNode:
						return "Wartedauer [s]";
					case Key.PulsingDurationSubNode:
						return "Plusierungsdauer [s]";
					case Key.NoActionTimer:
						return "Zustandsänderungsdauer [s]";
					case Key.NoStateChanged_Title:
						return "Keine Zustandsänderung!";
					case Key.NoStateChanged_Content:
						return "Es gab keine Zustandsänderung in den letzten {0} Sekunden.";
					case Key.ContinueOrQuitCharge:
						return "Bitte Charge fortsetzen oder beenden.";
					case Key.Overdosed:
						return "Überdosiert!";
					case Key.Done:
						return "Erledigt";
					case Key.ProzessPaused:
						return "Prozess wurde angehalten!";
					case Key.ChargeDone_TitleQuestion:
						return "Charge beenden?";
					case Key.ChargeDone_ContentQuestion:
						return "Möchten Sie die Charge '{0}' wirklich beenden?";
					case Key.TareFailed_Title:
						return "Tara fehlgeschlagen!";
					case Key.TareFailed_Content:
						return "Waage konnte nicht tarieren!";
					case Key.ChargrNr:
						return "Chargenr.";
					case Key.ChargrNumber:
						return "Chargennummer";
					case Key.Waiting:
						return "Warten";
					case Key.Pulsing:
						return "Pulsieren";
					case Key.RecipeSelection:
						return "Rezeptauswahl";
					case Key.RecipeManagement:
						return "Rezeptverwaltung";
					case Key.RecipeVersion:
						return "Rezeptversion";
					case Key.LastChanged:
						return "Zuletzt geändert";
					case Key.PutContentInFunnel:
						return "Bitte abgefüllten Behälter in den Trichten leeren.";
					case Key.DosingSucceeded:
						return "Dosierung erfolgreich abgeschlossen!";
					case Key.FilledValue:
						return "Abgefüllter Wert";
					case Key.SearchRecipe:
						return "Rezepte suchen!";
					case Key.NoRecipesFound:
						return "Keine Rezepte unter '{0}' gefunden!";
					case Key.RemoveFullContainer:
						return "Bitte abgefüllten Behälter von der Waage nehmen.";
					case Key.WrongHashCode_Title:
						return "Hashcode falsch!";
					case Key.WrongHashCode_Message:
						return "Hashcode stimmt nicht überein! Die Rezeptliste wurde modifiziert.";
					case Key.WrongRecipeNr_Title:
						return "Falsche Rezeptnummer!";
					case Key.WrongRecipeNr_Message:
						return "Rezeptnummer stimmt nicht mit dem ausgewählten Rezept überein.";
					case Key.ShortChargeNr_Title:
						return "Chargennummer zu kurz!";
					case Key.ShortChargeNr_Message:
						return "Die Chargennummer muss genau 8 Ziffern beinhalten.";
					case Key.RecipesNotFoundTitle:
						return "Keine Rezepte gefunden!";
					case Key.HashcodeNotFoundTitle:
						return "Kein Hashcode gefunden!";
					case Key.RecipesNotFoundMessage:
						return "Folgende Datei ist nicht vorhanden:";
					case Key.CoarseDosing:
						return "Grobdosierung";
					case Key.PreciseDosing:
						return "Feindosierung";
					case Key.Stop:
						return "Stoppen";
					default:
						return string.Empty;
				}
			}

			public string Language
			{
				get { return "de-DE"; }
			}
		}
		private class TranslationFR_FR : ITranslationProvider
		{
			public string Get(int key) 
			{
				switch((Key)key)
				{
					case Key.SupportedLanguages:
						return "français";
					default:
						return string.Empty;
				}
			}

			public string Language
			{
				get { return "fr-FR"; }
			}
		}
	}
}