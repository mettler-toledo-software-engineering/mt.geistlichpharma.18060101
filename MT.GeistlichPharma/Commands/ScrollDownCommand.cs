﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using MT.GeistlichPharma.Infrastructure;
using MT.GeistlichPharma.Pages;
using MT.Singularity.Logging;

namespace MT.GeistlichPharma.Commands
{
    public class ScrollDownCommand : CommandBase
    {

        private readonly IScrollableViewModel _viewModel;
        public ScrollDownCommand(IScrollableViewModel viewModel)
        {
            _viewModel = viewModel;
            _viewModel.PropertyChanged += ViewModelOnPropertyChanged;
        }

        private void ViewModelOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(_viewModel.ListIndex) || e.PropertyName == nameof(_viewModel.ListCount))
            {

                OnCanExecuteChanged();
            }
        }

        public override bool CanExecute(object parameter)
        {
            return base.CanExecute(parameter) && _viewModel.ListIndex < _viewModel.ListCount - 1;
        }

        public override void Execute(object parameter)
        {
            try
            {
                if (_viewModel.ListIndex + 5 >= _viewModel.ListCount)
                {
                    _viewModel.ListIndex = _viewModel.ListCount - 1;
                }
                else
                {
                    _viewModel.ListIndex = _viewModel.ListIndex + 5;
                }
            }
            catch (Exception e)
            {
                Logger.ErrorEx(e.Message, SourceClass, e);
            }

        }
    }
}
