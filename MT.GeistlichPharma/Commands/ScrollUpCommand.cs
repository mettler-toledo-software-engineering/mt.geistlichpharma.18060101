﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using MT.GeistlichPharma.Infrastructure;
using MT.GeistlichPharma.Pages;
using MT.Singularity.Logging;

namespace MT.GeistlichPharma.Commands
{
    public class ScrollUpCommand : CommandBase
    {

        private readonly IScrollableViewModel _viewModel;
        public ScrollUpCommand(IScrollableViewModel viewModel)
        {
            _viewModel = viewModel;
            _viewModel.PropertyChanged += ViewModelOnPropertyChanged;
        }

        private void ViewModelOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(_viewModel.ListIndex) || e.PropertyName == nameof(_viewModel.ListCount))
            {

                OnCanExecuteChanged();
            }
        }

        public override bool CanExecute(object parameter)
        {
            return base.CanExecute(parameter) && _viewModel.ListIndex > 0;
        }

        public override void Execute(object parameter)
        {

            try
            {
                if (_viewModel.ListIndex <= 5)
                {
                    _viewModel.ListIndex = 0;
                }
                else
                {
                    _viewModel.ListIndex = _viewModel.ListIndex - 5;
                }
            }
            catch (Exception e)
            {
                Logger.ErrorEx(e.Message, SourceClass, e);
            }
        }
    }
}
