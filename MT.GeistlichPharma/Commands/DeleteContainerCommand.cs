﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using MT.GeistlichPharma.Logic.MTSettings;
using MT.GeistlichPharma.Pages;
using MT.Singularity.Presentation.Controls;

namespace MT.GeistlichPharma.Commands
{
    public class DeleteContainerCommand : CommandBase
    {
        private readonly ManageContainerViewModel _manageContainerViewModel;
        private bool _canExecute;
        private readonly Visual _parent;
        public DeleteContainerCommand(ManageContainerViewModel manageContainerViewModel, Visual parent)
        {
            _manageContainerViewModel = manageContainerViewModel;
            _parent = parent;
            _manageContainerViewModel.PropertyChanged += ManageContainerViewModelOnPropertyChanged;
        }

        private void ManageContainerViewModelOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnCanExecuteChanged();
        }

        public override bool CanExecute(object parameter)
        {
            _canExecute = _manageContainerViewModel.SelectedContainer != null;
            return base.CanExecute(parameter) && _canExecute;
        }

        public override void Execute(object parameter)
        {
            MessageBox.Show(_parent, "Sind Sie sicher, dass Sie den Behälter wirklich löschen wollen?", "Achtung!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, result =>
            {
                if (result == DialogResult.Yes)
                {
                    Settings.Current.DeleteContainer(_manageContainerViewModel.SelectedContainer);
                    _manageContainerViewModel.Container.Remove(_manageContainerViewModel.SelectedContainer);
                    _manageContainerViewModel.UpdateContainerList();


                    if (_manageContainerViewModel.Container?.Count > 0)
                    {
                        _manageContainerViewModel.SelectedContainer = _manageContainerViewModel.Container[0];
                        _manageContainerViewModel.ListIndex = 0;
                    }
                }
            });

        }
    }
}
