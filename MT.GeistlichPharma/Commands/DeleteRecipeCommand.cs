﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using MT.GeistlichPharma.Logic.MTSettings;
using MT.GeistlichPharma.Pages;
using MT.Singularity.Presentation.Controls;

namespace MT.GeistlichPharma.Commands
{
    public class DeleteRecipeCommand : CommandBase
    {
        private readonly ManageRecipesViewModel _manageRecipesViewModel;
        private bool _canExecute;
        private readonly Visual _parent;
        public DeleteRecipeCommand(ManageRecipesViewModel manageRecipesViewModel, Visual parent)
        {
            _manageRecipesViewModel = manageRecipesViewModel;
            _parent = parent;
            _manageRecipesViewModel.PropertyChanged += ManageRecipesViewModelOnPropertyChanged;
        }

        private void ManageRecipesViewModelOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnCanExecuteChanged();
        }

        public override bool CanExecute(object parameter)
        {
            _canExecute = _manageRecipesViewModel.SelectedRecipe != null;
            return base.CanExecute(parameter) && _canExecute;
        }

        public override void Execute(object parameter)
        {
            MessageBox.Show(_parent, "Sind Sie sicher, dass Sie das Rezept wirklich löschen wollen?", "Achtung!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, result =>
            {
                if (result == DialogResult.Yes)
                {
                    Settings.Current.DeleteRecipe(_manageRecipesViewModel.SelectedRecipe);
                    _manageRecipesViewModel.Recipes.Remove(_manageRecipesViewModel.SelectedRecipe);
                    _manageRecipesViewModel.UpdateRecipeList();


                    if (_manageRecipesViewModel.Recipes?.Count > 0)
                    {
                        _manageRecipesViewModel.SelectedRecipe = _manageRecipesViewModel.Recipes[0];
                        _manageRecipesViewModel.ListIndex = 0;
                    }
                }
            });
            
        }
    }
}
