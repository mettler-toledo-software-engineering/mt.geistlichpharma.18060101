﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using MT.GeistlichPharma.Logic.MTSettings;
using MT.GeistlichPharma.Pages;
using MT.GeistlichPharma.Stores;
using MT.Singularity.Platform.Configuration;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;

namespace MT.GeistlichPharma.Commands
{
    public class NavigateSelectRecipeCommand : CommandBase
    {
        private readonly Visual _parentVisual;
        private readonly AnimatedNavigationFrame _navigationFrame;
        private readonly ProcessStore _processStore;
        private readonly StartPageViewModel _viewModel;


        public NavigateSelectRecipeCommand(StartPageViewModel viewModel, Visual parent, AnimatedNavigationFrame homeNavigationFrame, ProcessStore processStore)
        {
            _parentVisual = parent;
            _navigationFrame = homeNavigationFrame;
            _processStore = processStore;
            _viewModel = viewModel;

        }


        public override void Execute(object parameter)
        {
            CheckForOpenProcess();
        }

        private void CheckForOpenProcess()
        {
            string recipes = string.Empty;
            foreach (var recipe in Settings.Current.Recipes)  
            {
                if (recipe.Container == null)
                {
                    recipes = "\n" + "Rezeptnummer: " + recipe.Number + recipes;
                }
            }

            if (recipes != string.Empty)
            {
                MessageBox.Show(_parentVisual, $"Es wurden Rezepte ohne Behälter gefunden: {recipes}", "Rezepte bearbeiten!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            bool processExists = _processStore.CheckIfUnfinishedProcessExists();
            ResetDosingButtons();
            if (processExists)
            {
                MessageBox.Show(_parentVisual, $"Es wurde ein offenes Rezept mit der Nummer: {_processStore.SelectedRecipe?.Number} \nund der Charge: {_processStore.CurrentCharge?.Number} gefunden.\nDas Rezept wird jetzt fortgesetzt.", "Offener Auftrag gefunden", MessageBoxButtons.OK, MessageBoxIcon.Information, dialogResult =>
                {
                    if (dialogResult == DialogResult.OK)
                    {
                        

                        //todo ausdruck berücksichtigen
                        _navigationFrame.NavigateTo(new MainScreen(_navigationFrame, _processStore.SelectedRecipe, _processStore.CurrentCharge), TransitionAnimation.LeftToRight);
                        //todo state im mainscreen setzen
                        //todo json inhalt am ende des Prozesses löschen
                    }
                });

            }
            else
            {

                InfoPopup info;

                if (!File.Exists(Settings.FileName))
                {
                    var title = !File.Exists(Settings.FileName)
                        ? Localization.Get(Localization.Key.RecipesNotFoundTitle)
                        : Localization.Get(Localization.Key.HashcodeNotFoundTitle);

                    var message = Localization.Get(Localization.Key.RecipesNotFoundMessage)
                                  + "\n" + Settings.FileName;

                    info = new InfoPopup(title, message, MessageBoxIcon.Error, MessageBoxButtons.OK);
                    info.Show(_parentVisual);
                }
                else
                {
                    _navigationFrame.NavigateTo(new RecipeSelection(_navigationFrame), TransitionAnimation.LeftToRight);
                }
            }

        }
        private void ResetDosingButtons()
        {
            _viewModel.IsCoarseDosing = false;
            _viewModel.IsPreciseDosing = false;
            _viewModel.DigitalIo.StartStopFill(_viewModel.IsCoarseDosing, _viewModel.IsPreciseDosing);

            _viewModel.ShowCoarseDosingButton(true);
            _viewModel.ShowPreciseDosingButton(true);
        }
    }
}
