﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Threading;
using MT.GeistlichPharma.Logic.MTSettings;
using MT.GeistlichPharma.Logic.Process;
using MT.GeistlichPharma.Pages;
using MT.GeistlichPharma.Stores;
using MT.Singularity.Logging;
using MT.Singularity.Presentation.Controls;

namespace MT.GeistlichPharma.Commands
{
    public class SelectRecipeCommand : CommandBase
    {
        private readonly RecipeSelectionViewModel _pickRecipeViewModel;
        private readonly ProcessStore _processStore;
        public SelectRecipeCommand(RecipeSelectionViewModel pickRecipeViewModel, ProcessStore processStore)
        {
            _pickRecipeViewModel = pickRecipeViewModel;
            _processStore = processStore;
        }

        public override async void Execute(object parameter)
        {
            try
            {
                var recipe = parameter as Recipe;
                _pickRecipeViewModel.SelectedRecipe = recipe;
                _processStore.SetRecipe(recipe);

                var rinfo = new RecipeInformation(_pickRecipeViewModel);
                var result = await rinfo.ShowAsync(_pickRecipeViewModel.Parent);


                if (result == DialogResult.OK)
                {
                    
                    Charge charge = new Charge(_pickRecipeViewModel.Charge);
                    _processStore.SetCharge(charge);

                    var isCorrectRecipe = _pickRecipeViewModel.SelectedRecipe.Number.Equals(_pickRecipeViewModel.RecipeNumber);
                    var isLengthInRange = charge.Number.Length == 8;

                    var title = Localization.Get(Localization.Key.WrongRecipeNr_Title);
                    var message = Localization.Get(Localization.Key.WrongRecipeNr_Message);

                    if (isCorrectRecipe && isLengthInRange)
                    {
                        charge.StartTime = DateTime.Now;
                        _pickRecipeViewModel.Rapport.Charge = charge;
                        _pickRecipeViewModel.Rapport.Recipe = _pickRecipeViewModel.SelectedRecipe;

                        var printHeader = new Thread(async () => { await _pickRecipeViewModel.Rapport.PrintHeaderAsync(); });
                        printHeader.Start();

                        _pickRecipeViewModel.HomeNavigationFrame.NavigateTo(new MainScreen(_pickRecipeViewModel.HomeNavigationFrame, _processStore.SelectedRecipe, _processStore.CurrentCharge), TransitionAnimation.LeftToRight);
                    }
                    else
                    {
                        if (isCorrectRecipe)
                        {
                            title = Localization.Get(Localization.Key.ShortChargeNr_Title);
                            message = Localization.Get(Localization.Key.ShortChargeNr_Message);
                        }

                        var info = new InfoPopup(title, message, MessageBoxIcon.Error, MessageBoxButtons.OK);
                        info.Show(_pickRecipeViewModel.Parent);
                    }
                }




            }
            catch (Exception e)
            {
                Logger.ErrorEx(e.Message, SourceClass, e);

            }

        }
    }
}
