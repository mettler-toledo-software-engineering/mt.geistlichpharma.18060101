﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using MT.GeistlichPharma.Logic.MTSettings;
using MT.GeistlichPharma.Pages;
using MT.Singularity.Presentation.Controls;

namespace MT.GeistlichPharma.Commands
{
    public class UpdateContainerCommand : CommandBase
    {
        private bool _canExecute;
        private readonly ManageContainerViewModel _manageContainerViewModel;
        private readonly Visual _parent;
        public UpdateContainerCommand(ManageContainerViewModel manageContainerViewModel, Visual parent)
        {
            _manageContainerViewModel = manageContainerViewModel;
            _parent = parent;
            _manageContainerViewModel.PropertyChanged += ManageContainerViewModelOnPropertyChanged;
        }

        private void ManageContainerViewModelOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnCanExecuteChanged();
        }

        public override bool CanExecute(object parameter)
        {
            bool rule1 = _manageContainerViewModel.Name != null && _manageContainerViewModel.Name != string.Empty;
            bool rule2 = _manageContainerViewModel.Weight > 0;
            bool rule3 = _manageContainerViewModel.Tolerance > 0;


            _canExecute = rule1 && rule2 && rule3;
            return base.CanExecute(parameter) && _canExecute;
        }

        public override void Execute(object parameter)
        {
            var guid = _manageContainerViewModel.SelectedContainer.Id;
            Settings.Current.UpdateContainer(_manageContainerViewModel.SelectedContainer);

            _manageContainerViewModel.UpdateContainerList();

            _manageContainerViewModel.SelectedItem = _manageContainerViewModel.Container.FirstOrDefault(c => c.Id == guid);

            MessageBox.Show(_parent, "Behälter wurde aktualisiert.", string.Empty, MessageBoxButtons.OK, MessageBoxIcon.Information);


        }
    }
}
