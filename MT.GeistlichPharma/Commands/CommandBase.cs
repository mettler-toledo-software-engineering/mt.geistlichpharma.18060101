﻿using System;

using System.Collections.Generic;
using System.Text;
using log4net;
using MT.Singularity.Logging;
using MT.Singularity.Presentation.Model;

namespace MT.GeistlichPharma.Commands
{
    public abstract class CommandBase : ICommand
    {
        protected static readonly ILog Logger = Log4NetManager.ApplicationLogger;
        protected static readonly string SourceClass = nameof(CommandBase);

        public event EventHandler CanExecuteChanged;

        public virtual bool CanExecute(object parameter)
        {
            return true;
        }

        public abstract void Execute(object parameter);

        protected void OnCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, new EventArgs());
        }
    }
}
