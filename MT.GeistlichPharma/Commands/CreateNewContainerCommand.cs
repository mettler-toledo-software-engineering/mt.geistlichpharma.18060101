﻿using System;

using System.Collections.Generic;
using System.Text;
using MT.GeistlichPharma.Logic.MTSettings;
using MT.GeistlichPharma.Pages;

namespace MT.GeistlichPharma.Commands
{
    public class CreateNewContainerCommand : CommandBase
    {
        private readonly ManageContainerViewModel _manageContainerViewModel;
        public CreateNewContainerCommand(ManageContainerViewModel manageContainerViewModel)
        {
            _manageContainerViewModel = manageContainerViewModel;
        }
        public override void Execute(object parameter)
        {
            var container = new Container();
            container.Id = Guid.NewGuid();
            container.Name = "neu";
            container.Weight = 0;
            container.Tolerance = 0;
            
            _manageContainerViewModel.Container.Insert(0, container);
            _manageContainerViewModel.ListIndex = 0;
            _manageContainerViewModel.SelectedItem = _manageContainerViewModel.Container[0];

            Settings.Current.AddContainer(container);

        }
    }
}
