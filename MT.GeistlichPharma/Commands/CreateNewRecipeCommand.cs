﻿using System;

using System.Collections.Generic;
using System.Text;
using MT.GeistlichPharma.Logic.MTSettings;
using MT.GeistlichPharma.Pages;

namespace MT.GeistlichPharma.Commands
{
    public class CreateNewRecipeCommand : CommandBase
    {
        private readonly ManageRecipesViewModel _manageRecipesViewModel;
        public CreateNewRecipeCommand(ManageRecipesViewModel manageRecipesViewModel)
        {
            _manageRecipesViewModel = manageRecipesViewModel;
        }
        public override void Execute(object parameter)
        {
            var recipe = new Recipe();
            recipe.RecipeId = Guid.NewGuid();
            recipe.Number = "Neu";
            recipe.Description = "Neu";
            recipe.AcceptanceMin = 0;
            recipe.AcceptanceMax = 0;
            recipe.Limit1 = 0;
            recipe.Limit2 = 0;
            recipe.Target = 0;
            recipe.Container = new Container();


            _manageRecipesViewModel.Recipes.Insert(0,recipe);
            _manageRecipesViewModel.ListIndex = 0;
            _manageRecipesViewModel.SelectedItem = _manageRecipesViewModel.Recipes[0];

            Settings.Current.AddRecipe(recipe);
            
        }
    }
}
