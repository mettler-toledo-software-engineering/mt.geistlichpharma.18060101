﻿using System;

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using MT.GeistlichPharma.Logic.MTSettings;
using MT.GeistlichPharma.Pages;
using MT.Singularity.Presentation.Controls;

namespace MT.GeistlichPharma.Commands
{
    public class UpdateRecipeCommand : CommandBase
    {
        private bool _canExecute;
        private readonly ManageRecipesViewModel _manageRecipesViewModel;
        private readonly Visual _parent;
        public UpdateRecipeCommand(ManageRecipesViewModel manageRecipesViewModel,Visual parent)
        {
            _manageRecipesViewModel = manageRecipesViewModel;
            _parent = parent;
            _manageRecipesViewModel.PropertyChanged += ManageRecipesViewModelOnPropertyChanged;
        }

        private void ManageRecipesViewModelOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnCanExecuteChanged();
        }

        public override bool CanExecute(object parameter)
        {
            bool rule1 = _manageRecipesViewModel.AcceptanceMax > _manageRecipesViewModel.AcceptanceMin;
            bool rule2 = _manageRecipesViewModel.AcceptanceMax > _manageRecipesViewModel.Target;
            bool rule3 = _manageRecipesViewModel.Target < _manageRecipesViewModel.AcceptanceMin;

            bool rule4 = _manageRecipesViewModel.Limit1 < _manageRecipesViewModel.Limit2;
            bool rule5 = _manageRecipesViewModel.Number.Length == 6 && _manageRecipesViewModel.Number.All(char.IsDigit); 
            bool rule6 = string.IsNullOrEmpty(_manageRecipesViewModel.Description) == false;
            bool rule7 = _manageRecipesViewModel.SelectedRecipe?.Container != null;

            _canExecute = rule1 && rule2 && rule3 && rule4 && rule5 && rule6 && rule7;
            return base.CanExecute(parameter) && _canExecute;
        }

        public override void Execute(object parameter)
        {

            _manageRecipesViewModel.SelectedRecipe.LastModified = DateTime.Now;

            var guid = _manageRecipesViewModel.SelectedRecipe.RecipeId;
            Settings.Current.UpdateRecipe(_manageRecipesViewModel.SelectedRecipe);

            _manageRecipesViewModel.UpdateRecipeList();

            _manageRecipesViewModel.SelectedItem = _manageRecipesViewModel.Recipes.FirstOrDefault(r => r.RecipeId == guid);

            MessageBox.Show(_parent, "Rezept wurde gespeichert.", string.Empty, MessageBoxButtons.OK, MessageBoxIcon.Information);


        }
    }
}
