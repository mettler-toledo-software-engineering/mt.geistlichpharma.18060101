﻿using MT.GeistlichPharma.Logic.Process;

namespace MT.GeistlichPharma.Infrastructure
{
    public interface IProcessSerializationService : IGenericSerializationService<Process>
    {
    }
}