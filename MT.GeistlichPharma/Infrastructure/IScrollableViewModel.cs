﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MT.GeistlichPharma.Infrastructure
{
    public interface IScrollableViewModel : INotifyPropertyChanged
    {
        int ListIndex { get; set; }
        int ListCount { get; }

    }
}
