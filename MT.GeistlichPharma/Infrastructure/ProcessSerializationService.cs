﻿using System;

using System.Collections.Generic;
using System.Text;
using MT.GeistlichPharma.Logic.Process;

namespace MT.GeistlichPharma.Infrastructure
{
    public class ProcessSerializationService : GenericSerializationService<Process>, IProcessSerializationService
    {
    }
}
