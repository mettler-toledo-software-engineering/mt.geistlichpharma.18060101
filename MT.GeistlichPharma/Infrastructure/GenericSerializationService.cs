﻿using System;

using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using MT.GeistlichPharma.Logic;
using MT.GeistlichPharma.Logic.HashCode;
using MT.Singularity.Platform;
using Newtonsoft.Json;

namespace MT.GeistlichPharma.Infrastructure
{
    public class GenericSerializationService<T> : IGenericSerializationService<T> where T : class, new()
    {

        private readonly XmlSerializer _xmlSerializer = new XmlSerializer(typeof(T));


        public void WriteClassToJson(T classToWrite)
        {
            if (Directory.Exists(Globals.DataDirectory) == false)
            {
                Directory.CreateDirectory(Globals.DataDirectory);
            }
            Debug.WriteLine(Globals.DataDirectory);

            //string jsonString = JsonConvert.SerializeObject(classToWrite, Formatting.Indented);

            string jsonFile = Path.Combine(Globals.DataDirectory, $"{typeof(T).Name}.json");
            string xmlFile = Path.Combine(Globals.DataDirectory, $"{typeof(T).Name}.xml");



            using (FileStream fs = new FileStream(xmlFile, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                _xmlSerializer.Serialize(fs, classToWrite);
            }

        }



        public T ReadClassFromJson()
        {
            string jsonFile = Path.Combine(Globals.DataDirectory, $"{typeof(T).Name}.json");
            string xmlFile = Path.Combine(Globals.DataDirectory, $"{typeof(T).Name}.xml");

            if (Directory.Exists(Globals.DataDirectory) == false)
            {
                Directory.CreateDirectory(Globals.DataDirectory);
            }

            //using (var sr = new StreamReader(_jsonFile, Encoding.Default))
            //{
            //    string jsonString = sr.ReadToEnd();
            //    T result = JsonConvert.DeserializeObject<T>(jsonString);

            //    return result;
            //}


            using (FileStream fs = new FileStream(xmlFile, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                 var result = _xmlSerializer.Deserialize(fs);
                 return (T) result;
            }

            //string jsonString = File.ReadAllText(jsonFile);
            //T result = JsonConvert.DeserializeObject<T>(jsonString);

            //return result;
        }

        public bool CheckifJsonExists()
        {
            if (Directory.Exists(Globals.DataDirectory) == false)
            {
                Directory.CreateDirectory(Globals.DataDirectory);
            }
            string jsonFile = Path.Combine(Globals.DataDirectory, $"{typeof(T).Name}.json");
            string xmlFile = Path.Combine(Globals.DataDirectory, $"{typeof(T).Name}.xml");

            // return File.Exists(jsonFile);
            return File.Exists(xmlFile);
        }

        public void CreateJsonIfItDoesNotExists(T classToWrite)
        {
            if (CheckifJsonExists() == false)
            {
                WriteClassToJson(classToWrite);
            }
        }
    }
}
