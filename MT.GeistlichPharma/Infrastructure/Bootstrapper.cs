﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using MT.GeistlichPharma.Configuration;
using MT.GeistlichPharma.Stores;
using MT.Singularity.Composition;
using MT.Singularity.Platform.CommonUX.Infrastructure;
using MT.Singularity.Platform.Configuration;
using MT.Singularity.Platform.Infrastructure;

namespace MT.GeistlichPharma.Infrastructure
{
    /// <summary>
    /// Initializes and runs the application.
    /// </summary>
    internal class Bootstrapper : ClientBootstrapperBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Bootstrapper"/> class.
        /// </summary>
        public Bootstrapper()
            : base(Assembly.GetExecutingAssembly())
        {
        }

        /// <summary>
        /// Gets a value indicating whether it need to create customer database.
        /// </summary>
        protected override bool NeedToCreateCustomerDatabase
        {
            get { return true; }
        }
        
        protected override async void InitializeApplication()
        {
            await InitializeCustomerService();
            base.InitializeApplication();
        }

        private async Task InitializeCustomerService()
        {
            try
            {
                var engine = CompositionContainer.Resolve<IPlatformEngine>();
                var securityService = await engine.GetSecurityServiceAsync();
                var configurationStore = CompositionContainer.Resolve<IConfigurationStore>();
                var balancerComponent = new BalancerComponents(configurationStore, securityService, CompositionContainer);
                CompositionContainer.AddInstance<IBalancerComponents>(balancerComponent);
                CompositionContainer.AddInstance<IProcessSerializationService>(new ProcessSerializationService());
                CompositionContainer.AddInstance<ProcessStore>(new ProcessStore(CompositionContainer.Resolve<IProcessSerializationService>()));
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

    }
}
