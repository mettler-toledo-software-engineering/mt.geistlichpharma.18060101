﻿namespace MT.GeistlichPharma.Infrastructure
{
    public interface IGenericSerializationService<T>
    {
        bool CheckifJsonExists();
        T ReadClassFromJson();
        void WriteClassToJson(T classToWrite);
        void CreateJsonIfItDoesNotExists(T classToWrite);
    }
}