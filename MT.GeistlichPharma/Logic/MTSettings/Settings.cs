﻿using System;
using System.Collections.Generic;

using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Xml.Serialization;

#if IndPro
    using MT.Singularity.Platform;
#endif

namespace MT.GeistlichPharma.Logic.MTSettings
{
    [Serializable]
    public class Settings
    {
#if IndPro
        public static string FileName = Path.Combine(SingularityEnvironment.RunningApplication.DataDirectory, "Settings.xml");
        public static string BackupFileName = Path.Combine(SingularityEnvironment.RunningApplication.DataDirectory, "Settings.xml.bak");
#else
        public static string FileName = Path.Combine(".\\Settings", "Settings.xml");
        public static string BackupFileName = Path.Combine(".\\Settings", "Settings.xml.bak");
#endif
        private static readonly XmlSerializer Serializer = new XmlSerializer(typeof(Settings));
        private static Settings _current;

        #region Load
        private static Settings Load()
        {
            if (!File.Exists(FileName))
            {
                return new Settings();
            }
            else
            {
                try
                {
                    using (FileStream fs = new FileStream(FileName, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        var settings = (Settings)Serializer.Deserialize(fs);

                        foreach (var recipe in settings.Recipes)
                        {
                            if (recipe.RecipeId == Guid.Empty)
                            {
                                recipe.RecipeId = Guid.NewGuid();
                            }
                        }
                        foreach (var container in settings.Container)
                        {
                            if (container.Id == Guid.Empty)
                            {
                                container.Id = Guid.NewGuid();
                            }
                        }

                        return settings;
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine($"{e.Message}");
                    return new Settings();
                }
            }
        }

        public static Settings Current
        {
            get
            {
                //EnsureSettings();
                //return _current;

                //settings werden neu bei jedem Zugriff frisch aus dem XML geholt
                return Load();
            }
        }

        public static void EnsureSettings()
        {
            if (_current == null)
                _current = Load();
        }
        #endregion

        public List<Recipe> Recipes { get; set; }
        public List<Container> Container { get; set; }
        public int Version { get; set; }
        public DateTime LastChanged { get; set; }
        
        /// <summary>
        /// Initializes a new instance of the <see cref="Settings"/> class.
        /// </summary>
        public Settings()
        {
            Recipes = new List<Recipe>();
            Container = new List<Container>();
            Version = 0;
            LastChanged = new DateTime();
        }

        #region Add
        public void AddRecipe(Recipe recipe)
        {
            Recipes.Add(recipe);
        }
        public void AddContainer(Container container)
        {
            Container.Add(container);
        }
        #endregion

        #region Update
        public void UpdateRecipe(int index, Recipe newRecipe)
        {
            Recipes[index] = newRecipe;
        }

        public void UpdateRecipe(Recipe newRecipe)
        {
            Recipes.RemoveAll(r => r.RecipeId == newRecipe.RecipeId);
            Recipes.Add(newRecipe);
            Save();
        }
        public void UpdateContainer(int index, Container newContainer)
        {
            Container[index] = newContainer;
        }

        public void UpdateContainer(Container newContainer)
        {
            Container.RemoveAll(c => c.Id == newContainer.Id);
            foreach (var recipe in Recipes)
            {
                if (recipe.Container?.Id == newContainer.Id)
                {
                    recipe.Container = newContainer;
                }
            }

            ;
            Container.Add(newContainer);
            Save();
        }
        #endregion

        #region Delete
        public void DeleteRecipe(int index)
        {
            Recipes.RemoveAt(index);
        }

        public void DeleteRecipe(Recipe recipe)
        {
            Recipes.RemoveAll(r => r.RecipeId == recipe.RecipeId);
            Save();
        }
        public void DeleteContainer(int index)
        {
            Container.RemoveAt(index);
        }

        public void DeleteContainer(Container container)
        {

            foreach (var recipe in Recipes)
            {
                if (recipe.Container?.Id == container.Id)
                {
                    recipe.Container = null;
                }
            }

            Container.RemoveAll(c => c.Id == container.Id);
            
            Save();
        }
        #endregion

        #region Save
        [SuppressMessage("ReSharper", "AssignNullToNotNullAttribute")]
        public void Save()
        {
            Version++;
            LastChanged = DateTime.Now;

            if (FileName == null) return;
            if (!Directory.Exists(FileName))
                Directory.CreateDirectory(Path.GetDirectoryName(FileName));

            using (var fs = new FileStream(FileName, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                Serializer.Serialize(fs, this);
            }
        }
        #endregion
    }
}
