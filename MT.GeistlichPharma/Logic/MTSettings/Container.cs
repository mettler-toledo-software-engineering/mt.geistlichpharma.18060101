﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace MT.GeistlichPharma.Logic.MTSettings
{
    public class Container
    {
        public Guid Id { get; set; }
        public string Name { get; set; } = "Behälter";
        public double Weight { get; set; } = 0;
        public double Tolerance { get; set; } = 0;

        public Container Clone()
        {
            return new Container()
            {
                Id = Id,
                Name = Name,
                Weight = Weight,
                Tolerance = Tolerance
            };
        }
    }
}
