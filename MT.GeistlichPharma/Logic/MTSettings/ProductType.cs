﻿namespace MT.GeistlichPharma.Logic.MTSettings
{
    public enum ProductType
    {
        /// <summary>
        /// No product type defined
        /// </summary>
        None,
        /// <summary>
        /// Pen
        /// </summary>
        Pen,
        /// <summary>
        /// Flasche
        /// </summary>
        Flasche,
        /// <summary>
        /// Bio-Flow
        /// </summary>
        BioFlow //ergänzt mit change D
    }
}
