﻿using System;
using System.ComponentModel;

namespace MT.GeistlichPharma.Logic.MTSettings
{
    public class Recipe
    {
        public Guid RecipeId { get; set; }
        public string Number { get; set; }
        public string Description { get; set; }
        public string Size { get; set; }
        public double Target { get; set; }
        public string TargetFormatted => Target.ToString("0.000") + " g";
        public double AcceptanceMin { get; set; }
        public double AcceptanceMax { get; set; }
        public double Limit1 { get; set; }
        public double Limit2 { get; set; }
        public Container Container { get; set; }

        public DateTime LastModified { get; set; }
        public string LastModifiedFormatted => $"{LastModified:dd.MM.yyyy HH:mm}";

        public Recipe Clone()
        {
            Recipe recipe = new Recipe();
            recipe.RecipeId = RecipeId;
            recipe.Number = Number;
            recipe.Description = Description;
            recipe.Size = Size;
            recipe.Target = Target;
            recipe.AcceptanceMin = AcceptanceMin;
            recipe.AcceptanceMax = AcceptanceMax;
            recipe.Limit1 = Limit1;
            recipe.Limit2 = Limit2;
            recipe.LastModified = LastModified;
            recipe.Container = Container?.Clone();


            return recipe;
        }

    }
}
