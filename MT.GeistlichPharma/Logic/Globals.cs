﻿using MT.GeistlichPharma.Configuration;
using MT.Singularity.Platform;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Drawing;

namespace MT.GeistlichPharma.Logic
{
    public class Globals
    {
        private static Globals _instance;

        private BalancerConfiguration _config;

        public BalancerConfiguration Config
        {
            get { return _config; }
            set
            {
                if (_config == null)
                {
                    _config = value;
                }
            }
        }
        
        public static Globals Instance
        {
            get { return _instance ?? (_instance = new Globals()); }
        }

        public static int ButtonWidth = 140;
        public static readonly int TextBoxWidth = 350;
        public static readonly int TextBoxHeight = 50;
        public static readonly int BodyFontSize = 24;
        public static readonly int ButtonFontSize = 20;

        public static readonly string ArrowUpImage = "embedded://MT.GeistlichPharma/MT.GeistlichPharma.Images.ArrowUp.al8";
        public static readonly string ArrowDownImage = "embedded://MT.GeistlichPharma/MT.GeistlichPharma.Images.ArrowDown.al8";
        public static readonly string DeleteImage = "embedded://MT.GeistlichPharma/MT.GeistlichPharma.Images.Delete.al8";
        public static readonly string SaveImage = "embedded://MT.GeistlichPharma/MT.GeistlichPharma.Images.Save.al8";
        public static readonly string AddImage = "embedded://MT.GeistlichPharma/MT.GeistlichPharma.Images.Add.al8";


        public static readonly string DataDirectory = SingularityEnvironment.RunningApplication.DataDirectory;

        public static Color LightBlue = new Color(1, 248, 252, 248);
        public static readonly SolidColorBrush LightBlueBrush = new SolidColorBrush(LightBlue);

    }
}
