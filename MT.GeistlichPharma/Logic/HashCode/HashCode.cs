﻿using System;
using System.IO;
using System.Xml.Serialization;
using System.Security.Cryptography;

#if IndPro
    using MT.Singularity.Platform;
#endif

namespace MT.GeistlichPharma.Logic.HashCode
{
    public class HashCode
    {
#if IndPro
        public static readonly string FileName = Path.Combine(SingularityEnvironment.RunningApplication.DataDirectory, "HashCode.xml");
#else
        public static readonly string FileName = Path.Combine(".\\Settings", "HashCode.xml");
#endif
        public static readonly XmlSerializer Serializer = new XmlSerializer(typeof(HashCode));
        private static HashCode _current;

        #region Load
        private static HashCode Load()
        {
            if (!File.Exists(FileName))
            {
                return new HashCode();
            }
            else
            {
                try
                {
                    using (FileStream fs = new FileStream(FileName, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        return (HashCode)Serializer.Deserialize(fs);
                    }
                }
                catch (Exception)
                {
                    return new HashCode();
                }
            }
        }

        public static HashCode Current
        {
            get
            {
                EnsureSettings();
                return _current;
            }
        }

        public static void EnsureSettings()
        {
            if (_current == null)
                _current = Load();
        }
        #endregion

        public string Code { get; set; }

        #region Generate

        public bool Generate(string file)
        {
            try
            {
                Code = GenerateHashcode(file);
                Save();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion

        #region GenerateHashcode

        private string GenerateHashcode(string file)
        {
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(file))
                {
                    return BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", string.Empty);
                }
            }
        }

        #endregion

        #region Compare HashCode

        public bool IsSameHashCode(string file)
        {
            string oldHashcode = Code;
            string newHashcode = GenerateHashcode(file);

            return oldHashcode.Equals(newHashcode);
        }

        #endregion

        #region Save

        public void Save()
        {
            using (FileStream fs = new FileStream(FileName, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                Serializer.Serialize(fs, this);
            }
        }

        #endregion
    }
}
