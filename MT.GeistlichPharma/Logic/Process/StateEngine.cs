﻿namespace MT.GeistlichPharma.Logic.Process
{
    enum StateEngine
    {
        Init,
        Idle,
        LoadScale,
        PrepareDeltaTrac,
        Fill,
        ToleranceCheck,
        ToleranceCheckAfterPuls,
        Pulsing,
        Done,
        ResetScale
    }
}
