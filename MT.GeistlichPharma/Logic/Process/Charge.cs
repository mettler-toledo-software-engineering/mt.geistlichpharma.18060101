﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace MT.GeistlichPharma.Logic.Process
{
    public class Charge
    {
        #region Properties
        public DateTime StartTime { get; set; }

        public string Number { get; set; }

        public int RecipesInTol => _weightList.Count;

        public int RecipesOverdosed { get; set; }

        public double AverageWeight => _weightList.Any() ? _weightList.Average() : 0;

        public double MinWeight => _weightList.Any() ? _weightList.Min() : 0;

        public double MaxWeight => _weightList.Any() ? _weightList.Max() : 0;

        public double TotalWeight => _weightList.Any() ? _weightList.Sum() : 0;

        public int TotalFillings => RecipesOverdosed + RecipesInTol;
        public List<double> WeightList => _weightList;

        private readonly List<double> _weightList;
        #endregion

        public Charge(string number)
        {
            Number = number;
            _weightList = new List<double>();
        }

        public Charge()
        {
            _weightList = new List<double>();
        }
    }
}