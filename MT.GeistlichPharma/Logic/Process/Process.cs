﻿using System;

using System.Collections.Generic;
using System.Text;
using MT.GeistlichPharma.Logic.MTSettings;

namespace MT.GeistlichPharma.Logic.Process
{
    public class Process
    {
        public Recipe SelectedRecipe { get; set; }
        public Charge CurrentCharge { get; set; }
    }
}
