﻿using System;
using System.Threading.Tasks;
using MT.GeistlichPharma.Logic.MTSettings;
using MT.GeistlichPharma.Logic.Printer;

namespace MT.GeistlichPharma.Logic.Process
{
    public class Rapport
    {
        #region public
        public Recipe Recipe { get; set; }
        public Charge Charge { get; set; }
        #endregion

        #region private
        private readonly Apr331 _printer;

        private const int MaxLine = 48;
        private static readonly string dateFormat = "dd.MM.yy";
        private static readonly string timeFormat = "HH:mm:ss";
        private static readonly string weightFormat = "0.000 g";
        #endregion

        public Rapport()
        {
            _printer = new Apr331();
        }

        #region Print Header
        public async Task PrintHeaderAsync()
        {
            SetHeader(false);

            await _printer.PrintAsync();
        }

        private void SetHeader(bool isReprint)
        {
            AddToBody(_printer.Apr331Height3);
            AddToBody(_printer.Apr331DoubleWidth + "     Waegeprotokoll     ");
            AddToBody(_printer.Apr331SingleWidth + _printer.Apr331Height2);
            if (isReprint)
                AddToBody_Center("Reprint");
            PrintCharLine('*');
            AddToBody(" ");
            AddToBody_LeftRight("DATUM", Charge.StartTime.ToString(dateFormat));
            AddToBody_LeftRight("ZEIT", Charge.StartTime.ToString(timeFormat));
            AddToBody(" ");
            AddToBody(Recipe.Description);
            AddToBody_LeftRight(Recipe.Size + ", " + Recipe.Target.ToString(weightFormat), Recipe.Number);
            AddToBody_LeftRight("Chargennummer: ", Charge.Number.ToString());
            AddToBody_LeftRight("Akzeptanzkriterium min.:", Recipe.AcceptanceMin.ToString(weightFormat));
            AddToBody_LeftRight("Akzeptanzkriterium max.:", Recipe.AcceptanceMax.ToString(weightFormat));
            AddToBody(" ");
            PrintCharLine('-');
            AddToBody(" ");
            AddToBody(" ");
            AddToBody(" ");
            AddToBody(" ");
        }
        #endregion

        #region Print Body
        public async Task PrintBodyAsync()
        {
            SetBody();

            await _printer.PrintAsync();
        }

        private void SetBody()
        {
            AddToBody(" ");
            AddToBody_Center("STATISTIK");
            AddToBody(" ");
            PrintCharLine('-');
            AddToBody(" ");
            AddToBody_LeftRight("Mittelwert:", Charge.AverageWeight.ToString(weightFormat));
            AddToBody_LeftRight("Abfuellung min.:", Charge.MinWeight.ToString(weightFormat));
            AddToBody_LeftRight("Abfuellung max.:", Charge.MaxWeight.ToString(weightFormat));
            AddToBody(" ");
            PrintCharLine('-');
            AddToBody(" ");
            AddToBody_LeftRight("Abfuellmenge Gesamt:", Charge.TotalWeight.ToString(weightFormat));
            AddToBody_LeftRight("Anzahl Abfuellungen:", Charge.RecipesInTol.ToString("0 Stk"));
            AddToBody(" ");
            PrintCharLine('*');
            AddToBody(" ");
            AddToBody(_printer.Apr331DoubleWidth + _printer.Apr331Height3);
            AddToBody(" ");
            AddToBody(" ");
        }
        #endregion

        #region Reprint
        public async Task ReprintAsync()
        {
            SetHeader(true);
            SetBody();

            await _printer.PrintAsync();
        }
        #endregion

        #region Helper Methods
        public void PrintCharLine(char c)
        {
            _printer.Add("".PadRight(MaxLine, c));
        }

        public void AddToBody_LeftRight(string textL, string textR)
        {
            int padding = MaxLine - textL.Length - textR.Length;
            _printer.Add(textL + "".PadRight(padding, ' ') + textR);
        }

        public void AddToBody_Center(string textC)
        {
            int padding = (MaxLine - textC.Length) / 2;
            string filling = "".PadRight(padding, ' ');
            _printer.Add(filling + textC + filling);
        }

        public void AddToBody(string item)
        {
            _printer.Add(item);
        }
        #endregion
    }
}
