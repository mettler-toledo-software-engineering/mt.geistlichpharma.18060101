﻿using System;

using System.Collections.Generic;
using System.Text;
using MT.GeistlichPharma.Logic.MTSettings;
using MT.Singularity.Presentation.Controls;

namespace MT.GeistlichPharma.Logic
{
    public class ContainerSelectionDialogResult
    {
        public DialogResult DialogResult { get; set; }
        public Container SelectedContainer { get; set; }

    }
}
