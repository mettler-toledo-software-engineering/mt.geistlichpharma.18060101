﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using MT.GeistlichPharma.Pages;
using MT.Singularity.Logging;

namespace MT.GeistlichPharma.Logic.DigitalIO
{
    public class DigitalIo
    {
        /// <summary>
        /// Boolean flags, to stop both devices
        /// </summary>
        public bool[] Stop = { false, false, false, false };

        /// <summary>
        /// Boolean flags, to start both devices (Grob- & Feindosierung)
        /// </summary>
        public bool[] Fast = { true, true, false, false };

        /// <summary>
        /// Boolean flags, to start the first device (Grobdosierung)
        /// </summary>
        public bool[] CoarseDosing = { true, false, false, false };

        /// <summary>
        /// Boolean flags, to stop the first devices and start the second device (Feindosierung)
        /// </summary>
        public bool[] Slow = { false, true, false, false };

        /// <summary>
        /// Current boolean flags
        /// </summary>
        public bool[] OutputState { get; set; }

        public DigitalIo()
        {
            OutputState = Stop;
        }

        public void StartFastFill()
        {
            Debug.WriteLine("IO: start FAST FILL");
            StartThread(Fast);
        }

        public void StartSlowFill()
        {
            Debug.WriteLine("IO: start SLOW FILL");
            StartThread(Slow);
        }

        public void StopFill()
        {
            Debug.WriteLine("IO: stop FILL");
            StartThread(Stop);
        }

        public void StartStopFill(bool coarse, bool slow)
        {
            Debug.WriteLine("IO: start / stop COARSE OR PRECISE FILL");

            bool[] bits = { coarse, slow, false, false };
            StartThread(bits);
        }

        public void Continue()
        {
            StartThread(OutputState);
        }

        private void StartThread(bool[] inputState)
        {
            //Thread th = new Thread(async () =>
            //{
            //    var msg = "";

            //    if (HomeScreenViewModel.Instance == null)
            //    {
            //        msg = "HomeScreenViewModel.Instance not instantiated";
            //        Log4NetManager.ApplicationLogger.Error(msg);
            //        throw new Exception(msg);
            //    }

            //    if (HomeScreenViewModel.Instance.DigitalIo == null)
            //    {
            //        msg = "DigitalIo not instantiated";
            //        Log4NetManager.ApplicationLogger.Error(msg);
            //        throw new Exception(msg);
            //    }

            //    await HomeScreenViewModel.Instance.DigitalIo.WriteOutputAsync(inputState);
            //});

            //th.Start();

            Task.Run(async () =>
            {
                var msg = "";

                if (HomeScreenViewModel.Instance == null)
                {
                    msg = "HomeScreenViewModel.Instance not instantiated";
                    Log4NetManager.ApplicationLogger.Error(msg);
                    throw new Exception(msg);
                }

                if (HomeScreenViewModel.Instance.DigitalIo == null)
                {
                    msg = "DigitalIo not instantiated";
                    Log4NetManager.ApplicationLogger.Error(msg);
                    throw new Exception(msg);
                }

                await HomeScreenViewModel.Instance.DigitalIo.WriteOutputAsync(inputState);
            });

            OutputState = inputState;
        }
    }
}
