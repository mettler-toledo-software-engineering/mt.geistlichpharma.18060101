﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using MT.DataServices.UnibaseEx.Client.Communication;
using System.Net;

namespace MT.GeistlichPharma.Logic
{

    public class TimeSynch
    {

        public TimeSynch()
        {
            Thread timeSynchThread = new Thread(SynchTime);
            timeSynchThread.Start();
        }

        [SuppressMessage("ReSharper", "RedundantAssignment")]
        [SuppressMessage("ReSharper", "UnusedVariable")]
        [SuppressMessage("ReSharper", "FunctionNeverReturns")]
        [SuppressMessage("ReSharper", "CoVariantArrayConversion")]
        public void SynchTime()
        {
            while (true)
            {
                UnibaseExClient client = null;
                try
                {
                    client = ConnectUnibase();
                    if (client != null)
                    {
                        string result = (string)client.ExecuteExtension("TimeSynch.CurrentDateTime", new string[] { "dd.MM.yyyy HH:mm:ss" });
                        //SystemTime.SetTime(DateTime.Parse(result));
                    }
                }
                catch (Exception)
                {
                    // ignored
                }

                if (client != null)
                {
                    client.Close();
                    client = null;
                }
                Thread.Sleep(300000);
            }
        }


        private UnibaseExClient ConnectUnibase()
        {
            UnibaseExClient client;
            var endPoint = new IPEndPoint(IPAddress.Parse(Globals.Instance.Config.IpAddress), Globals.Instance.Config.Port);
            client = new UnibaseExClient(new TcpEndPointConnector(endPoint));
            client.Timeout = Globals.Instance.Config.HostTimeout * 1000;
            try
            {
                client.Open();
                return client;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
