﻿using System;

namespace MT.GeistlichPharma.Logic
{
    abstract class StaticStuff
    {
        public static string WeightFormat
        {
            get { return "0.000 g"; }
        }

        public static string WeightToString(double weight)
        {
            return weight.ToString(WeightFormat);
        }

        public static string WeightToString(string weight)
        {
            return String.Format(WeightFormat, weight);
        }
    }
}
