﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MT.Singularity.Composition;
using MT.Singularity.IO;
using MT.Singularity.Logging;
using MT.Singularity.Platform.Devices;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Serialization;

namespace MT.GeistlichPharma.Logic.Printer
{
    public class Apr331
    {
        #region Properties
        public string Apr331Height2 { get { return "\x1B\x48" + "2"; } }
        public string Apr331Height3 { get { return "\x1B\x48" + "3"; } }
        public string Apr331DoubleWidth { get { return "\x0E"; } }
        public string Apr331SingleWidth { get { return "\x0F"; } }
        #endregion

        private StringSerializer _stringSerializer;
        private List<string> _stringlist;

        public Apr331()
        {
            _stringlist = new List<string>();

            Initialize();
        }

        #region Initialize
        [SuppressMessage("ReSharper", "IdentifierTypo")]
        [SuppressMessage("ReSharper", "RedundantEnumerableCastCall")]
        private async void Initialize()
        {
            try
            {
                var compositionContainer = ApplicationBootstrapperBase.CompositionContainer;
                var platformEngine = compositionContainer.Resolve<IPlatformEngine>();
                var interfaceService = await platformEngine.GetInterfaceServiceAsync();

                var allInterfaces = await interfaceService.GetAllInterfacesAsync();
                var allserialInterface = allInterfaces.OfType<ISerialInterface>();
                var serialInterface = allserialInterface.OfType<ISerialInterface>().FirstOrDefault((item => item.HardwareType == InterfaceHardwareType.RS232));

                IConnectionChannel<DataSegment> actualConnectionChannel = await serialInterface.CreateConnectionChannelAsync();

                if (actualConnectionChannel == null)
                    return;

                var delimiterSerializer = new DelimiterSerializer(actualConnectionChannel, CommonDataSegments.Newline);
                _stringSerializer = new StringSerializer(delimiterSerializer);
            }
            catch (Exception ex)
            {
                Log4NetManager.ApplicationLogger.Error(ex.Message, ex);
            }
        }
        #endregion

        #region Open serial port
        private async Task OpenSerialPort()
        {
            try
            {
                Debug.WriteLine("Opening serial port...");
                await _stringSerializer.OpenAsync();
                Debug.WriteLine("Opened");
            }
            catch (Exception ex)
            {
                Log4NetManager.ApplicationLogger.Error(ex.Message, ex);
            }
        }
        #endregion

        #region Close serial port
        private async Task CloseSerialPort()
        {
            try
            {
                Debug.WriteLine("Closing serial port...");
                await _stringSerializer.CloseAsync();
                Debug.WriteLine("Closed");
                _stringlist = new List<string>();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                Log4NetManager.ApplicationLogger.Error(ex.Message, ex);
            }
        }
        #endregion

        #region Print Async
        public async Task PrintAsync()
        {
            await OpenSerialPort();

            try
            {
                foreach (String s in _stringlist)
                {
                    Debug.WriteLine("Printing '" + s);
                    await _stringSerializer.WriteAsync(s);
                    await Task.Delay(50);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                Log4NetManager.ApplicationLogger.Error(ex.Message, ex);
            }

            await CloseSerialPort();
        }
        #endregion

        #region Add
        public void Add(string item)
        {
            _stringlist.Add(item);
        }
        #endregion
    }
}