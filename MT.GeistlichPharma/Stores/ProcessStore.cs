﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using MT.GeistlichPharma.Infrastructure;
using MT.GeistlichPharma.Logic.MTSettings;
using MT.GeistlichPharma.Logic.Process;
using MT.Singularity.Composition;

namespace MT.GeistlichPharma.Stores
{
    [Export(typeof(ProcessStore))]
    [InjectionBehavior(IsSingleton = true)]
    public class ProcessStore
    {
        private Process _currentProcess = new Process();
        private readonly IProcessSerializationService _processSerializationService;
        public ProcessStore(IProcessSerializationService processSerializationService)
        {
            _processSerializationService = processSerializationService;
            _processSerializationService.CreateJsonIfItDoesNotExists(_currentProcess);
        }

        public ProcessStore(IProcessSerializationService processSerializationService, Charge charge)
        {
            _currentProcess.CurrentCharge = charge;
        }

        public Recipe SelectedRecipe => _currentProcess.SelectedRecipe;
        public Charge CurrentCharge => _currentProcess.CurrentCharge;

        public void SetRecipe(Recipe recipe)
        {
            _currentProcess.SelectedRecipe = recipe;
            UpdateJson();
        }

        public void SetCharge(Charge charge)
        {
            _currentProcess.CurrentCharge = charge;
            UpdateJson();
        }


        public bool CheckIfUnfinishedProcessExists()
        {
            var process = _processSerializationService.ReadClassFromJson();

            if (process.CurrentCharge?.WeightList != null && process.CurrentCharge.WeightList.Any())
            {
                _currentProcess = process;
                return true;
            }

            return false;
        }

        private void UpdateJson()
        {
            _processSerializationService.WriteClassToJson(_currentProcess);
        }

        public void ResetProcess()
        {
            _currentProcess = new Process();
            UpdateJson();
        }


        /// <summary>
        /// Increment counter for overdosed recipes
        /// </summary>
        public void RecipeOverdosed()
        {
            _currentProcess.CurrentCharge.RecipesOverdosed++;
            UpdateJson();
        }

        /// <summary>
        /// Save weight temporary and prepare for rapport
        /// </summary>
        /// <param name="weight"></param>
        public void SaveWeight(double weight)
        {
            _currentProcess.CurrentCharge.WeightList.Add(weight);
            UpdateJson();
        }

    }
}
