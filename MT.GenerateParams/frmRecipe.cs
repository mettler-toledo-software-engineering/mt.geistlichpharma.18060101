﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Windows.Forms;
using MT.GeistlichPharma.Logic.MTSettings;

namespace MT.GenerateParams
{
    [SuppressMessage("ReSharper", "SpecifyACultureInStringConversionExplicitly")]
    [SuppressMessage("ReSharper", "LocalizableElement")]
    public partial class FrmRecipe : Form
    {
        private readonly Recipe _currentRecipe;
        private readonly int _selectedIndex;
        private Recipe _newRecipe;

        public FrmRecipe()
        {
            InitializeComponent();
            InizializeDefaultValues();

            _newRecipe = new Recipe();
        }

        public FrmRecipe(int index, Recipe recipe)
        {
            InitializeComponent();
            InizializeDefaultValues();

            _currentRecipe = recipe;
            _selectedIndex = index;

            LoadRecipe();
        }

        private void InizializeDefaultValues()
        {
            cbType.DataSource = Enum.GetNames(typeof(ProductType));
        }

        private void LoadRecipe()
        {
            _newRecipe = _currentRecipe;

            tbNumber.Text = _currentRecipe.Number;
            tbDescription.Text = _currentRecipe.Description;
            tbSize.Text = _currentRecipe.Size;
            tbTarget.Text = _currentRecipe.Target.ToString();
            tbAcceptanceMin.Text = _currentRecipe.AcceptanceMin.ToString();
            tbAcceptanceMax.Text = _currentRecipe.AcceptanceMax.ToString();
            tbLimit1.Text = _currentRecipe.Limit1.ToString();
            tbLimit2.Text = _currentRecipe.Limit2.ToString();
        }

        #region Buttons

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (CheckEmptyFields())
                return;

            if (CheckInValidLimits(tbAcceptanceMin, _newRecipe.AcceptanceMin, _newRecipe.AcceptanceMax))
                return;

            if (CheckInValidLimits(tbLimit1, _newRecipe.Limit1, _newRecipe.Limit2))
                return;

            if (_currentRecipe == null)
                Settings.Current.AddRecipe(_newRecipe);
            else
                Settings.Current.UpdateRecipe(_selectedIndex, _newRecipe);

            Close();
        }

        private bool CheckEmptyFields()
        {
            return (tbNumber.Text == "" || tbDescription.Text == "" || tbSize.Text == "" || tbAcceptanceMax.Text == "" ||
                tbAcceptanceMin.Text == "" || tbTarget.Text == "" || tbLimit1.Text == "" || tbLimit2.Text == "");
        }

        private bool CheckInValidLimits(TextBox textBox, double lowerValue, double greaterValue)
        {

            if (!(lowerValue > greaterValue))
                return false;

            MessageBox.Show("Value '" + lowerValue + "' must be smaller than value '" + greaterValue + "'!",
                "Invalid Limits", MessageBoxButtons.OK, MessageBoxIcon.Error);
            textBox.Focus();

            return true;
        }

        #endregion

        #region Leave (Lost Focus)

        private void tbNumber_TextChanged(object sender, EventArgs e)
        {
            _newRecipe.Number = tbNumber.Text;
        }

        private void tbDescription_TextChanged(object sender, EventArgs e)
        {
            _newRecipe.Description = tbDescription.Text;
        }
                
        private void tbSize_TextChanged(object sender, EventArgs e)
        {
            _newRecipe.Size = tbSize.Text;
        }

        private void tbTarget_Leave(object sender, EventArgs e)
        {
            _newRecipe.Target = ValidateField(ref tbTarget);
        }

        private void tbAcceptanceMin_Leave(object sender, EventArgs e)
        {
            _newRecipe.AcceptanceMin = ValidateField(ref tbAcceptanceMin);
        }

        private void tbAcceptanceMax_Leave(object sender, EventArgs e)
        {
            _newRecipe.AcceptanceMax = ValidateField(ref tbAcceptanceMax);
        }

        private void tbLimit1_Leave(object sender, EventArgs e)
        {
            _newRecipe.Limit1 = ValidateField(ref tbLimit1);
        }

        private void tbLimit2_Leave(object sender, EventArgs e)
        {
            _newRecipe.Limit2 = ValidateField(ref tbLimit2);
        }

        private double ValidateField(ref TextBox textBox)
        {
            try
            {
                double tmp = double.Parse(textBox.Text);
                return tmp;
            }
            catch
            {
                textBox.Focus();
                textBox.SelectAll();
                return 0;
            }
        }

        #endregion
    }
}
