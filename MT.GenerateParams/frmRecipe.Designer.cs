﻿namespace MT.GenerateParams
{
    partial class FrmRecipe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tbTarget = new System.Windows.Forms.TextBox();
            this.tbAcceptanceMin = new System.Windows.Forms.TextBox();
            this.tbAcceptanceMax = new System.Windows.Forms.TextBox();
            this.tbLimit1 = new System.Windows.Forms.TextBox();
            this.tbLimit2 = new System.Windows.Forms.TextBox();
            this.tbNumber = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.cbType = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.tbSize = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tbDescription = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Grösse";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 120);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Abfüllgewicht:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 146);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(121, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Akzeptanzkriterium min.:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 172);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(124, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Akzeptanzkriterium max.:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 198);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Limite 1:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 225);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 13);
            this.label6.TabIndex = 20;
            this.label6.Text = "Limite 2:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 15);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Artikelnummer:";
            // 
            // tbTarget
            // 
            this.tbTarget.Location = new System.Drawing.Point(153, 117);
            this.tbTarget.Name = "tbTarget";
            this.tbTarget.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tbTarget.Size = new System.Drawing.Size(135, 20);
            this.tbTarget.TabIndex = 4;
            this.tbTarget.Leave += new System.EventHandler(this.tbTarget_Leave);
            // 
            // tbAcceptanceMin
            // 
            this.tbAcceptanceMin.Location = new System.Drawing.Point(152, 143);
            this.tbAcceptanceMin.Name = "tbAcceptanceMin";
            this.tbAcceptanceMin.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tbAcceptanceMin.Size = new System.Drawing.Size(135, 20);
            this.tbAcceptanceMin.TabIndex = 5;
            this.tbAcceptanceMin.Leave += new System.EventHandler(this.tbAcceptanceMin_Leave);
            // 
            // tbAcceptanceMax
            // 
            this.tbAcceptanceMax.Location = new System.Drawing.Point(153, 169);
            this.tbAcceptanceMax.Name = "tbAcceptanceMax";
            this.tbAcceptanceMax.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tbAcceptanceMax.Size = new System.Drawing.Size(135, 20);
            this.tbAcceptanceMax.TabIndex = 6;
            this.tbAcceptanceMax.Leave += new System.EventHandler(this.tbAcceptanceMax_Leave);
            // 
            // tbLimit1
            // 
            this.tbLimit1.Location = new System.Drawing.Point(152, 195);
            this.tbLimit1.Name = "tbLimit1";
            this.tbLimit1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tbLimit1.Size = new System.Drawing.Size(135, 20);
            this.tbLimit1.TabIndex = 7;
            this.tbLimit1.Leave += new System.EventHandler(this.tbLimit1_Leave);
            // 
            // tbLimit2
            // 
            this.tbLimit2.Location = new System.Drawing.Point(152, 221);
            this.tbLimit2.Name = "tbLimit2";
            this.tbLimit2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tbLimit2.Size = new System.Drawing.Size(135, 20);
            this.tbLimit2.TabIndex = 8;
            this.tbLimit2.Leave += new System.EventHandler(this.tbLimit2_Leave);
            // 
            // tbNumber
            // 
            this.tbNumber.Location = new System.Drawing.Point(153, 12);
            this.tbNumber.Name = "tbNumber";
            this.tbNumber.Size = new System.Drawing.Size(135, 20);
            this.tbNumber.TabIndex = 0;
            this.tbNumber.TextChanged += new System.EventHandler(this.tbNumber_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(293, 225);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(19, 13);
            this.label8.TabIndex = 22;
            this.label8.Text = "[g]";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(293, 120);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(19, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "[g]";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(293, 146);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(19, 13);
            this.label10.TabIndex = 13;
            this.label10.Text = "[g]";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(293, 172);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(19, 13);
            this.label11.TabIndex = 16;
            this.label11.Text = "[g]";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(293, 198);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(19, 13);
            this.label12.TabIndex = 19;
            this.label12.Text = "[g]";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(212, 299);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(100, 30);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cbType
            // 
            this.cbType.FormattingEnabled = true;
            this.cbType.Location = new System.Drawing.Point(153, 90);
            this.cbType.Name = "cbType";
            this.cbType.Size = new System.Drawing.Size(135, 21);
            this.cbType.TabIndex = 3;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(12, 93);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(61, 13);
            this.label14.TabIndex = 6;
            this.label14.Text = "Produkttyp:";
            // 
            // tbSize
            // 
            this.tbSize.Location = new System.Drawing.Point(152, 64);
            this.tbSize.Name = "tbSize";
            this.tbSize.Size = new System.Drawing.Size(135, 20);
            this.tbSize.TabIndex = 2;
            this.tbSize.TextChanged += new System.EventHandler(this.tbSize_TextChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(12, 41);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(69, 13);
            this.label13.TabIndex = 2;
            this.label13.Text = "Bezeichnung";
            // 
            // tbDescription
            // 
            this.tbDescription.Location = new System.Drawing.Point(152, 38);
            this.tbDescription.Name = "tbDescription";
            this.tbDescription.Size = new System.Drawing.Size(135, 20);
            this.tbDescription.TabIndex = 1;
            this.tbDescription.TextChanged += new System.EventHandler(this.tbDescription_TextChanged);
            // 
            // FrmRecipe
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(324, 341);
            this.Controls.Add(this.tbDescription);
            this.Controls.Add(this.tbSize);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.cbType);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.tbNumber);
            this.Controls.Add(this.tbLimit2);
            this.Controls.Add(this.tbLimit1);
            this.Controls.Add(this.tbAcceptanceMax);
            this.Controls.Add(this.tbAcceptanceMin);
            this.Controls.Add(this.tbTarget);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(340, 380);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(340, 380);
            this.Name = "FrmRecipe";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Recipe";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbTarget;
        private System.Windows.Forms.TextBox tbAcceptanceMin;
        private System.Windows.Forms.TextBox tbAcceptanceMax;
        private System.Windows.Forms.TextBox tbLimit1;
        private System.Windows.Forms.TextBox tbLimit2;
        private System.Windows.Forms.TextBox tbNumber;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ComboBox cbType;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tbSize;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tbDescription;
    }
}