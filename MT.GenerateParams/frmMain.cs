﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Windows.Forms;
using MT.GeistlichPharma.Logic.HashCode;
using MT.GeistlichPharma.Logic.MTSettings;

namespace MT.GenerateParams
{
    [SuppressMessage("ReSharper", "SpecifyACultureInStringConversionExplicitly")]
    [SuppressMessage("ReSharper", "LocalizableElement")]
    public partial class FrmMain : Form
    {
        private Recipe _currentRecipe;
        private int _selectedIndex = -1;

        private bool _recipesSaved;

        public FrmMain()
        {
            InitializeComponent();
            _recipesSaved = false;

            EnableSaveButtons(false);
        }

        private void EnableSaveButtons(bool enable)
        {
            btnGenerateHashcode.Enabled = enable;
            saveRecipesToolStripMenuItem.Enabled = enable;
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            LoadRecipes();
            LoadInformation();
        }

        private void LoadInformation()
        {
            lbLastChanged.Text = Settings.Current.LastChanged.ToString();
            lbVersion.Text = Settings.Current.Version.ToString();
            UpdateItemCounter();
        }

        private void UpdateItemCounter()
        {
            lbItems.Text = Settings.Current.Recipes.Count.ToString();
        }

        private void LoadRecipes()
        {
            dgvRecipes.DataSource = null;
            dgvRecipes.DataSource = Settings.Current.Recipes;
            dgvRecipes.ClearSelection();

            EnablelButtons(false, false);
        }

        #region Buttons

        private void btnNew_Click(object sender, EventArgs e)
        {
            FrmRecipe frm = new FrmRecipe();
            frm.FormClosed += frm_FormClosed;
            frm.ShowDialog();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditRecipe();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteRecipe();
        }

        private void frm_FormClosed(object sender, FormClosedEventArgs e)
        {
            LoadRecipes();
            LoadInformation();

            EnableSaveButtons(true);
        }

        private void EditRecipe()
        {
            FrmRecipe frm = new FrmRecipe(_selectedIndex, _currentRecipe);
            frm.FormClosed += frm_FormClosed;
            frm.ShowDialog();
        }

        private void EnablelButtons(bool enableEdit, bool enableDel)
        {
            btnEdit.Enabled = enableEdit;
            btnDelete.Enabled = enableDel;
        }

        private void DeleteRecipe()
        {
            DialogResult res = MessageBox.Show(this, "Do you really want to delete this item?", "Delete item", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (res == DialogResult.Yes)
            {
                Settings.Current.DeleteRecipe(_selectedIndex);
                LoadRecipes();
                LoadInformation();
            }
        }

        private void btnGenerateHashcode_Click(object sender, EventArgs e)
        {
            SaveRecipes();
        }

        private void SaveRecipes()
        {
            if (File.Exists(Settings.FileName))
                SaveBackupFile();

            SaveCurrentSettings();
            //GenerateHashcode();
            LoadInformation();
        }

        #endregion

        #region Generate Hash Code

        #region ---- GenerateHashCode

        private void GenerateHashcode()
        {
            _recipesSaved = true;

            //if (HashCode.Current.Generate(Settings.FileName))
            //    MessageBox.Show(this, "Hashcode successfully generated!", "Generate Hashcode", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //else
            //    MessageBox.Show(this, "Hashcode could not be generated!", "Generate Hashcode", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        #endregion

        #region ---- SaveCurrentSettings

        private static void SaveCurrentSettings()
        {
            Settings.Current.Save();
        }

        #endregion

        #region ---- SaveBackupFile

        private void SaveBackupFile()
        {
            try
            {
                string file = Settings.FileName;
                string backupFile = Settings.BackupFileName;

                if (File.Exists(backupFile))
                    File.Delete(backupFile);

                File.Copy(file, backupFile);
            }
            catch (Exception)
            {
                // ignored
            }
        }

        #endregion

        #endregion

        #region SelectionChanged

        private void dgvRecipes_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgvRecipes.SelectedRows.Count > 0)
                {
                    if (dgvRecipes.CurrentRow != null)
                    {
                        _selectedIndex = dgvRecipes.CurrentRow.Index;
                        _currentRecipe = (Recipe) dgvRecipes.CurrentRow.DataBoundItem;
                    }

                    EnablelButtons(true, true);
                }
                else
                    _selectedIndex = -1;
            }
            catch (Exception)
            {
                // ignored
            }
        }

        private void dgvRecipes_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (dgvRecipes.CurrentRow != null) _selectedIndex = dgvRecipes.CurrentRow.Index;
            EditRecipe();
        }

        #endregion

        #region KeyUp

        private void dgvRecipes_KeyUp(object sender, KeyEventArgs e)
        {
            if (_selectedIndex >= 0 && e.KeyCode == Keys.Delete)
                DeleteRecipe();
        }

     #endregion

        #region Menu

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox ab = new AboutBox();
            ab.ShowDialog();
        }

        private void saveRecipesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveRecipes();
        }

        private void quitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CloseProgram();
            Close();
        }

        private void CloseProgram()
        {
            if (!_recipesSaved)
            {
                DialogResult res = MessageBox.Show("Do you want to save the Recipes?", "Save Recipes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (res == DialogResult.Yes)
                    SaveRecipes();
            }
        }

        #endregion

        #region FormClosing
        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            CloseProgram();
        }
        #endregion
    }
}
