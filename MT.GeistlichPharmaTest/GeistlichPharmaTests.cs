﻿using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MT.GeistlichPharma.Infrastructure;
using MT.GeistlichPharma.Logic.Process;
using MT.GeistlichPharma.Logic.DigitalIO;
using MT.GeistlichPharma.Stores;


namespace MT.GeistlichPharmaTest
{
    [TestClass]
    public class GeistlichPharmaTests
    {
        
        private readonly DigitalIo _digitalIo = new DigitalIo();
        private readonly double _minWeight = 1.25;
        private readonly double _maxWeight = 2.36;
        private readonly double _weight = 1.65;
        private readonly ProcessStore _processStore = new ProcessStore(new ProcessSerializationService(), new Charge("12345"));

        [TestMethod]
        public void TestCalculateAverageWeightSuccess()
        {
            _processStore.SaveWeight(_minWeight);
            _processStore.SaveWeight(_maxWeight);
            _processStore.SaveWeight(_weight);

            var expected = (_minWeight + _maxWeight + _weight) / 3;

            Assert.AreEqual((object)expected, _processStore.CurrentCharge.AverageWeight);
        }

        [TestMethod]
        public void TestCalculateAverageWeightFail()
        {
            _processStore.SaveWeight(_minWeight);
            _processStore.SaveWeight(_maxWeight);
            _processStore.SaveWeight(_weight);

            var expected = (_minWeight + _maxWeight + _weight) / 3;

            Assert.AreNotEqual((object)expected, _processStore.CurrentCharge.MaxWeight);
        }

        [TestMethod]
        public void TestMinWeightSuccess()
        {
            _processStore.SaveWeight(_minWeight);
            _processStore.SaveWeight(_maxWeight);
            _processStore.SaveWeight(_weight);

            var expected = _minWeight;

            Assert.AreEqual((object)expected, _processStore.CurrentCharge.MinWeight);
        }

        [TestMethod]
        public void TestMinWeightFail()
        {
            _processStore.SaveWeight(_minWeight);
            _processStore.SaveWeight(_maxWeight);
            _processStore.SaveWeight(_weight);

            var expected = _minWeight;

            Assert.AreNotEqual((object)expected, _processStore.CurrentCharge.MaxWeight);
        }

        [TestMethod]
        public void TestMaxWeightSuccess()
        {
            _processStore.SaveWeight(_minWeight);
            _processStore.SaveWeight(_maxWeight);
            _processStore.SaveWeight(_weight);

            var expected = _maxWeight;

            Assert.AreEqual((object)expected, _processStore.CurrentCharge.MaxWeight);
        }

        [TestMethod]
        public void TestMaxWeightFail()
        {
            _processStore.SaveWeight(_minWeight);
            _processStore.SaveWeight(_maxWeight);
            _processStore.SaveWeight(_weight);

            var expected = _maxWeight;

            Assert.AreNotEqual((object)expected, _processStore.CurrentCharge.MinWeight);
        }


        [TestMethod]
        public void TestStartFastFillSuccess()
        {
            _digitalIo.StartFastFill();

            bool[] expected = { true, true, false, false };
            CollectionAssert.AreEqual(expected, _digitalIo.OutputState);
        }

        [TestMethod]
        public void TestStartFastFillFail()
        {
            _digitalIo.StartFastFill();

            bool[] expected = { true, true, false, false };
            CollectionAssert.AreNotEqual(expected, _digitalIo.Slow);
        }

        [TestMethod]
        public void TestStartSlowFillSuccess()
        {
            _digitalIo.StartSlowFill();

            bool[] expected = { false, true, false, false };
            CollectionAssert.AreEqual(expected, _digitalIo.OutputState);
        }

        [TestMethod]
        public void TestStartSlowFillFail()
        {
            _digitalIo.StartSlowFill();

            bool[] expected = { false, true, false, false };
            CollectionAssert.AreNotEqual(expected, _digitalIo.Fast);
        }

        [TestMethod]
        public void TestStopFillSuccess()
        {
            _digitalIo.StopFill();

            bool[] expected = { false, false, false, false };
            CollectionAssert.AreEqual(expected, _digitalIo.OutputState);
        }

        [TestMethod]
        public void TestStopFillFail()
        {
            _digitalIo.StopFill();

            bool[] expected = { false, false, false, false };
            CollectionAssert.AreNotEqual(expected, _digitalIo.Fast);
        }
    }
}
